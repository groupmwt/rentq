//
//  HistoryVC.swift
//  GO2
//
//  Created by NC2-46 on 07/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController {

    var arrReserved = [MyHistoryData]()
    var arrRentals = [MyHistoryData]()
    var selectedIndex: Int = 0
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideMenu()
    }
    @IBAction func segmentControl(_ sender: Any) {

        segmentControl.changeUnderlinePosition()
        selectedIndex = segmentControl.selectedSegmentIndex
        self.tblView.setContentOffset(CGPoint.zero, animated:true)
        self.tblView.reloadData()
        scrollToFirstRow()
        
    }
    
    func scrollToFirstRow() {
        let indexPath = IndexPath(row: 0, section: 0)
        if self.tblView.validate(indexPath: indexPath) {
            self.tblView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
        
    }
}


extension UITableView {
    
    func validate(indexPath: IndexPath) -> Bool {
        if indexPath.section >= numberOfSections {
            return false
        }
        
        if indexPath.row >= numberOfRows(inSection: indexPath.section) {
            return false
        }
        
        return true
    }
    
}
