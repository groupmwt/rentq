//
//  InvoiceVC.swift
//  GO2
//
//  Created by NC2-46 on 19/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit

class InvoiceVC: UIViewController {

    //MARK: Views
    @IBOutlet weak var globalView: UIView!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var labelTotalAmount: UILabel!
    @IBOutlet weak var btnPayment: UIButtonX!
    
    var cityName : String!
    var headerTitle: String = "Payment Details"
    var searchVehicleData : SearchVehicleData!
    var myVehicleData : VehicleDetailsData!
    var arrVehicleOffersData = [SearchVehicleOffers]()
    var totalAmount : Float = 0
    var isFromNearbyVehicle : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Style
        navigationController?.customNavBar()
        btnPayment.backgroundColor = Color.buttonBG
        //navigationItem.customTitle()
        self.title = headerTitle
        setBackBtn()
        
        globalView.setBackgroundWithTopBarToView()
            
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        
        arrVehicleOffersData = isFromNearbyVehicle ? myVehicleData.offers! : searchVehicleData.offers!
        labelTotalAmount.text = " $ "
    }
    
    @IBAction func btnPayment(_ sender: UIButtonX) {
        let centerViewController = loadViewController(StoryBoardName: "MakePayment", VCIdentifer: "PaymentVC") as! PaymentVC
        centerViewController.purchaseAmount = "\(totalAmount)"
        centerViewController.isFromNearbyVehicle = self.isFromNearbyVehicle
        if isFromNearbyVehicle{
            centerViewController.myVehicleData = self.myVehicleData
        }else{
            centerViewController.searchVehicleData = self.searchVehicleData
        }
        centerViewController.cityName = self.cityName
        self.navigationController?.pushViewController(centerViewController, animated: true)
        
    }
    
    
}

//MARK: - TableView Data Source and delegate Method
extension InvoiceVC : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.italicSystemFont(ofSize: 17)
        header.textLabel?.textColor = UIColor.darkGray
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return "Item Name : " + "Car Rented"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVehicleOffersData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let lblRentDate = cell.contentView.viewWithTag(51) as! UILabel
        let lblRentPrice = cell.contentView.viewWithTag(53) as! UILabel
        
        lblRentDate.text = arrVehicleOffersData[indexPath.row].offerDate
        lblRentPrice.text = "$ \(arrVehicleOffersData[indexPath.row].price ?? 0)"
        totalAmount += arrVehicleOffersData[indexPath.row].price ?? 0
        labelTotalAmount.text = " $ \(totalAmount)"
        return cell
    }
    
}
