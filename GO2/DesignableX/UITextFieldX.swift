//
//  DesignableUITextField.swift
//  SkyApp
//
//  Created by Mark Moeykens on 12/16/16.
//  Copyright © 2016 Mark Moeykens. All rights reserved.
//

import UIKit

@IBDesignable
class UITextFieldX: UITextField {
    /// SwifterSwift: Left view tint color.
    @IBInspectable var leftViewTintColor: UIColor? {
        didSet{
            updateView()
        }
    }
    /// SwifterSwift: Right view tint color.
    @IBInspectable public var rightViewTintColor: UIColor? {
        didSet{
            updateView()
        }
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    @IBInspectable var leftImageHeight: CGFloat = 20 {
        didSet {
            updateView()
        }
    }
    @IBInspectable var leftImageWidth: CGFloat = 20 {
        didSet {
            updateView()
        }
    }
    @IBInspectable var leftImageCenterPedding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightImageHeight: CGFloat = 20 {
        didSet {
            updateView()
        }
    }
    @IBInspectable var rightImageWidth: CGFloat = 20 {
        didSet {
            updateView()
        }
    }
    @IBInspectable var rightImageCenterPedding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    private var _isRightViewVisible: Bool = true
    var isRightViewVisible: Bool {
        get {
            return _isRightViewVisible
        }
        set {
            _isRightViewVisible = newValue
            updateView()
        }
    }
    
    func updateView() {
        setLeftImage()
        setRightImage()
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: tintColor])
    }
    
    func setLeftImage() {
        leftViewMode = UITextFieldViewMode.always
        var view: UIView
        if let image = leftImage {
            let imageView = UIImageView()
            imageView.frame.size = CGSize( width: leftImageWidth, height: leftImageHeight)
            
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = tintColor
            if leftViewTintColor != nil {
                imageView.image = image.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = leftViewTintColor
            }
            
            var width = imageView.frame.size.width + leftPadding
            
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line {
                width += 5
            }
 
            view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: self.frame.height))
            imageView.center.x = view.center.x + leftImageCenterPedding
            imageView.center.y = view.center.y
            
            view.addSubview(imageView)
        } else {
            view = UIView(frame: CGRect(x: 0, y: 0, width: leftPadding, height: self.frame.height))
        }
        leftView = view
    }
    
    func setRightImage() {
        rightViewMode = UITextFieldViewMode.always
        
        var view: UIView
        
        if let image = rightImage, isRightViewVisible {
            let imageView = UIImageView()
            imageView.frame.size = CGSize( width: rightImageWidth, height: rightImageHeight)
            
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = tintColor
            if rightViewTintColor != nil {
                imageView.image = image.withRenderingMode(.alwaysTemplate)
                imageView.tintColor = rightViewTintColor
            }
            
            var width = imageView.frame.size.width + rightPadding
            
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line {
                width += 5
            }
            
            view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: self.frame.height))
            imageView.center.x = view.center.x + rightImageCenterPedding
            imageView.center.y = view.center.y
            
            view.addSubview(imageView)
        } else {
            view = UIView(frame: CGRect(x: 0, y: 0, width: rightPadding, height: self.frame.height))
        }
        
        rightView = view
    }
    
    
    // MARK: - Corner Radius
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}
