//
//  TermConditionVC.swift
//  GO2
//
//  Created by C115 on 15/04/21.
//  Copyright © 2021 Juan Cardozo. All rights reserved.
//

import UIKit
import WebKit

class TermConditionVC: UIViewController, WKUIDelegate ,WKNavigationDelegate{

    @IBOutlet weak var vwDisTerms: UIView!
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideMenu()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideMenu()
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupUI() {
        self.title = "Terms and Conditions"
        navigationController?.customNavBar()
        
        let webConfiguration = WKWebViewConfiguration()
        let customFrame = CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.vwDisTerms.frame.size.width, height: self.vwDisTerms.frame.size.height))
        self.webView = WKWebView (frame: customFrame , configuration: webConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.vwDisTerms.addSubview(webView)
        self.view.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        webView.topAnchor.constraint(equalTo: vwDisTerms.topAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: vwDisTerms.rightAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: vwDisTerms.leftAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: vwDisTerms.bottomAnchor).isActive = true
        webView.heightAnchor.constraint(equalTo: vwDisTerms.heightAnchor).isActive = true
        webView.uiDelegate = self
        webView.navigationDelegate = self
//        let myURL = URL(string: "http://clientapp.narola.online/PG/QR-Code/RENT-QPRIVACY_POLICY.html")
//        let myRequest = URLRequest(url: myURL!)
//        webView.load(myRequest)

        let htmlFile = Bundle.main.path(forResource: "RENT-Q TERMS OF USE", ofType: "html")
        let html = try! String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
        self.webView.loadHTMLString(html, baseURL: nil)
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.view.stopIndicator(indicator: indicator, container: container)
    }
}
