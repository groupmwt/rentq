//
//  NSDictionary.swift
//  GO2
//
//  Created by NC2-46 on 17/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
extension NSDictionary
{
    func RemoveNullValueFromDic()-> NSDictionary
    {
        let mutableDictionary:NSMutableDictionary = NSMutableDictionary(dictionary: self)
        for key in mutableDictionary.allKeys
        {
            let value = mutableDictionary[key] as AnyObject
            if(value is NSNull)
            {
                mutableDictionary.setValue("", forKey: key as! String)
            }
            
        }
        return mutableDictionary
    }
}
