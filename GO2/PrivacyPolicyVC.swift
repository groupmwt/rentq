//
//  PrivacyPolicyVC.swift
//  GO2
//
//  Created by NC2-46 on 06/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyVC: UIViewController ,WKUIDelegate ,WKNavigationDelegate{
    
//    @IBOutlet weak var webKitView: WKWebView!
    @IBOutlet weak var textPolicy: UITextView!
    //MARK: - Views
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var viewDisWebKitView: UIView!
    
    var headerTitle : String =  ""
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideMenu()
        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideMenu()
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PrivacyPolicyVC {
    
    func setupUI() {
        
        
        self.title = headerTitle
        globalView.setBackgroundWithTopBarToView()        
        navigationController?.customNavBar()
        //navigationItem.customTitle()
        
        let webConfiguration = WKWebViewConfiguration()
        let customFrame = CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.viewDisWebKitView.frame.size.width, height: self.viewDisWebKitView.frame.size.height))
        self.webView = WKWebView (frame: customFrame , configuration: webConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.viewDisWebKitView.addSubview(webView)
        self.view.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        webView.topAnchor.constraint(equalTo: viewDisWebKitView.topAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: viewDisWebKitView.rightAnchor).isActive = true
        webView.leftAnchor.constraint(equalTo: viewDisWebKitView.leftAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: viewDisWebKitView.bottomAnchor).isActive = true
        webView.heightAnchor.constraint(equalTo: viewDisWebKitView.heightAnchor).isActive = true
        webView.uiDelegate = self
        webView.navigationDelegate = self
//        let myURL = URL(string: "http://clientapp.narola.online/PG/QR-Code/RENT-QPRIVACY_POLICY.html")
//        let myRequest = URLRequest(url: myURL!)
//        webView.load(myRequest)

        let htmlFile = Bundle.main.path(forResource: "RENT-Q PRIVACY POLICY", ofType: "html")
        let html = try! String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
        self.webView.loadHTMLString(html, baseURL: nil)
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.view.stopIndicator(indicator: indicator, container: container)
    }
    
}
