//
//  ReserveCarApi.swift
//  GO2
//
//  Created by GNM on 6/12/17.
//  Copyright © 2017 Juan Cardozo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BookingCarApi {

    // MARK: - Singelton
    static let sharedInstance:BookingCarApi = BookingCarApi()
    
    private init() {}
    
    
//    private let pathUrl = general.conexionAPI_Production + general.versionPath
    //private let pathUrl = getConexionAPI_Production() + general.versionPath
    
    
    // MARK: - ApiMethods
    // ********************************************************
    // MARK: Get County, State and cities API Request
    // ********************************************************
    func getLocationRequest(myView: UIView = UIView(), completion: @escaping (LocationData) -> Void) {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
        
        let urlString = getPathUrl() + APIS.getLocationDetails
        print("====== API CALL FOR LOCATION ====== ")
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON { (response) in
                var location : LocationData!
                var errorMessage = ""
                switch response.result {
                case .success:
//                    print("API Connect Successful")
                        
                    if let value: AnyObject = response.result.value as AnyObject? {
                        if value["status"] as? Bool == false {
                            //displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                            myView.stopIndicator(indicator: indicator, container: container)
                        }
                        else{
                            
                            updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                            
                            let result = value["data"] as! NSDictionary

                            let apiReturn = JSON(result)
                            if let object = apiReturn.to(type: LocationData.self){
                                location = object as? LocationData
                            }
                            print("locationData",location)
                            myView.stopIndicator(indicator: indicator, container: container)
                            completion(location)
                        }
                    }
                break
                    
                case .failure( _):
                    if let data = response.data {
                        let responseJSON = JSON(data)
                        
                        if let message: String = responseJSON["errors"].array?[0].rawString() {
                            if !message.isEmpty {
                                errorMessage = message
                            }
                        }
                    }
                    
                    print("ERROR IN CONECTION: \(errorMessage)")
                    //displayMyAlertMessage(userMessage: "Server Connection Failed")
                    myView.stopIndicator(indicator: indicator, container: container)
                break
                }
        }
    }
    
    // ********************************************************
    // MARK: Get Car Categories API Request
    // ********************************************************
    func allCarCategoriesRequest(myView: UIView = UIView(), completion: @escaping ([CarCategoryData]) -> Void) {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
    
        let urlString = getPathUrl() + APIS.getCarsCategory
                        
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON { (response) in
            
            var carType = [CarCategoryData]()
            var errorMessage = ""
            switch response.result {
            case .success:
                print("allCarCategoriesRequest --- API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        //displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                    else {
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        let result = value["data"] as! NSArray
                        
                        let apiReturn = JSON(result)
                        if let object = apiReturn.to(type: CarCategoryData.self){
                            carType = object as! [CarCategoryData]
                        }
                        print(carType)
                        myView.stopIndicator(indicator: indicator, container: container)
                        completion(carType)
                    }
                }
                break
                
            case .failure( _):
                if let data = response.data {
                    let responseJSON = JSON(data)
                    
                    if let message: String = responseJSON["errors"].array?[0].rawString() {
                        if !message.isEmpty {
                            errorMessage = message
                        }
                    }
                }
                
                print("ERROR IN CONECTION: \(errorMessage)")
                //displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
    
    // ********************************************************
    // MARK: Get Car Types API Request
    // ********************************************************
    func allCarTypesRequest(myView: UIView = UIView(), categoryId:Int16, completion: @escaping ([CarTypeData]) -> Void) {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
    
        let urlString = getPathUrl() + APIS.getCarsType + "?" + "vehicle_category_id=\(categoryId)"
        
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON { (response) in
            
            var carType = [CarTypeData]()
            var errorMessage = ""
            switch response.result {
            case .success:
                print("allCarTypesRequest --- API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        //displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                    else {
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        let result = value["data"] as! NSArray
                        
                        let apiReturn = JSON(result)
                        if let object = apiReturn.to(type: CarTypeData.self){
                            carType = object as! [CarTypeData]
                        }
                        print(carType)
                        myView.stopIndicator(indicator: indicator, container: container)
                        completion(carType)
                    }
                }
                break
                
            case .failure( _):
                if let data = response.data {
                    let responseJSON = JSON(data)
                    
                    if let message: String = responseJSON["errors"].array?[0].rawString() {
                        if !message.isEmpty {
                            errorMessage = message
                        }
                    }
                }
                
                print("ERROR IN CONECTION: \(errorMessage)")
                //displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
    
    // ********************************************************
    // MARK: Get Car Parts API Request
    // ********************************************************
    func allCarPartsRequest(myView: UIView = UIView(), typeId:Int, completion: @escaping ([VehicleDetailsPartStatuses]) -> Void) {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
    
        let urlString = getPathUrl() + APIS.getCarParts + "?" + "vehicle_type_id=\(typeId)"
        
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON { (response) in
            
            var carPart = [VehicleDetailsPartStatuses]()
            var errorMessage = ""
            switch response.result {
            case .success:
                print("allCarTypesRequest --- API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        //displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                    else {
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        let result = value["data"] as! NSArray
                        
                        let apiReturn = JSON(result)
                        if let object = apiReturn.to(type: VehicleDetailsPartStatuses.self){
                            carPart = object as! [VehicleDetailsPartStatuses]
                        }
                        print(carPart)
                        myView.stopIndicator(indicator: indicator, container: container)
                        completion(carPart)
                    }
                }
                break
                
            case .failure( _):
                if let data = response.data {
                    let responseJSON = JSON(data)
                    
                    if let message: String = responseJSON["errors"].array?[0].rawString() {
                        if !message.isEmpty {
                            errorMessage = message
                        }
                    }
                }
                
                print("ERROR IN CONECTION: \(errorMessage)")
                //displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
    
    // ********************************************************
    // MARK: Search Cars API Request
    // ********************************************************
    func searchCarsRequest(myView: UIView, city:String, state:String, country:String, lat:Double, lng:Double, start_date:String, end_date:String, car_type: String,ride_type:String, completion: @escaping ([SearchVehicleData]) -> Void)  {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
//        state=San Cristóbal&country=Dominican Republic&
        
        let parameters : [String : AnyObject] = [
            "start_date": start_date as AnyObject,
            "end_date": end_date as AnyObject,
            "city": city as AnyObject,
//            "car_type": car_type as AnyObject,
//            "ride_type": ride_type as AnyObject,
            "vehicle_type_id": ride_type as AnyObject,
            "state":state as AnyObject,
            "country":country as AnyObject
//            "lat":lat as AnyObject,
//            "lng":lng as AnyObject
        ]
        
        let Auth_header = [ "access-token": getAccessToken(),
                            "client": getClientToken(),
                            "uid": getUID() ]
        
        print("parameters: \(parameters)")
        
        let urlString = getPathUrl() + APIS.searchCarsRequest
        
        Alamofire.request(urlString, method: .get, parameters: parameters, headers: Auth_header).validate().responseJSON{
            response in
            var searchResult = [SearchVehicleData]()
            
            switch response.result {
                case .success:
                    print("searchCarsRequest --- API Connect Successful")
                    
                    if let value: AnyObject = response.result.value as AnyObject? {
                        
                        if value["status"] as? Bool == false {
                            displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                            myView.stopIndicator(indicator: indicator, container: container)
                        }
                        else {
                            updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                            let result = value["data"] as! NSArray
                            
                            let apiReturn = JSON(result)
                            if let object = apiReturn.to(type: SearchVehicleData.self){
                                searchResult = object as! [SearchVehicleData]
                            }
                            print(searchResult)
                            myView.stopIndicator(indicator: indicator, container: container)
                            completion(searchResult)
                        }
                    }
                break
                
                case .failure(let error):
                    print("ERROR IN CONECTION: \(error)")
                    displayMyAlertMessage(userMessage: "Server Connection Failed")
                    myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
    
        // ********************************************************
        // MARK: Make/accept Payment Cars API Request
        // ********************************************************
        func acceptPaymentsRequest(myView: UIView, amount:String, data:String, completion: @escaping (MakePaymentBaseClass) -> Void)  {
    
            myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)

            let Auth_header = [ "access-token": getAccessToken(),
                                "client": getClientToken(),
                                "uid": getUID() ]
    
            print("parameters: \(Auth_header)")
    
            let urlString = getPathUrl() + APIS.acceptPaymentPath
            var transectionResult : MakePaymentBaseClass!
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in

                multipartFormData.append("apple".data(using: .utf8)!, withName: "platform")
                multipartFormData.append(amount.data(using: .utf8)!, withName: "amount")
                multipartFormData.append(data.data(using: .utf8)!, withName: "data")

            }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: Auth_header) { encodingResult in
                switch encodingResult {
                case .success(let upload, _,_ ):
                    upload.responseJSON { response in
                        if let value: AnyObject = response.result.value as AnyObject? {
                            
                            if let errors = value["error"] as? String {
                                    displayMyAlertMessage(userMessage: errors)
                                    myView.stopIndicator(indicator: indicator, container: container)
                            }
                            else {
                                updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                                let result = value as! NSDictionary
                                
                                let apiReturn = JSON(result)
                                if let object = apiReturn.to(type: MakePaymentBaseClass.self){
                                    transectionResult = object as! MakePaymentBaseClass
                                }
                                print(transectionResult)
                                myView.stopIndicator(indicator: indicator, container: container)
                                completion(transectionResult)
                            }
                        }
                        else {
                            displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
                            myView.stopIndicator(indicator: indicator, container: container)
                        }
                    }
                    break
                case .failure(let encodingError):
                    print("ERROR IN CONECTION: \(encodingError)")
                    displayMyAlertMessage(userMessage: "Server Connection Failed")
                    myView.stopIndicator(indicator: indicator, container: container)
                    break
                    
                }
            }
    }
    
    // ********************************************************
    // MARK: Reserve Car API Request
    // ********************************************************
    func reserveCarRequest(myView: UIView, startDate:String, endDate:String,cityName:String,carType:String,carID:String, completion: @escaping (BookingData) -> Void)  {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header = [ "access-token": getAccessToken(),
                            "client": getClientToken(),
                            "uid": getUID() ]
        
        print("parameters: \(Auth_header)")
        
        let urlString = getPathUrl() + APIS.reserveCarPath
        var bookingResult : BookingData!
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(startDate.data(using: .utf8)!, withName: "start_date")
            multipartFormData.append(endDate.data(using: .utf8)!, withName: "end_date")
            multipartFormData.append(cityName.data(using: .utf8)!, withName: "city")
            multipartFormData.append(carType.data(using: .utf8)!, withName: "car_type")
            multipartFormData.append(carID.data(using: .utf8)!, withName: "car_id")
            
            
        }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: Auth_header) { encodingResult in
            switch encodingResult {
            case .success(let upload, _,_ ):
                upload.responseJSON { response in
                    if let value: AnyObject = response.result.value as AnyObject? {
                        
                        if value["status"] as? Bool == false {
                            displayMyAlertMessage(userMessage: (value["errors"] as? String)!)
                            myView.stopIndicator(indicator: indicator, container: container)
                        }
                        else {
                            updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                            let result = value["data"] as! NSDictionary
                            
                            let apiReturn = JSON(result)
                            if let object = apiReturn.to(type: BookingData.self){
                                bookingResult = object as! BookingData
                            }
                            print(bookingResult)
                            myView.stopIndicator(indicator: indicator, container: container)
                            completion(bookingResult)
                        }
                    }
                    else {
                        displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                }
                break
            case .failure(let encodingError):
                print("ERROR IN CONECTION: \(encodingError)")
                displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                break
                
            }
        }
    }
    
}
