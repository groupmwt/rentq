//
//  UITextfieldValidation.swift
//  GO2
//
//  Created by NC2-46 on 10/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit
    
    // MARK: - Enums
public extension UITextField {
    
    /// SwifterSwift: UITextField text type.
    ///
    /// - emailAddress: UITextField is used to enter email addresses.
    /// - password: UITextField is used to enter passwords.
    /// - generic: UITextField is used to enter generic text.
    public enum TextType {
        /// UITextField is used to enter email addresses.
        case emailAddress
        
        /// UITextField is used to enter passwords.
        case password
        
        /// UITextField is used to enter phone number.
        case phone
        
        /// UITextField is used to enter generic text.
        case generic
    }
    
}

// MARK: - Properties
public extension UITextField {
    
    /// SwifterSwift: Set textField for common text types.
    public var textType: TextType {
        get {
            if keyboardType == .emailAddress {
                return .emailAddress
            } else if isSecureTextEntry {
                return .password
            }   else if keyboardType == .phonePad {
                return .phone
            }
            return .generic
        }
        set {
            switch newValue {
            case .emailAddress:
                keyboardType = .emailAddress
                autocorrectionType = .no
                autocapitalizationType = .none
                isSecureTextEntry = false
                //placeholder = self.placeholder
                
            case .password:
                keyboardType = .asciiCapable
                autocorrectionType = .no
                autocapitalizationType = .none
                isSecureTextEntry = true
                //placeholder = self.placeholder
            case .phone:
                keyboardType = .phonePad
                autocorrectionType = .no
                autocapitalizationType = .none
                isSecureTextEntry = false
            case .generic:
                isSecureTextEntry = false
            }
        }
    }
    
    /// SwifterSwift: Check if text field is empty.
    public var isEmpty: Bool {
        if self.text != nil {
            let text =  self.text!.trimmingCharacters(in: NSCharacterSet.whitespaces)
            if text.count < 1 {
                return true
            }
        }
        else {
            return true
        }
        
        return text?.isEmpty == true
    }
    /// SwifterSwift: Check if text field is empty.
    public var isValidMobileNumber: Bool {
        if text!.count > 5 && text!.count < 16 {
            return true
        }
        return false
    }
    public var isValidPassword: Bool {
        if text!.count > 5 {
            return true
        }
        return false
    }
    /// SwifterSwift: Return text with no spaces or new lines in beginning and end.
    public var trimmedText: String? {
        return text?.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    /// SwifterSwift: Check if textFields text is a valid email format.
    ///
    ///        textField.text = "john@doe.com"
    ///        textField.hasValidEmail -> true
    ///
    ///        textField.text = "swifterswift"
    ///        textField.hasValidEmail -> false
    ///
    public var hasValidEmail: Bool {
        // http://stackoverflow.com/questions/25471114/how-to-validate-an-e-mail-address-in-swift
        return text!.range(of: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}",
                           options: String.CompareOptions.regularExpression,
                           range: nil, locale: nil) != nil
    }
        
}

// MARK: - Methods
public extension UITextField {
    
    /// SwifterSwift: Clear text.
    public func clear() {
        text = ""
        attributedText = NSAttributedString(string: "")
    }
    
    /// SwifterSwift: Set placeholder text color.
    ///
    /// - Parameter color: placeholder text color.
    public func setPlaceHolderTextColor(_ color: UIColor) {
        guard let holder = placeholder, !holder.isEmpty else { return }
        
        
        let attributes = [NSAttributedStringKey.foregroundColor : color]
        //attributes[NSAttributedStringKey.foregroundColor : color] = color

        self.attributedPlaceholder = NSAttributedString(string: holder, attributes: attributes)
    }
    
    /// SwifterSwift: Add padding to the left of the textfield rect.
    ///
    /// - Parameter padding: amount of padding to apply to the left of the textfield rect.
    public func addPaddingLeft(_ padding: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: frame.height))
        leftView = paddingView
        leftViewMode = .always
    }
    
    /// SwifterSwift: Add padding to the left of the textfield rect.
    ///
    /// - Parameters:
    ///   - image: left image
    ///   - padding: amount of padding between icon and the left of textfield
    public func addPaddingLeftIcon(_ image: UIImage, padding: CGFloat) {
        let imageView = UIImageView(image: image)
        imageView.contentMode = .center
        self.leftView = imageView
        self.leftView?.frame.size = CGSize(width: image.size.width + padding, height: image.size.height)
        self.leftViewMode = UITextFieldViewMode.always
    }
    
}
