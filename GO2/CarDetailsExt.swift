//
//  CarDetailsExt.swift
//  GO2
//
//  Created by NC2-46 on 12/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import UIKit

extension CarDetailsVC
{
    func setupUI()
    {
        labelBarTitle.text = headerTitle
        self.title = navigationTitle
        //Style
        navigationController?.customNavBar()
        setBackBtn()
        btnEditPayment.backgroundColor = Color.buttonBG
        btnDelete.backgroundColor = Color.buttonBG
        globalView.setBackgroundWithTopBarToView()
        headerBar.backgroundColor = Color.SubHeaderColor
        labelBarTitle.setTextHeader()
        self.collOwnerCars.delegate = self
        self.collOwnerCars.dataSource = self
        
        if isMyVehicleDetails {
            //show my car details
            btnDelete.isHidden = false
            btnEditPayment.setTitle("Edit", for: .normal)
            self.vwOwnerCarList.isHidden = true
            self.collOwnerCars.isHidden = true
            self.heightOfOwnerList.constant = 0
            self.heightOfOwnerImgIcon.constant = 0
            self.iconOwnerCar.isHidden = true
            self.lblOwnerCarTitle.isHidden = true
            //            MyVehiclesApi.sharedInstance.getVehicleDetailsRequest(myView: self.view, carID: "\(myVehicleData.id ?? 0)", completion: { (vehicleDetails) in
//                self.myVehicleData = vehicleDetails[0]
//                self.vwPager.numberOfPages = self.myVehicleData.images?.count ?? 0
////                let finalUrl = getConexionAPI_Production() + (self.myVehicleData.images?[0].url ?? "")!
////                let url = URL.init(string: finalUrl)
////                self.imgProfile.sd_setShowActivityIndicatorView(true)
////                self.imgProfile.sd_setIndicatorStyle(.gray)
////                self.imgProfile.sd_setImage(with: url , placeholderImage: nil)
//                self.brandData = self.myVehicleData.brand
//                self.modelData = self.myVehicleData.model
//                self.plateData = self.myVehicleData.plate
//                self.vehicleTypeData = self.myVehicleData.vehicleType
//                self.insuranceIdData = "\(self.myVehicleData.insuranceId ?? 0)"
//                self.insuranceCompanyData = self.myVehicleData.insuranceCompany
//                self.insuranceExpityData = self.myVehicleData.insuranceExpiryDate
//                self.chassisNumberData = self.myVehicleData.chassisNumber
//                self.registrationNumberData = self.myVehicleData.registrationNumber
//                self.noOfSeatsData = "\(self.myVehicleData.numberOfSeats ?? 0)"
//                self.tractionData = self.myVehicleData.typeOfTraction
//                self.vehiclePartsData = self.myVehicleData.vehiclePartStatuses!//                self.vehicleTypeData = vehicleDetails.carType
////                self.rideTypeData = vehicleDetails.rideType
//                self.locationData = self.myVehicleData.address
//                self.countryData = self.myVehicleData.country
//                self.stateData = self.myVehicleData.state
//                self.cityData = self.myVehicleData.city
//                self.tableView.reloadData()
//            })
            self.getCarDetails(carID: "\(myVehicleData.id ?? 0)")
//            getCarListOfSameOwner(carID: "\(myVehicleData.id ?? 0)")
        }
//        else if isDataComeFromNearbyVehicle{
//            // show car details for payment
//            btnDelete.isHidden = true
//            btnEditPayment.setTitle("Make Payment", for: .normal)
//            MyVehiclesApi.sharedInstance.getVehicleDetailsRequest(myView: self.view, carID: "\(self.vehicleId!)", completion: { (vehicleDetails) in
//                self.myVehicleData = vehicleDetails[0]
//                let vehicleDetails = vehicleDetails[0]
//                self.vwPager.numberOfPages = self.myVehicleData.images?.count ?? 0
////                let finalUrl = getConexionAPI_Production() + (vehicleDetails.images?[0].url ?? "")!
////                let url = URL.init(string: finalUrl)
////                self.imgProfile.sd_setShowActivityIndicatorView(true)
////                self.imgProfile.sd_setIndicatorStyle(.gray)
////                self.imgProfile.sd_setImage(with: url , placeholderImage: nil)
//                self.brandData = vehicleDetails.brand
//                self.modelData = vehicleDetails.model
//                self.plateData = vehicleDetails.plate
//                self.vehicleTypeData = vehicleDetails.vehicleType
//                self.insuranceIdData = "\(vehicleDetails.insuranceId ?? 0)"
//                self.insuranceCompanyData = vehicleDetails.insuranceCompany
//                self.insuranceExpityData = vehicleDetails.insuranceExpiryDate
//                self.chassisNumberData = vehicleDetails.chassisNumber
//                self.registrationNumberData = vehicleDetails.registrationNumber
//                self.noOfSeatsData = "\(vehicleDetails.numberOfSeats ?? 0)"
//                self.tractionData = vehicleDetails.typeOfTraction
//                self.vehiclePartsData = vehicleDetails.vehiclePartStatuses!
////                self.vehicleTypeData = vehicleDetails.carType
////                self.rideTypeData = vehicleDetails.rideType
//                self.locationData = vehicleDetails.address
//                self.countryData = vehicleDetails.country
//                self.stateData = vehicleDetails.state
//                self.cityData = vehicleDetails.city
//                self.tableView.reloadData()
//            })
//        }
        else {
            // show car details for payment
            btnDelete.isHidden = true
            btnEditPayment.setTitle("Make Payment", for: .normal)
            self.vwOwnerCarList.isHidden = false
            self.collOwnerCars.isHidden = false
            self.heightOfOwnerList.constant = 120
            self.heightOfOwnerImgIcon.constant = 27
            self.iconOwnerCar.isHidden = false
            self.lblOwnerCarTitle.isHidden = false
//            var carId : String?
//            if isDataComeFromNearbyVehicle{
//                carId = "\(self.vehicleId ?? 0)"
//            }else{
//                carId = "\(searchVehicleData.id!)"
//            }
//            MyVehiclesApi.sharedInstance.getVehicleDetailsRequest(myView: self.view, carID: carId ?? "0", completion: { (vehicleDetails) in
//                self.myVehicleData = vehicleDetails[0]
//                let vehicleDetails = vehicleDetails[0]
//                let finalUrl = getConexionAPI_Production() + (vehicleDetails.images?[0].url ?? "")!
//                let url = URL.init(string: finalUrl)
////                self.imgProfile.sd_setShowActivityIndicatorView(true)
////                self.imgProfile.sd_setIndicatorStyle(.gray)
////                self.imgProfile.sd_setImage(with: url , placeholderImage: nil)
//                self.brandData = vehicleDetails.brand
//                self.modelData = vehicleDetails.model
//                self.plateData = vehicleDetails.plate
//                self.vehicleTypeData = vehicleDetails.vehicleType
//                self.insuranceIdData = "\(vehicleDetails.insuranceId ?? 0)"
//                self.insuranceCompanyData = vehicleDetails.insuranceCompany
//                self.insuranceExpityData = vehicleDetails.insuranceExpiryDate
//                self.chassisNumberData = vehicleDetails.chassisNumber
//                self.registrationNumberData = vehicleDetails.registrationNumber
//                self.noOfSeatsData = "\(vehicleDetails.numberOfSeats ?? 0)"
//                self.tractionData = vehicleDetails.typeOfTraction
//                self.vehiclePartsData = vehicleDetails.vehiclePartStatuses!
////                self.vehicleTypeData = vehicleDetails.carType
////                self.rideTypeData = vehicleDetails.rideType
//                self.locationData = vehicleDetails.address
//                self.countryData = vehicleDetails.country
//                self.stateData = vehicleDetails.state
//                self.cityData = vehicleDetails.city
//                self.tableView.reloadData()
//            })
            getCarDetails(carID: isDataComeFromNearbyVehicle ? "\(self.vehicleId ?? 0)" : "\(searchVehicleData.id!)")
            getCarListOfSameOwner(carID:  isDataComeFromNearbyVehicle ? "\(self.vehicleId ?? 0)" : "\(searchVehicleData.id!)")
        }
        
    }
    
    func getCarDetails(carID:String){
        MyVehiclesApi.sharedInstance.getVehicleDetailsRequest(myView: self.view, carID: carID, completion: { (vehicleDetails) in
            self.myVehicleData = vehicleDetails[0]
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.allowsSelection = false
            self.tableView.tableFooterView = UIView()
            self.tblVIewHeight.constant = CGFloat(((VehicleDetailsEnum.totalValues - 1) * 60) + 100)
            
            self.collImages.delegate = self
            self.collImages.dataSource = self
            self.collImages.reloadData()
            self.vwPager.numberOfPages = self.myVehicleData.images?.count ?? 0
//                let finalUrl = getConexionAPI_Production() + (self.myVehicleData.images?[0].url ?? "")!
//                let url = URL.init(string: finalUrl)
//                self.imgProfile.sd_setShowActivityIndicatorView(true)
//                self.imgProfile.sd_setIndicatorStyle(.gray)
//                self.imgProfile.sd_setImage(with: url , placeholderImage: nil)
            self.brandData = self.myVehicleData.brand
            self.modelData = self.myVehicleData.model
            self.plateData = self.myVehicleData.plate
            self.vehicleTypeData = self.myVehicleData.vehicleType
            self.insuranceIdData = "\(self.myVehicleData.insuranceId ?? 0)"
            self.insuranceCompanyData = self.myVehicleData.insuranceCompany
            self.insuranceExpityData = self.myVehicleData.insuranceExpiryDate
            self.chassisNumberData = self.myVehicleData.chassisNumber
            self.registrationNumberData = self.myVehicleData.registrationNumber
            self.noOfSeatsData = "\(self.myVehicleData.numberOfSeats ?? 0)"
            self.tractionData = self.myVehicleData.typeOfTraction
            self.vehiclePartsData = self.myVehicleData.vehiclePartStatuses!//                self.vehicleTypeData = vehicleDetails.carType
//                self.rideTypeData = vehicleDetails.rideType
            self.locationData = self.myVehicleData.address
            self.countryData = self.myVehicleData.country
            self.stateData = self.myVehicleData.state
            self.cityData = self.myVehicleData.city
            self.tableView.reloadData()
        })
    }
 
    func getCarListOfSameOwner(carID:String){
        MyVehiclesApi.sharedInstance.getVehicleListOfOwnerRequest(myView: self.view, carID: carID, completion: { (vehicleListData) in
            self.arrOwnerVehicleList = vehicleListData
            if self.arrOwnerVehicleList.count > 0{
                self.vwOwnerCarList.isHidden = false
                self.collOwnerCars.isHidden = false
                self.heightOfOwnerList.constant = 120
                self.heightOfOwnerImgIcon.constant = 27
                self.iconOwnerCar.isHidden = false
                self.lblOwnerCarTitle.isHidden = false
                self.collOwnerCars.reloadData()
            }else{
                self.vwOwnerCarList.isHidden = true
                self.collOwnerCars.isHidden = true
                self.heightOfOwnerList.constant = 0
                self.heightOfOwnerImgIcon.constant = 0
                self.iconOwnerCar.isHidden = true
                self.lblOwnerCarTitle.isHidden = true
            }
        })
    }
    
}

//MARK: - TableView Data Source and delegate Method
extension CarDetailsVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VehicleDetailsEnum.totalValues
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailsTVC") as! CarDetailsTVC

        switch indexPath.row {
        case VehicleDetailsEnum.Model.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "model"),rowTitle: "Model".uppercased(), rowData: modelData, tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.Brand.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "brand"), rowTitle: "Brand".uppercased(), rowData: brandData, tagvalue: indexPath.row, numLines: 1)
            return cell
        
        case VehicleDetailsEnum.Plate.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "plate"), rowTitle: "Plate".uppercased(), rowData: plateData ,tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.VehicleType.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "vehicleType"), rowTitle: "Vehicle Type".uppercased(), rowData: vehicleTypeData ,tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.InsuranceId.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "insuranceId"), rowTitle: "Insurance ID".uppercased(), rowData: insuranceIdData ,tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.InsuranceCompany.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "insuranceCompany"), rowTitle: "Insurance Company".uppercased(), rowData: insuranceCompanyData ,tagvalue: indexPath.row, numLines: 1)
            
            return cell
            
        case VehicleDetailsEnum.InsuranceExpiry.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "ExpiryDate"), rowTitle: "Insurance Expiry".uppercased(), rowData: insuranceExpityData ,tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.ChassisNumber.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "chassisNumber"), rowTitle: "Chassis Number".uppercased(), rowData: chassisNumberData ,tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.RegistrationNumber.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "RegistrationNumber"), rowTitle: "Registration Number".uppercased(), rowData: registrationNumberData ,tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.NumberOfSeats.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "seats"), rowTitle: "No. of Seats".uppercased(), rowData: noOfSeatsData ,tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.Traction.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "vehicleType"), rowTitle: "Traction".uppercased(), rowData: tractionData ,tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.VehicleParts.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellVehiclePartDetails") as! CellVehiclePartDetails
            cell.vehiclePartsData = vehiclePartsData
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "part"), rowTitle: "Vehice Parts".uppercased(), rowData: vehiclePartsData ,tagvalue: indexPath.row, numLines: 1)
            return cell
            
            //        case VehicleDetailsEnum.VehicleType.rawValue:
//            cell.configureCell(imgLogo: #imageLiteral(resourceName: "vehicleType"),rowTitle: "Vehicle Type".uppercased(), rowData: vehicleTypeData, tagvalue: indexPath.row, numLines: 1)
//
//            return cell
//        case VehicleDetailsEnum.RideType.rawValue:
//            cell.configureCell(imgLogo: #imageLiteral(resourceName: "rideType"),rowTitle: "Ride Type".uppercased(), rowData: rideTypeData?.replacingOccurrences(of: "_", with: ""), tagvalue: indexPath.row, numLines: 1)
//
//            return cell
        case VehicleDetailsEnum.Country.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "country"), rowTitle: "Country".uppercased(), rowData: countryData, tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.State.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "State"), rowTitle: "State".uppercased(), rowData: stateData, tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.City.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "city"), rowTitle: "City".uppercased(), rowData: cityData, tagvalue: indexPath.row, numLines: 1)
            return cell
            
        case VehicleDetailsEnum.Location.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "address"), rowTitle: "Address".uppercased(), rowData: locationData, tagvalue: indexPath.row, numLines: 5)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case VehicleDetailsEnum.Location.rawValue:
            return 80
        case VehicleDetailsEnum.VehicleParts.rawValue:
            return 100
        default:
            return 60
        }
    }
    
}

extension CarDetailsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == collImages ? myVehicleData.images?.count ?? 0 : arrOwnerVehicleList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collImages{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellVehicleDetailImages", for: indexPath as IndexPath) as! CellVehicleDetailImages
            cell.imgVehicles.contentMode = .scaleAspectFit
            cell.imgVehicles.clipsToBounds = true
            let finalUrl = getConexionAPI_Production() + (self.myVehicleData.images?[indexPath.item].url ?? "")!
            let url = URL.init(string: finalUrl)
            cell.imgVehicles.sd_setShowActivityIndicatorView(true)
            cell.imgVehicles.sd_setIndicatorStyle(UIActivityIndicatorViewStyle.medium)
            cell.imgVehicles.sd_setImage(with: url , placeholderImage: nil)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellOwnerCarDetail", for: indexPath as IndexPath) as! CellOwnerCarDetail
            let objCar = arrOwnerVehicleList[indexPath.item]
            cell.imgViewCar.contentMode = .scaleAspectFit
            cell.imgViewCar.clipsToBounds = true
            cell.imgViewCar.layer.cornerRadius = 8
            let finalUrl = objCar.image ?? ""
            let url = URL.init(string: finalUrl)
            cell.imgViewCar.sd_setShowActivityIndicatorView(true)
//            cell.imgViewCar.sd_setIndicatorStyle(UIActivityIndicatorViewStyle.medium)
            cell.imgViewCar.sd_setImage(with: url , placeholderImage: #imageLiteral(resourceName: "AppIcon"))
            
            cell.lblCarModel.text = objCar.model
            cell.lblCarBrand.text = objCar.brand
            cell.lblCarType.text = objCar.vehicleType
//            cell.vwShadow.clipsToBounds = true
//            cell.vwShadow.layer.cornerRadius = 8
            cell.vwShadow.layer.cornerRadius = 15
            cell.vwShadow.layer.shadowColor = UIColor.gray.cgColor
            cell.vwShadow.layer.shadowOpacity = 0.5
            cell.vwShadow.layer.shadowOffset = CGSize.zero
            cell.vwShadow.layer.shadowRadius = 3

            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if collectionView == collImages{
            return CGSize(width: collImages.frame.size.width, height: collImages.frame.size.height)
        }else{
            return CGSize(width: 230, height: 100)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        if collectionView == collImages{
            return UIEdgeInsetsMake(0, 0, 0, 0)
        }else{
            return UIEdgeInsetsMake(10, 10, 10, 10)
        }
                
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView == collImages ? 0 : 10
    }
    
    func collectionView(_ collectionView: UICollectionView,
            layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView == collImages ? 0 : 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView == collOwnerCars{
            self.getCarDetails(carID: "\(self.arrOwnerVehicleList[indexPath.item].id ?? 0)")
            self.getCarListOfSameOwner(carID: "\(self.arrOwnerVehicleList[indexPath.item].id ?? 0)")
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = collImages.contentOffset
        visibleRect.size = collImages.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = collImages.indexPathForItem(at: visiblePoint) else { return }
        vwPager.currentPage = indexPath.item
    }
}

//extension CarDetailsVC : UIScrollViewDelegate{
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let pageNumber = scrollViewImages.contentOffset.x / scrollViewImages.frame.size.width
//            vwPager.currentPage = Int(pageNumber)
//        let finalUrl = getConexionAPI_Production() + (self.myVehicleData.images?[Int(pageNumber)].url ?? "")!
//        let url = URL.init(string: finalUrl)
//        self.imgProfile.sd_setShowActivityIndicatorView(true)
//        self.imgProfile.sd_setIndicatorStyle(.gray)
//        self.imgProfile.sd_setImage(with: url , placeholderImage: nil)
//    }
//}
