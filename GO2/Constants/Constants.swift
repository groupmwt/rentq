//
//  Constants.swift
//  Motiv8
//
//  Created by NC2-46 on 26/03/18.
//  Copyright © 2018 NC2-46. All rights reserved.
//

import UIKit
import MMDrawerController
import CountryPickerView

let AppDel = UIApplication.shared.delegate as! AppDelegate
let addressPlaceholder = "Pick a Address"
let kDToken = "DToken"
let kAccessToken = "access-token"
let kClient = "client"
let kUid = "uid"
let kIsUserLoggedIn = "isUserLoggedIn"
let kConexionAPI_Production = "conexionAPI_Production"
let ENTITY_VEHICLE_TYPE = "CD_Vehicle_Type"
let ENTITY_VEHICLE_Category = "CD_Vehicle_Category"
let ENTITY_COUNTRY = "CD_Country"
let ENTITY_CITY = "CD_City"
let ENTITY_RIDE_TYPE = "CD_Ride_Type"
let ENTITY_STATE = "CD_State"
let kSetting = "isFromMenu"
struct general{
    static let fact5: Float = 1.25
    static let factPlus: Float = 0.75
    static let versionPath: String = "/v1"
    
}

public func getConexionAPI_Production() -> String {
//    var conexionAPI_Production = "http://rent-q.com:8080"
    var conexionAPI_Production = "https://rent-q.com"
//    var co12nexionAPI_Production = "http://192.168.1.240:3002" //"http://192.168.100.81:3001" //http://rent-q.com:8080"//"https://372846b8.ngrok.io"
    if let value = UserDefaults.standard.object(forKey: kConexionAPI_Production) as? String{
        conexionAPI_Production = value
    }
    return conexionAPI_Production
}

public func setConexionAPI_Production(url : String) {
    UserDefaults.standard.set(url, forKey: kConexionAPI_Production)
    
}
public func getPathUrl() -> String {
    let pathUrl = getConexionAPI_Production() + general.versionPath
    return pathUrl
}

struct Color {
    
    static let BarColor = UIColor(red:0.99, green:0.98, blue:0.98, alpha:1.0)
    static let SubBarColor = UIColor(red:0.71, green:0.70, blue:0.71, alpha:1.0)
    static let SubBarClear = UIColor(red:0.71, green:0.70, blue:0.71, alpha:0.5)
    static let BarTextColor = UIColor(red:0.39, green:0.39, blue:0.39, alpha:1.0)
    static let FooterColor = UIColor(red:0.24, green:0.65, blue:0.81, alpha:1.0)
    static let HeaderColor = UIColor(red:0.00, green:0.00, blue:0.60, alpha:1.0)
    static let SubHeaderColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
    static let TextfieldBorder = UIColor(red:0.77, green:0.76, blue:0.73, alpha:1.0)
    static let TextGrey = UIColor(red:0.40, green:0.40, blue:0.40, alpha:1.0)
    static let TextClear = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
    static let TextBlue = UIColor(red:0.11, green:0.44, blue:0.72, alpha:1.0)
    static let TextYellow = UIColor(red:255/255, green:251/255, blue:0/255, alpha:1.0)
    static let TextLight = UIColor(red:255/255, green:255/255, blue: 255/255, alpha: 0.6)
//    static let GradientMenuStart = UIColor(red:0.00, green:0.00, blue:0.60, alpha:1.0)
    static let GradientMenuStart = UIColor(red:5/255, green:35/255, blue:44/255, alpha:1.0)
    static let GradientMenuEnd = UIColor(red:0.00, green:0.00, blue:1.00, alpha:1.0)
    static let GradientSubViewStart = UIColor(red:0.73, green:0.74, blue:0.73, alpha:1.0)
    static let GradientSubViewEnd = UIColor(red:0.70, green:0.70, blue:0.69, alpha:1.0)
    static let GradientPopUpStart = UIColor(red:0.95, green:0.95, blue:0.93, alpha:1.0)
    static let GradientPopUpEnd = UIColor(red:0.77, green:0.77, blue:0.76, alpha:1.0)
    static let DataViewBG = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.0)
    static let buttonBG = UIColor(red: 205/255, green: 0/255, blue: 29/255, alpha: 1.0)
    static let screenBG = UIColor(red:5/255, green:35/255, blue:44/255, alpha:1.0)
    static let DividerGreen = UIColor(red:0.66, green:0.85, blue:0.68, alpha:1.0)
    static let ChatG1 = UIColor(red:0.76, green:0.76, blue:0.77, alpha:0.8)
    static let ChatG2 = UIColor(red:0.74, green:0.74, blue:0.75, alpha:0.8)
    static let ChatG3 = UIColor(red:0.72, green:0.72, blue:0.73, alpha:0.8)
    static let MenuBackgroundColor = UIColor(red:32.0, green:1.0, blue:255.0, alpha:1.0)
    static let NavigationBlue = UIColor(red:0.0, green:0.0, blue:1, alpha:1.0)
    static let textfiledTextColor = UIColor(red:85/255, green:85/255, blue:85/255, alpha:1.0)
    static let textfiledPlaceholderColor = UIColor(red:170/255, green:170/255, blue:170/255, alpha:1.0)
    
}

struct APIS {
    static let loginPath = "/auth/sign_in"
    static let registerPath = "/auth"
    static let forgotPasswordPath = "/users/reset_password"
    static let changePasswordPath = "/auth/password"
    static let getUserDetailsPath = "/users/get_user_details"
    static let getNearbyVehiclesPath = "/get_nearby_vehicles"
    static let getUpdateProfilePath = "/profile"
    static let logoutPath = "/auth/sign_out"

    static let carPath = "/cars"
    static let addNewCar = "/vehicles"
//    static let addNewCar = "/cars"
    static let getAllMyCars = "/vehicles"
    static let deleteMyCar = "/cars"
    static let editMyCar = "/cars"
    static let carList = "/list"
    static let getLocationDetails = "/get_locations.json"
    static let getCarsType = "/vehicle_types"
    static let getCarParts = "/vehicle_parts"
    static let getCarsCategory = "/vehicle_categories"
    static let searchCarsRequest = "/vehicles/search"
//    static let searchCarsRequest = "/cars/search.json?"
    static let myRents = "/cars/get_my_cars_rents.json"
    static let myReservations = "/cars/get_my_reservations.json"
    static let acceptPaymentPath = "/authorize_accept_payments"
    static let reserveCarPath = "/cars/reserve"
    static let getOffersCalendarPath = "/get_offers_calendar.json"
    static let addOffersCalendarPath = "/generate_offers.json"
    static let updateOffersCalendarPath = "/update_offer_price"
    static let deleteOffersCalendarPath = "/remove_offers.json"
    static let checkAppVersion = "/appVersion"
    static let getStates = "/get_states"
    static let getCitiesByStates = "/get_cities_by_states"
}
//MARK: - Activity Indicator
let indicator:UIActivityIndicatorView = UIActivityIndicatorView  (activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
let container: UIView = UIView()
let loadingView: UIView = UIView()



struct PaymentDetails {

    static let CLIENT_NAME : String = "Joe"
    static let CLIENT_KEY : String = "44agbD9ecL2ydc9s7ff7cwSQ2jmHR6h4Rc2mz8B87MNNDX2ueh47PL8kPZ36RNCa"
    
    //            "2G6nr8PFceGsgj7d9cb8Zc8TK62xuA8YLgrh6YXPK9S47gN2ueJyH535WBDD59tq"; // production
    //"44agbD9ecL2ydc9s7ff7cwSQ2jmHR6h4Rc2mz8B87MNNDX2ueh47PL8kPZ36RNCa"; // sandbox
    
    static let API_LOGIN_ID : String = "9p9YZb2yX"
    static let cardNumberDemo : String = "4111 1111 1111 1111"
        
    //            "34kyuM29Yw"; // production
   // "9p9YZb2yX"; // sandbox
   // card number : 4111 1111 1111 1111
    
}


func isUserLogin() -> Bool{
    if (UserDefaults.standard.bool(forKey: kIsUserLoggedIn) == true) {
        return true
    }
    return false
}

func getUID() -> String{
    if (UserDefaults.standard.string(forKey: kUid) != nil) {
        return UserDefaults.standard.string(forKey: kUid)!
    }
    return ""
}

func getAccessToken() -> String{
    if (UserDefaults.standard.string(forKey: kAccessToken) != nil) {
        return UserDefaults.standard.string(forKey: kAccessToken)!
    }
    return ""
}

func getClientToken() -> String{
    if (UserDefaults.standard.string(forKey: kClient) != nil) {
        return UserDefaults.standard.string(forKey: kClient)!
    }
    return ""
}


func getDeviceToken() -> String{
    var DToken : String = "123456"
    if let token = UserDefaults.standard.object(forKey: kDToken) as? String{
        DToken = token
    }
    return DToken
}

func removeUserLogin(viewController : UIViewController){
    
    UserApi.sharedInstance.logoutRequest(myView: viewController.view) { (hasError) in
 
        let Dtoken = getDeviceToken()
        
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        
        print("device",Dtoken)
        UserDefaults.standard.set(Dtoken, forKey: kDToken)
        let vc = viewController.loadViewController(StoryBoardName: "Main", VCIdentifer: "LoginController") as! LoginController
        AppDel.mainNavigationController = UINavigationController.init(rootViewController: vc)
        AppDel.window?.rootViewController = AppDel.mainNavigationController
    }
    
}
