//
//  Label+Extension.swift
//  Motiv8
//
//  Created by NC2-46 on 12/04/18.
//  Copyright © 2018 NC2-46. All rights reserved.
//

import Foundation
import UIKit
extension UILabel {
    func setLableConstraint(imgView:UIImageView) {
        self.bottomAnchor.constraint(equalTo: imgView.bottomAnchor).isActive = true
        
        self.leftAnchor.constraint(equalTo: imgView.leftAnchor, constant: 0).isActive = true
        
        self.rightAnchor.constraint(equalTo: imgView.rightAnchor, constant: 0).isActive = true
    }
}
