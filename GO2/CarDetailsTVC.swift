//
//  CarDetailsTVC1.swift
//  GO2
//
//  Created by NC2-46 on 11/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit

class CarDetailsTVC: UITableViewCell {

    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblData: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgLogo.contentMode = .scaleAspectFit
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(imgLogo: UIImage,rowTitle : String, rowData:String?, tagvalue:Int,numLines : Int) {
        
        self.imgLogo.image = imgLogo
        self.lblTitle.text = rowTitle
        self.lblData.text = rowData
        self.lblData.tag = tagvalue
        self.lblData.textAlignment = .right
        self.lblData.numberOfLines = numLines
    }
}
