//
//  AddressPickerVC.swift
//  GO2
//
//  Created by NC2-46 on 25/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit
import GooglePlaces

//MARK:- Protocol
protocol SelectedAddressDelegete
{
    func getSelectedAddress(pickAddress : GMSPlace)
    
}

class AddressPickerVC: UIViewController, UISearchControllerDelegate {

    var delegateSelectedAddress: SelectedAddressDelegete!
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        searchController?.isActive = true
        searchController?.delegate = self
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        let country = AppDel.country ?? ""
        let state = AppDel.state ?? ""
        let city = AppDel.city ?? ""
        self.searchController?.searchBar.text = city + "," + state + "," + country
        
        DispatchQueue.main.async {
            
            self.searchController?.searchBar.becomeFirstResponder()
        }

        navigationItem.titleView = searchController?.searchBar
        
        if #available(iOS 13.0, *) {
            searchController?.searchBar.searchTextField.textColor = UIColor.white
        } else {
            // Fallback on earlier versions
        }
        
        
        definesPresentationContext = true

        searchController?.hidesNavigationBarDuringPresentation = false
        searchController?.searchBar.placeholder = "Enter a location"
        self.setBackBtn()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.searchController?.searchBar.becomeFirstResponder()
        }
    }

    func presentSearchController(_ searchController: UISearchController) {
        self.searchController?.searchBar.becomeFirstResponder()
    }

}


// Handle the user's selection.
extension AddressPickerVC: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = true
        self.delegateSelectedAddress.getSelectedAddress(pickAddress: place)
        self.backTap()
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
