//
//  OwnerVehicleListData.swift
//  GO2
//
//  Created by C115 on 04/01/22.
//  Copyright © 2022 Juan Cardozo. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class OwnerVehicleListData: NSCoding,JSONable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let model = "model"
        static let brand = "brand"
        static let vehicleType = "vehicle_type"
        static let image = "image"
        static let id = "id"
    }
    
    // MARK: Properties
    public var model: String?
    public var brand: String?
    public var vehicleType: String?
    public var image: String?
    public var id: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        id = json[SerializationKeys.id].int
        model = json[SerializationKeys.model].string
        brand = json[SerializationKeys.brand].string
        image = json[SerializationKeys.image].string
        vehicleType = json[SerializationKeys.vehicleType].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = model { dictionary[SerializationKeys.model] = value }
        if let value = brand { dictionary[SerializationKeys.brand] = value }
        if let value = image { dictionary[SerializationKeys.image] = value }
        if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
        self.brand = aDecoder.decodeObject(forKey: SerializationKeys.brand) as? String
        self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
        self.vehicleType = aDecoder.decodeObject(forKey: SerializationKeys.vehicleType) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(model, forKey: SerializationKeys.model)
        aCoder.encode(brand, forKey: SerializationKeys.brand)
        aCoder.encode(image, forKey: SerializationKeys.image)
        aCoder.encode(vehicleType, forKey: SerializationKeys.vehicleType)
    }
    
}
