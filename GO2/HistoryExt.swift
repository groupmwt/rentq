//
//  HistoryExt.swift
//  GO2
//
//  Created by NC2-46 on 21/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation

extension HistoryVC {
    
    func setupUI() {
        hideMenu()
        navigationController?.customNavBar()
        //navigationItem.customTitle()
        self.title = "History"
               
        tblView.delegate = self
        tblView.dataSource = self
        tblView.separatorStyle = .none
        //tblView.se
        tblView.tableFooterView = UIView()
        
        segmentControl.backgroundColor = .clear
        segmentControl.addUnderlineForSelectedSegment(vcWidth: self.view.bounds.width)
        self.loadDataFromApi(myView: self.view)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.loadDataFromApi), for: UIControlEvents.valueChanged)
        tblView.addSubview(refreshControl)
        
        
    }
    
    @objc func loadDataFromApi(myView: UIView = UIView()) {
        HistoryApi.sharedInstance.getMyReservedVehiclesRequest(myView: myView, completion: { (myHistory) in
            self.arrReserved = myHistory
            self.arrReserved.reverse()
            self.tblView.reloadData()
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        })
        HistoryApi.sharedInstance.getMyRentalVehiclesRequest(myView: myView, completion: { (myHistory) in
            self.arrRentals = myHistory
            self.arrRentals.reverse()
            self.tblView.reloadData()
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            
        })
    }
}



extension HistoryVC : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch selectedIndex {
        case 0 :
            if (arrReserved.count < 1)
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No Record Available"
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            else
            {
                tableView.separatorStyle = .singleLine
                tableView.backgroundView = nil
            }
            break
        case 1:
            if (arrRentals.count < 1)
            {
                let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text          = "No Record Available"
                noDataLabel.textColor     = UIColor.black
                noDataLabel.textAlignment = .center
                tableView.backgroundView  = noDataLabel
                tableView.separatorStyle  = .none
            }
            else
            {
                tableView.separatorStyle = .singleLine
                tableView.backgroundView = nil
            }
            break
        default : break
            
        }

        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedIndex {
        case 0 :
            return arrReserved.count
        case 1:
            return arrRentals.count
        default :
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTVC") as! HistoryTVC
        
        switch selectedIndex {
        case 0:
            
            cell.configureCell(vehicleImageURL: arrReserved[indexPath.row].image?.url, vehicleStartDate: arrReserved[indexPath.row].startDate!, vehicleEndDate: arrReserved[indexPath.row].endDate!, vehiclePlate: arrReserved[indexPath.row].plate!, vehicleModel: arrReserved[indexPath.row].model!)
            return cell
        case 1:
            cell.configureCell(vehicleImageURL: arrRentals[indexPath.row].image?.url, vehicleStartDate: arrRentals[indexPath.row].startDate!, vehicleEndDate: arrRentals[indexPath.row].endDate!, vehiclePlate: arrRentals[indexPath.row].plate!, vehicleModel: arrRentals[indexPath.row].model!)
            return cell
        default:
            return UITableViewCell()
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let centerViewController = loadViewController(StoryBoardName: "History", VCIdentifer: "HistoryDetailsVC") as! HistoryDetailsVC
        switch selectedIndex {
        case 0:
            centerViewController.historyDetails = arrReserved[indexPath.row]
            break
        case 1:
           centerViewController.historyDetails = arrRentals[indexPath.row]
            break
        default:
            break
        }
        self.navigationController?.pushViewController(centerViewController, animated: true)
    }
    
}
