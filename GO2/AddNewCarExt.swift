
//
//  AddNewCarExt.swift
//  GO2
//
//  Created by NC2-46 on 13/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit
import Foundation
import GoogleMaps
import IQKeyboardManagerSwift
import CoreData
import StepSlider

extension AddNewCarVC :  SelectedPlaceDelegete{
    func getSelectedPlace(pickAddress: GMSAddress, placeName: String) {
        vehicleAddressTV.textColor = UIColor.black
        vehicleAddressTV.text = placeName
        addressLat = "\(pickAddress.coordinate.latitude)"
        addressLng = "\(pickAddress.coordinate.longitude)"
        isVehicleAddressSelected = true
    }
    
    func setupUI() {
        setBackBtn()
        self.title = navigationTitle
        navigationController?.customNavBar()
        vwRating.isHidden = true
        vwShadow.isHidden = true
        globalView.setBackgroundWithTopBarToView()
        headerBar.backgroundColor = Color.SubHeaderColor
        sliderRating.labels = ["0","1","2","3","4","5","6","7","8","9","10"]
        vwRating.clipsToBounds = true
        vwRating.layer.cornerRadius = 25
        
        vehicleAddressTV.text = "Pick Address"
        vehicleAddressTV.textColor = UIColor.lightGray
        
        //Set Toolbar Configuration
        vehicleBrandTF.accessibilityIdentifier = "vehicleBrandTF"
        vehicleModelTF.accessibilityIdentifier = "vehicleModelTF"
        vehiclePlateTF.accessibilityIdentifier = "vehiclePlateTF"
        vehiclePriceTF.accessibilityIdentifier = "vehiclePriceTF"
        vehicleInsuranceIDTF.accessibilityIdentifier = "vehicleInsuranceIDTF"
        vehicleInsuranceCompanyTF.accessibilityIdentifier = "vehicleInsuranceCompanyTF"
        vehicleInsuranceExpirytDateTF.accessibilityIdentifier = "vehicleInsuranceExpirytDateTF"
        vehicleInsuranceChassisNumTF.accessibilityIdentifier = "vehicleInsuranceChassisNumTF"
        vehicleRegistrationNumTF.accessibilityIdentifier = "vehicleRegistrationNumTF"
        vehicleNumOfSeatsTF.accessibilityIdentifier = "vehicleNumOfSeatsTF"
        vehicleTractionTypeTFTF.accessibilityIdentifier = "vehicleTractionTypeTFTF"
        vehicleTypeTF.accessibilityIdentifier = "vehicleTypeTF"
        vehicleCategoryTF.accessibilityIdentifier = "vehicleRideTypeTF"
        vehicleCountryTF.accessibilityIdentifier = "vehicleCountryTF"
        vehicleStateTF.accessibilityIdentifier = "vehicleStateTF"
        vehicleCityTF.accessibilityIdentifier = "vehicleCityTF"
        vehicleAddressTV.accessibilityIdentifier = "vehicleLocationTF"
        
        vehicleTypeTF.delegate = self
        vehicleCategoryTF.delegate = self
        vehicleCountryTF.delegate = self
        vehicleStateTF.delegate = self
        vehicleCityTF.delegate = self
        vehicleAddressTV.delegate = self
        vehicleBrandTF.delegate = self
        vehicleModelTF.delegate = self
        vehiclePlateTF.delegate = self
        vehiclePriceTF.delegate = self
        vehicleInsuranceIDTF.delegate = self
        vehicleInsuranceCompanyTF.delegate = self
        vehicleInsuranceExpirytDateTF.delegate = self
        vehicleInsuranceChassisNumTF.delegate = self
        vehicleRegistrationNumTF.delegate = self
        vehicleNumOfSeatsTF.delegate = self
        vehicleTractionTypeTFTF.delegate = self
        
        //Set Pickers Configuration
        vehicleTypePickerView.delegate = self
        vehicleTypePickerView.dataSource = self
        vehicleTypePickerView.accessibilityLabel = vehicleTypePickerViewIdentifier
        vehicleTypeTF.inputView = vehicleTypePickerView
        
        vehicleRideTypePickerView.delegate = self
        vehicleRideTypePickerView.dataSource = self
        vehicleRideTypePickerView.accessibilityLabel = vehicleRideTypePickerViewIdentifier
        vehicleCategoryTF.inputView = vehicleRideTypePickerView
        
        vehicleTractionTypePickerView.delegate = self
        vehicleTractionTypePickerView.dataSource = self
        vehicleTractionTypePickerView.accessibilityLabel = vehicleTractionTypePickerViewIdentifier
        vehicleTractionTypeTFTF.inputView = vehicleTractionTypePickerView
        
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        countryPickerView.accessibilityLabel = countryPickerViewIdentifier
        vehicleCountryTF.inputView = countryPickerView

        statePickerView.delegate = self
        statePickerView.dataSource = self
        statePickerView.accessibilityLabel = statePickerViewIdentifier
        vehicleStateTF.inputView = statePickerView

        cityPickerView.delegate = self
        cityPickerView.dataSource = self
        cityPickerView.accessibilityLabel = cityPickerViewIdentifier
        vehicleCityTF.inputView = cityPickerView
        stateView = stackView.arrangedSubviews[0]
        cityView = stackView.arrangedSubviews[1]
        
        rentalExpiryDatePicker.datePickerMode = .date
        rentalExpiryDatePicker.frame = CGRect(x: 0, y: self.view.frame.height - (self.view.frame.height - 70), width: self.view.frame.width, height: 70)
        rentalExpiryDatePicker.frame = CGRect(x: 0, y: self.view.frame.height - (self.view.frame.height - 70), width: self.view.frame.width, height: 70)
        
        let date = Date()
        rentalExpiryDatePicker.minimumDate = date
        rentalExpiryDatePicker.addTarget(self, action: #selector(dateExpiryChanged), for: UIControlEvents.valueChanged)
        vehicleInsuranceExpirytDateTF.inputView = rentalExpiryDatePicker
        
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
//        stateView.isHidden = true
//        cityView.isHidden = true
        btnSave.setTitle(btnSaveTitle, for: .normal)
        btnSave.backgroundColor = Color.buttonBG
        self.arrTractionType = ["front","rear","4x4"]
        self.arrVehicleCategory = CoreDBManager.sharedDatabase.getVehicleCategories()
//            self.arrTypeCars = CoreDBManager.sharedDatabase.getVehicleTypes()
        
        if isDetailsUpdate {
            vehicleBrandTF.text = self.myVehicleData.brand
            vehicleModelTF.text = self.myVehicleData.model
            vehiclePlateTF.text = self.myVehicleData.plate
            vehicleTypeTF.text = self.myVehicleData.vehicleType
            vehicleCategoryTF.text = self.myVehicleData.vehicleCategoryType
//            vehiclePriceTF.text = "000"

            vehicleAddressTV.text = self.myVehicleData.address
            vehicleAddressTV.textColor = UIColor.black
            addressLat = self.myVehicleData.lat ?? ""
            addressLng = self.myVehicleData.lng ?? ""
            
            selectedCityID = self.myVehicleData.cityId ?? 0
            selectedStateID = self.myVehicleData.stateId ?? 0
            selectedCountryID = self.myVehicleData.countryId ?? 0
            
            if myVehicleData.images!.count > 0{
                vehicleImagesData = myVehicleData.images!
            }
            
            if myVehicleData.vehiclePartStatuses!.count > 0{
                vehiclePartsData = myVehicleData.vehiclePartStatuses!
            }
            
//            countryPickerView.selectRow(self.myVehicleData.countryId ?? 0, inComponent: 0, animated: true)
//            statePickerView.selectRow(self.myVehicleData.stateId ?? 0, inComponent: 0, animated: true)
//            cityPickerView.selectRow(self.myVehicleData.cityId ?? 0, inComponent: 0, animated: true)
            
            priceViewHeightConstraint.constant = 0
            bottomConstraingOfPriceView.constant = 0
            vehicleInsuranceIDTF.text = "\(self.myVehicleData.insuranceId ?? 0)"
            vehicleInsuranceCompanyTF.text = myVehicleData.insuranceCompany
            vehicleInsuranceExpirytDateTF.text = myVehicleData.insuranceExpiryDate
            vehicleInsuranceChassisNumTF.text = myVehicleData.chassisNumber
            vehicleRegistrationNumTF.text = myVehicleData.registrationNumber
            vehicleNumOfSeatsTF.text = "\(myVehicleData.numberOfSeats ?? 0)"
            
            if arrVehicleCategory.count > 0 {
                self.vehicleCategoryTF.text = self.myVehicleData.vehicleCategoryType
                self.isVehicleRideTypeSelected = true
            }
            if arrTypeCars.count > 0 {
                self.vehicleTypeTF.text = self.myVehicleData.vehicleType
                self.isVehicleTypeSelected = true
            }
            
            if arrTractionType.count > 0 {
                self.vehicleTractionTypeTFTF.text = self.myVehicleData.typeOfTraction
                self.isVehicleTractionTypeSelected = true
            }
            
//            let finalUrl = getConexionAPI_Production() + (myVehicleData.image?.url ?? "")
//            let url = URL.init(string: finalUrl)
//            vehicleImage.sd_setShowActivityIndicatorView(true)
//            vehicleImage.sd_setIndicatorStyle(.gray)
//            vehicleImage.sd_setImage(with: url , placeholderImage: nil)

//            self.arrTypeCars = CoreDBManager.sharedDatabase.getVehicleTypes()
//            self.arrVehicleCategory = CoreDBManager.sharedDatabase.getVehicleCategories()
            
//            self.arrCountrys = CoreDBManager.sharedDatabase.getAllCountry()
//            self.arrStates = CoreDBManager.sharedDatabase.getAllStateRelatedByCountry(countryID: myVehicleData.countryId ?? 0)
//            self.arrCities = CoreDBManager.sharedDatabase.getAllCityRelatedByState(stateID: myVehicleData.stateId ?? 0)
            
            self.arrCountrys = AppDel.arrCountrys
            self.arrStates = AppDel.arrStates
            self.arrCities = AppDel.arrCities
            
            self.isVehicleCountrySelected = true
            self.isVehicleStateSelected = true
            self.isVehicleCitySelected = true
            isVehicleTypeSelected = true
            isVehicleRideTypeSelected = true
            isVehicleAddressSelected = true
            isVehicleImageSelected = true
            
            getCarType(catId: Int16(self.myVehicleData.vehicleCategoryId ?? 0))
            self.arrCarParts = myVehicleData.vehiclePartStatuses!
            if self.arrCarParts.count>0{
                for carPart in arrCarParts{
                    autoreleasepool{
                        arrPartsStatus.append(carPart.status ?? 0)
                    }
                }
                self.heightVehiclePartsConstant.constant = 90
                self.lblNoParts.isHidden = true
            }else{
//                self.heightVehiclePartsConstant.constant = 0
                self.arrCarParts.removeAll()
                self.lblNoParts.isHidden = false
            }
            self.vehicleCountryTF.text = CoreDBManager.sharedDatabase.getCountryName(countryId: myVehicleData.countryId ?? 0)
//            self.vehicleCountryTF.text = CoreDBManager.sharedDatabase.getCountryName(id: myVehicleData.countryId ?? 0)
            
//            self.stateView.isHidden = false
            self.vehicleStateTF.text = CoreDBManager.sharedDatabase.getStateName(stateId: myVehicleData.stateId ?? 0)
//            self.vehicleStateTF.text = CoreDBManager.sharedDatabase.getStateName(id: myVehicleData.stateId ?? 0)
            
//            self.cityView.isHidden = false
            self.vehicleCityTF.text = CoreDBManager.sharedDatabase.getCityName(cityId: myVehicleData.cityId ?? 0)
//            self.vehicleCityTF.text = CoreDBManager.sharedDatabase.getCityName(id: myVehicleData.cityId ?? 0)
           
        }else {
            vehicleImages.append(UIImage(named: "add")!)
            priceViewHeightConstraint.constant = 45
            bottomConstraingOfPriceView.constant = 15
            getCarType(catId: arrVehicleCategory[0].id)
            
            if arrVehicleCategory.count > 0 {
                self.vehicleCategoryTF.text = arrVehicleCategory[0].name!
                self.isVehicleRideTypeSelected = true
            }
            if arrTypeCars.count > 0 {
                self.vehicleTypeTF.text = arrTypeCars[0].name!
                self.isVehicleTypeSelected = true
            }
            
            if arrTractionType.count > 0 {
                self.vehicleTractionTypeTFTF.text = arrTractionType[0] as? String
                self.isVehicleTractionTypeSelected = true
            }
            
            self.arrCountrys = AppDel.arrCountrys
            self.arrStates = AppDel.arrStates
            self.arrCities = AppDel.arrCities
            
            if AppDel.country != "" && AppDel.country != nil{
                vehicleCountryTF.text = AppDel.country
                if AppDel.country != "" && AppDel.country != nil{
                    if let currentCountry:CountriesModel = arrCountrys.filter({$0.name == AppDel.country}).first{
                        if let index = self.arrCountrys.firstIndex(where:{$0.name == currentCountry.name}){
                            selectedCountryID = arrCountrys[index].id ?? 0
                            self.isVehicleCountrySelected = true
                            countryPickerView.selectRow(index, inComponent: 0, animated: true)
                            self.arrStates = CoreDBManager.sharedDatabase.getStatesFromId(countryId: selectedCountryID)
                            self.isNotFound = "1"
                        }
                    }else{
                        self.isNotFound = "0"
                    }
                }
            }
            if AppDel.state != "" && AppDel.state != nil{
                vehicleStateTF.text = AppDel.state
                if AppDel.state != "" && AppDel.state != nil{
                    if let currentState:StatesModel = arrStates.filter({$0.name == AppDel.state}).first{
                        if let index = arrStates.firstIndex(where:{$0.name == currentState.name}){
                            selectedStateID = arrStates[index].id ?? 0
                            self.isVehicleStateSelected = true
                            statePickerView.selectRow(index, inComponent: 0, animated: true)
                            self.arrCities = CoreDBManager.sharedDatabase.getCitiesFromId(stateId: selectedStateID)
                            self.isNotFound = "1"
                        }
                    }else{
                        self.isNotFound = "0"
                    }
                }
            }
            
            if AppDel.city != "" && AppDel.city != nil{
                vehicleCityTF.text = AppDel.city
                if AppDel.city != "" && AppDel.city != nil{
                    if let currentCity:CitiesModel = arrCities.filter({$0.name == AppDel.city}).first{
                        if let index = arrCities.firstIndex(where:{$0.name == currentCity.name}){
                            selectedCityID = arrCities[index].id ?? 0
                            self.isVehicleCitySelected = true
                            cityPickerView.selectRow(index, inComponent: 0, animated: true)
                            self.isNotFound = "\(selectedCityID)"
                        }
                    }else{
                        self.isNotFound = "0"
                    }
                }
            }
        }
        collImages.delegate = self
        collImages.dataSource = self
        collVehicleParts.delegate = self
        collVehicleParts.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 5
        layout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
        collVehicleParts.collectionViewLayout = layout
        collImages.reloadData()
        collVehicleParts.reloadData()
    }
    
    @objc func dateExpiryChanged(){
        vehicleInsuranceExpirytDateTF.text = dateFormatter.string(from: rentalExpiryDatePicker.date)
    }
    
    //MARK :- GetAllCarTypes
    func getCarType(catId:Int16){
        BookingCarApi.sharedInstance.allCarTypesRequest(categoryId: catId) { (carTypes) in
            if carTypes.count > 0{
                self.arrTypeCars = carTypes
                
                if !self.isDetailsUpdate{
                    self.vehicleTypeTF.text = carTypes[0].name
                    self.getCarParts(typeId: carTypes[0].id ?? 0)
                    self.selectedTypeId = carTypes[0].id
                }else{
                    self.selectedTypeId = Int16(self.myVehicleData.vehicleTypeId ?? 0)
                }
            }
        }
    }
    
    //MARK :- GetAllCarParts
    func getCarParts(typeId:Int16){
        BookingCarApi.sharedInstance.allCarPartsRequest(typeId: Int(typeId)) { (carPart) in
            self.arrPartsStatus.removeAll()
            if carPart.count>0{
//                if !self.isDetailsUpdate{
                for _ in carPart{
                    autoreleasepool{
                        self.arrPartsStatus.append(0)
                    }
                }
//                }
                self.arrCarParts = carPart
                self.selectedCarPart = carPart[0].id
                self.heightVehiclePartsConstant.constant = 90
                self.collVehicleParts.reloadData()
                self.lblNoParts.isHidden = true
            }else{
//                self.heightVehiclePartsConstant.constant = 0
                self.arrCarParts.removeAll()
                self.collVehicleParts.reloadData()
                self.lblNoParts.isHidden = false
            }
        }
    }
    
    //MARK :- Delete Image of vehicle
//    @objc func deleteVehicleImage(){
    @IBAction func deleteVehicleImage(_ sender: UIButton) {
        selectedItem = sender.tag
        if !isDetailsUpdate{
            vehicleImages.remove(at: selectedItem ?? 0)
            imagesCount = imagesCount - 1
        }else{
            if vehicleImagesData[selectedItem ?? 0].id != nil && vehicleImagesData[selectedItem ?? 0].id != 0{
                self.arrDeletedImageIds.append(vehicleImagesData[selectedItem ?? 0].id ?? 0)
            }
            self.vehicleImagesData.remove(at: selectedItem ?? 0)
        }
        collImages.reloadData()
    }
}

extension AddNewCarVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isDetailsUpdate{
            var imageCount:Int = 0
            imageCount = vehicleImagesData.count >= 8 ? vehicleImagesData.count : vehicleImagesData.count + 1
            return collectionView == collImages ? imageCount : arrCarParts.count
        }else{
            return collectionView == collImages ? imagesCount : arrCarParts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collImages{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellVehicleImages", for: indexPath as IndexPath) as! CellVehicleImages
            if(isDetailsUpdate){
                if indexPath.item == vehicleImagesData.count{
                    cell.btnDelete.isHidden = true
                    cell.imgVehicle.image = UIImage(named: "add")
                }else{
                    cell.btnDelete.isHidden = false
                    let finalUrl = getConexionAPI_Production() + (vehicleImagesData[indexPath.item].url ?? "")!
                    if (vehicleImagesData[indexPath.item].url ?? "") == ""{
                        let imageOfVehicle = vehicleImagesData[indexPath.item].image
                        if imageOfVehicle != nil{
                            cell.imgVehicle.image = imageOfVehicle
                        }
                    }else{
                        let url = URL.init(string: finalUrl)
                        cell.imgVehicle.sd_setShowActivityIndicatorView(true)
                        cell.imgVehicle.sd_setIndicatorStyle(.gray)
                        cell.imgVehicle.sd_setImage(with: url , placeholderImage: nil)
                    }
                }
            }else{
                cell.imgVehicle.image = vehicleImages[indexPath.item]
                cell.btnDelete.isHidden = vehicleImages[indexPath.item] == UIImage(named: "add")
            }
            cell.imgVehicle.clipsToBounds = true
            cell.imgVehicle.layer.cornerRadius = 10
            cell.btnDelete.tag = indexPath.item
            cell.btnDelete.addTarget(self, action: #selector(deleteVehicleImage), for: .touchUpInside)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellVehicleParts", for: indexPath as IndexPath) as! CellVehicleParts
            let obj = arrCarParts[indexPath.item]
            cell.lblPartName.text = obj.vehiclePart != "" && obj.vehiclePart != nil ? obj.vehiclePart : obj.name
            cell.backView.layer.borderColor = arrPartsStatus[indexPath.item] == -1 ? UIColor.clear.cgColor : UIColor.gray.cgColor
            cell.backView.layer.borderWidth = arrPartsStatus[indexPath.item] == -1 ? 0 : 1.0
            cell.backView.clipsToBounds = true
            cell.backView.layer.cornerRadius = 5
            cell.lblPartName.backgroundColor = UIColor.clear
            cell.backView.backgroundColor = arrPartsStatus[indexPath.item] == -1 ? UIColor.red : UIColor.white
            cell.lblPartStatus.text = arrPartsStatus[indexPath.item] == -1 ? "" : "\(arrPartsStatus[indexPath.item])"
            cell.lblPartName.textColor = arrPartsStatus[indexPath.item] == -1 ? UIColor.white : UIColor.black
            cell.lblPartStatus.isHidden = arrPartsStatus[indexPath.item] == -1 ? true : false
            return cell
        }
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView == collImages{
            selectedItem = indexPath.item
            imagePicker.delegate = self
            let optionMenu = UIAlertController(title: nil, message: "Add Photo", preferredStyle: .actionSheet)
            
            let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler:{
                (alert: UIAlertAction!) -> Void in
                self.openGallery()
            })
            
            let cameraAction = UIAlertAction(title: "Camera", style: .default, handler:{
                (alert: UIAlertAction!) -> Void in
                self.openCamera()
            })
            
            let cancleAction = UIAlertAction(title: "Cancel", style: .cancel, handler:{
                (alert: UIAlertAction!) -> Void in
                print("Cancel")
            })
            
            optionMenu.addAction(galleryAction)
            optionMenu.addAction(cameraAction)
            optionMenu.addAction(cancleAction)
            optionMenu.popoverPresentationController?.sourceView = collectionView // works for both iPhone & iPad
            self.present(optionMenu, animated: true, completion: nil)
        }else{
            self.lblPartName.text = "   " + ((arrCarParts[indexPath.item].vehiclePart != "" && arrCarParts[indexPath.item].vehiclePart != nil ? arrCarParts[indexPath.item].vehiclePart : arrCarParts[indexPath.item].name) ?? "")
            vwRating.isHidden = false
            vwShadow.isHidden = false
            selectedPartIndex = indexPath.item
            sliderRating.index =  arrPartsStatus[indexPath.item] == -1 ? 0 : UInt(arrPartsStatus[indexPath.item])
            checkBoxNotApplicable.image = arrPartsStatus[indexPath.item] == -1 ? UIImage(named: "checked"):UIImage(named: "unchecked")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
            let cellSize = CGSize(width: (collectionView.bounds.width - (5 * 10))/4, height: (collectionView.bounds.height - (3 * 10))/2)
            return cellSize
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView == collImages{
            return 10
        }else{
            return 5
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        if collectionView == collImages{
            let sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
            return sectionInset
        }else{
            return UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
}

extension AddNewCarVC : UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.accessibilityLabel == countryPickerViewIdentifier {
            return arrCountrys.count
        }
        else if pickerView.accessibilityLabel == statePickerViewIdentifier {
            return arrStates.count
        }
        else if pickerView.accessibilityLabel == cityPickerViewIdentifier {
            return arrCities.count
        }
        else if pickerView.accessibilityLabel == vehicleTypePickerViewIdentifier{
            return arrTypeCars.count
        }
        else if pickerView.accessibilityLabel == vehicleRideTypePickerViewIdentifier{
            return arrVehicleCategory.count
        }
        else if pickerView.accessibilityLabel == vehicleTractionTypePickerViewIdentifier{
            return arrTractionType.count
        }
        else {
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.accessibilityLabel == countryPickerViewIdentifier {
//            stateView.isHidden = true
//            cityView.isHidden = true
            return arrCountrys[row].name
        }
        else if pickerView.accessibilityLabel == cityPickerViewIdentifier {
            return arrCities[row].name
        }
        else if pickerView.accessibilityLabel == statePickerViewIdentifier {
            return arrStates[row].name
        }
        else if pickerView.accessibilityLabel == vehicleTypePickerViewIdentifier {
            return arrTypeCars[row].name
        }
        else if pickerView.accessibilityLabel == vehicleRideTypePickerViewIdentifier {
            return arrVehicleCategory[row].name!.replacingOccurrences(of: "_", with: "")
        }
        else if pickerView.accessibilityLabel == vehicleTractionTypePickerViewIdentifier {
            return (arrTractionType[row] as? String)?.replacingOccurrences(of: "_", with: "")
        }
        else {
            return ""
        }
        //return ""
    }
}




extension AddNewCarVC: UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
//        let vc = loadViewController(StoryBoardName: "Location", VCIdentifer: "LocationPickerVC") as! LocationPickerVC
//        
//        vc.delegateSelectedPlace = self
//        self.navigationController?.pushViewController(vc, animated: false)
        
        
        return true
    }
    
}
extension AddNewCarVC: UITextFieldDelegate{

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Pick Address"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let id = textField.accessibilityIdentifier
        switch id {
        case vehicleCountryTF.accessibilityIdentifier!?,vehicleStateTF.accessibilityIdentifier!?,vehicleCityTF.accessibilityIdentifier!?:
            IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
            break
        default:
            IQKeyboardManager.shared.previousNextDisplayMode = .default
            break
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        let id = textField.accessibilityIdentifier
        switch id {
        case vehicleInsuranceExpirytDateTF.accessibilityIdentifier!? :
            vehicleInsuranceExpirytDateTF.text = dateFormatter.string(from: rentalExpiryDatePicker.date)
            self.view.endEditing(true)
            isExpiryDateSelectyed = true
            break
        case vehicleCategoryTF.accessibilityIdentifier!?:
            vehicleCategoryTF.text = arrVehicleCategory[vehicleRideTypePickerView.selectedRow(inComponent: 0)].name
            isVehicleRideTypeSelected = true
            getCarType(catId: arrVehicleCategory[vehicleRideTypePickerView.selectedRow(inComponent: 0)].id)
            break
        case vehicleTypeTF.accessibilityIdentifier!?:
            if arrTypeCars.count > 0 {
                vehicleTypeTF.text = arrTypeCars[vehicleTypePickerView.selectedRow(inComponent: 0)].name
                isVehicleTypeSelected = true
                self.selectedTypeId = arrTypeCars[vehicleTypePickerView.selectedRow(inComponent: 0)].id
                if isDetailsUpdate{
                    if self.selectedTypeId ?? 0 != myVehicleData.vehicleTypeId ?? 0{
                        if vehiclePartsData.count == 0{
                            isPartsAvailable = false
                        }else{
                            isPartsAvailable = true
                        }
                        for partOfVehicle in vehiclePartsData{
                            autoreleasepool{
                                arrDeletedPartIds.append(partOfVehicle.id ?? 0)
                            }
                        }
                    }
                }
                getCarParts(typeId: self.selectedTypeId ?? 0)
                
                if arrCarParts.count > 0{
                    arrUpdatedParts = arrCarParts
                    if arrUpdatedParts.count > 0{
                        for carPart in arrUpdatedParts{
                            autoreleasepool{
                                arrUpdatedPartStatuses.append(carPart.status ?? 0)
                            }
                        }
                    }
                }
            }
            break
        case vehicleTractionTypeTFTF.accessibilityIdentifier!?:
            vehicleTractionTypeTFTF.text = arrTractionType[vehicleTractionTypePickerView.selectedRow(inComponent: 0)] as? String
            isVehicleTractionTypeSelected = true
            break
        case vehicleCountryTF.accessibilityIdentifier!?:
            if arrCountrys.count > 0 {
                selectedCountryID = arrCountrys[countryPickerView.selectedRow(inComponent: 0)].id ?? 0
                let name : String = arrCountrys[countryPickerView.selectedRow(inComponent: 0)].name!
                vehicleCountryTF.text = name
                isVehicleStateSelected = false
                isVehicleCitySelected = false
                arrStates.removeAll()
                arrCities.removeAll()
                vehicleStateTF.text = ""
                vehicleCityTF.text = ""
//                arrStates = CoreDBManager.sharedDatabase.getAllStateRelatedByCountry(countryID: selectedCountryID)
                arrStates = CoreDBManager.sharedDatabase.getStatesFromId(countryId: selectedCountryID)
                isVehicleCountrySelected = true
//                stateView.isHidden = false
            }
            break
        case vehicleStateTF.accessibilityIdentifier!?:
            if arrStates.count > 0 {
                selectedStateID = arrStates[statePickerView.selectedRow(inComponent: 0)].id ?? 0
                
                let name : String = arrStates[statePickerView.selectedRow(inComponent: 0)].name!
                vehicleStateTF.text = name
                isVehicleCitySelected = false
                arrCities.removeAll()
                vehicleCityTF.text = ""
                arrCities = CoreDBManager.sharedDatabase.getCitiesFromId(stateId: selectedStateID)
//                arrCities = CoreDBManager.sharedDatabase.getAllCityRelatedByState(stateID: selectedStateID)
                
                isVehicleStateSelected = true
//                cityView.isHidden = false
            }
            break
        case vehicleCityTF.accessibilityIdentifier!?:
            if arrCities.count > 0 {
                selectedCityID = arrCities[cityPickerView.selectedRow(inComponent: 0)].id ?? 0
                vehicleCityTF.text = arrCities[cityPickerView.selectedRow(inComponent: 0)].name
                isVehicleCitySelected = true
            }
            break
        default:
            break
        }
    }

}

//Mark: - Extension For Validation
extension AddNewCarVC {
    //Mark: - Validate Function for checking fileds proper data.
    func validateData() -> Bool {
        self.view.endEditing(true)
        if !isDetailsUpdate{
            if vehicleImages.contains(UIImage(named: "add")!){
                guard (vehicleImages.count > 1) else{
                    displayMyAlertMessage(userMessage: "Please Select Vehicle Image.")
                    return false
                }
            }else{
                guard (vehicleImages.count > 0) else{
                    displayMyAlertMessage(userMessage: "Please Select Vehicle Image.")
                    return false
                }
            }
            
        }else{
            guard (vehicleImagesData.count > 0) else{
                displayMyAlertMessage(userMessage: "Please Select Vehicle Image.")
                return false
            }
        }
        
        guard !(vehicleBrandTF.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please Enter Vehicle Brand.")
            return false
        }
        guard !(vehicleModelTF.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please Enter Vehicle Model.")
            return false
        }
        guard !(vehiclePlateTF.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please Enter Vehicle Plate Number.")
            return false
        }
        
        if !isDetailsUpdate{
            guard !(vehiclePriceTF.isEmpty) else{
                displayMyAlertMessage(userMessage: "Please Enter Vehicle Price.")
                return false
            }
        }
        
        guard !(vehicleInsuranceIDTF.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please Enter Vehicle Insurance Id.")
            return false
        }
        guard !(vehicleInsuranceCompanyTF.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please Enter Vehicle Insurance Company.")
            return false
        }
        guard !(vehicleInsuranceExpirytDateTF.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please Enter Vehicle Insurance Expiry Date.")
            return false
        }
        guard !(vehicleInsuranceChassisNumTF.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please Enter Vehicle Chassis Number.")
            return false
        }
        guard !(vehicleRegistrationNumTF.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please Enter Vehicle Registration Number.")
            return false
        }
        guard !(vehicleNumOfSeatsTF.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please Enter Vehicle No. of Seats.")
            return false
        }
//        guard (isVehicleTractionTypeSelected) else{
//            displayMyAlertMessage(userMessage: "Please Select Vehicle Traction Type.")
//            return false
//        }
//        guard (isVehicleTypeSelected) else{
//            displayMyAlertMessage(userMessage: "Please Select a Vehicle Category.")
//            return false
//        }
//        guard (isVehicleRideTypeSelected) else{
//            displayMyAlertMessage(userMessage: "Please Select a Vehicle Type.")
//            return false
//        }
//
//        guard (isVehicleCountrySelected) else{
//            displayMyAlertMessage(userMessage: "Please Select a Country.")
//            return false
//        }
        
//        guard (isVehicleStateSelected) && (!stateView.isHidden) else{
//        guard (!stateView.isHidden) else{
//        displayMyAlertMessage(userMessage: "Please Select a State.")
//            return false
//        }
//
//        //        guard (isVehicleCitySelected) && (!cityView.isHidden) else{
//        guard (!cityView.isHidden) else{
//            displayMyAlertMessage(userMessage: "Please Select a City.")
//            return false
//        }
        guard (isVehicleAddressSelected) else{
            displayMyAlertMessage(userMessage: "Please Select/Pick an Address.")
            return false
        }
        return true
    }
}


extension AddNewCarVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            openGallery()
        }
    }
    func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            if !isDetailsUpdate{
                if vehicleImages.count < 8{
                    if vehicleImages[selectedItem ?? 0] == UIImage(named: "add"){
                        imagesCount = imagesCount+1
                        vehicleImages.append(UIImage(named: "add")!)
                    }
                }
                vehicleImages[selectedItem ?? 0] = pickedImage
                self.isVehicleImageSelected = true
            }else{
                if vehicleImagesData.count < 8{
                    if selectedItem == vehicleImagesData.count{
//                        var imageObj : VehicleDetailsImage?
                        var imageObj = VehicleDetailsImage(object: AnyObject.self)
                        imageObj.id = 0
                        imageObj.url = ""
                        imageObj.image = pickedImage
                        vehicleImagesData.append(imageObj)
                    }else{
                        vehicleImagesData[selectedItem ?? 0].id = 0
                        vehicleImagesData[selectedItem ?? 0].url = ""
                        vehicleImagesData[selectedItem ?? 0].image = pickedImage
                    }
                }else{
                    vehicleImagesData[selectedItem ?? 0].id = 0
                    vehicleImagesData[selectedItem ?? 0].url = ""
                    vehicleImagesData[selectedItem ?? 0].image = pickedImage
                }
                self.isVehicleImageSelected = true
            }
            collImages.reloadData()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
}
