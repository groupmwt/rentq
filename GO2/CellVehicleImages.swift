//
//  CellVehicleImages.swift
//  GO2
//
//  Created by C115 on 28/10/21.
//  Copyright © 2021 Juan Cardozo. All rights reserved.
//

import UIKit

class CellVehicleImages: UICollectionViewCell {
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var imgVehicle: UIImageView!
}
