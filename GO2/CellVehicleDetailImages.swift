//
//  CellVehicleDetailImages.swift
//  GO2
//
//  Created by C115 on 11/11/21.
//  Copyright © 2021 Juan Cardozo. All rights reserved.
//

import UIKit

class CellVehicleDetailImages: UICollectionViewCell {
    @IBOutlet weak var imgVehicles: UIImageView!
    
}
