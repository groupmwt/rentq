//
//  UserProfileData.swift
//
//  Created by NC2-46 on 18/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class UserProfileData: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lastName = "last_name"
    static let email = "email"
    static let id = "id"
    static let phoneNumber = "phone_number"
    static let firstName = "first_name"
    static let image = "image"
  }

  // MARK: Properties
  public var lastName: String?
  public var email: String?
  public var id: Int?
  public var phoneNumber: String?
  public var firstName: String?
  public var image: UserProfileImage?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    lastName = json[SerializationKeys.lastName].string
    email = json[SerializationKeys.email].string
    id = json[SerializationKeys.id].int
    phoneNumber = json[SerializationKeys.phoneNumber].string
    firstName = json[SerializationKeys.firstName].string
    image = UserProfileImage(json: json[SerializationKeys.image])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = phoneNumber { dictionary[SerializationKeys.phoneNumber] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = image { dictionary[SerializationKeys.image] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.phoneNumber = aDecoder.decodeObject(forKey: SerializationKeys.phoneNumber) as? String
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? UserProfileImage
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(phoneNumber, forKey: SerializationKeys.phoneNumber)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(image, forKey: SerializationKeys.image)
  }

}
