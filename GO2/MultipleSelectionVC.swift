//
//  MultipleSelectionVC.swift
//  GO2
//
//  Created by NC2-46 on 04/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit

class MultipleSelectionVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtStartDate: UITextFieldX!
    @IBOutlet weak var txtEndDate: UITextFieldX!
    @IBOutlet weak var txtPrice: UITextFieldX!
    @IBOutlet weak var btnDone: UIButtonX!
    @IBOutlet weak var btnCancle: UIButtonX!
    
    var carID = ""
    var minimumDate : Date!
    var superView1 : CheckAvailabilityVC!
    let startDatePickerIdentifier = "startDatePicker"
    let startDatePicker = UIDatePicker()
    
    let endDatePickerIdentifier = "endDatePicker"
    let endDatePicker = UIDatePicker()
    // Create date formatter
    let dateFormatter: DateFormatter = DateFormatter()
    
    //MARK: Formatter
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        txtPrice.keyboardType = .decimalPad
        txtStartDate.accessibilityIdentifier = "txtStartDate"
        txtEndDate.accessibilityIdentifier = "txtEndDate"
        
        //Set Date/Time Formatters
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        addToolBar(textField: txtStartDate)
        addToolBar(textField: txtEndDate)
        //Set DatePickers
        startDatePicker.datePickerMode = .date
        endDatePicker.datePickerMode = .date
        
        startDatePicker.minimumDate = minimumDate
        endDatePicker.minimumDate = minimumDate
        txtStartDate.text = dateFormatter.string(from: minimumDate)
        txtEndDate.text = dateFormatter.string(from: minimumDate)
        
        startDatePicker.addTarget(self, action: #selector(dateStartChanged), for: UIControlEvents.valueChanged)
        endDatePicker.addTarget(self, action: #selector(dateEndChanged), for: UIControlEvents.valueChanged)
        
        txtStartDate.inputView = startDatePicker
        txtEndDate.inputView = endDatePicker
        lblTitle.backgroundColor = Color.buttonBG
        btnDone.backgroundColor = Color.buttonBG
        btnCancle.backgroundColor = Color.buttonBG
    }
    
    @objc func dateStartChanged(){
        txtStartDate.text = dateFormatter.string(from: startDatePicker.date)
        endDatePicker.minimumDate = startDatePicker.date
    }
    
    
    @objc func dateEndChanged(){
        txtEndDate.text = dateFormatter.string(from: endDatePicker.date)
    }
    
    @IBAction func btnDone(_ sender: UIButtonX) {
        if (txtPrice.text != nil) && (txtPrice.text != "") {
            superView1.addOffer(carID: carID, start_date: txtStartDate.text!, end_date: txtEndDate.text!, price: txtPrice.text!)
            removeAnimate()
        }
        else {
            displayMyAlertMessage(userMessage: "Please enter valid price")
        }
    }
    
    @IBAction func btnCancel(_ sender: UIButtonX) {
        removeAnimate()
    }
    
}


extension MultipleSelectionVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func addToolBar(textField: UITextField){
        
        textField.delegate = self
        let id = textField.accessibilityIdentifier
        var doneBtn:UIBarButtonItem
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        
        switch id {
        case txtStartDate.accessibilityIdentifier!? :
            doneBtn = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.doneStartDateProcess))
            
        case txtEndDate.accessibilityIdentifier!?:
            doneBtn = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.doneEndDateProcess))
            
        default:
            doneBtn = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePressed))
        }
        
        let candelBtn = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelPressed))
        
        let spaceBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([candelBtn, spaceBtn, doneBtn], animated: true)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.delegate = self
        textField.inputAccessoryView = toolBar
    }
    
    @objc func donePressed(){
        view.endEditing(true)
    }
    
    @objc func doneStartDateProcess(){
        txtStartDate.text = dateFormatter.string(from: startDatePicker.date)
        endDatePicker.minimumDate = startDatePicker.date
        txtEndDate.text = dateFormatter.string(from: endDatePicker.date)
        self.view.endEditing(true)
        //isRentalDateSelected = true
    }
    
    @objc func doneEndDateProcess(){
        txtEndDate.text = dateFormatter.string(from: endDatePicker.date)
        self.view.endEditing(true)
       // isReturnDateSelected = true
    }
    
    
    @objc func cancelPressed(){
        view.endEditing(true)
    }
}
