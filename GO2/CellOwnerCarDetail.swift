//
//  CellOwnerCarDetail.swift
//  GO2
//
//  Created by C115 on 04/01/22.
//  Copyright © 2022 Juan Cardozo. All rights reserved.
//

import UIKit

class CellOwnerCarDetail: UICollectionViewCell {
    @IBOutlet weak var vwShadow: UIView!
    @IBOutlet weak var imgViewCar: UIImageView!
    @IBOutlet weak var lblCarModel: UILabel!
    @IBOutlet weak var lblCarBrand: UILabel!
    @IBOutlet weak var lblCarType: UILabel!
}
