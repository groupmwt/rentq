//
//  JSONExtension.swift
//  everydayHero
//
//  Created by NC2-28 on 19/09/17.
//  Copyright © 2017 NC2-28. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit
extension JSON {
    func to<T>(type: T?) -> Any? {
        if let baseObj = type as? JSONable.Type {
            if self.type == .array {
                var arrObject: [Any] = []
                for obj in self.arrayValue {
                    let object = baseObj.init(json: obj)
                    arrObject.append(object!)
                }
                return arrObject
            } else {
                let object = baseObj.init(json: self)
                return object!
            }
        }
        return nil
    }
}
protocol JSONable {
    init?(json: JSON)
}
protocol JSONAble {}
extension JSONAble {
    func toDict() -> [String:Any] {
        var dict = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                dict[key] = child.value
            }
        }
        return dict
    }
}

