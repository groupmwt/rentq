//
//  CD_City+CoreDataProperties.swift
//  GO2
//
//  Created by NC2-46 on 17/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//
//

import Foundation
import CoreData


extension CD_City {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CD_City> {
        return NSFetchRequest<CD_City>(entityName: "CD_City")
    }

    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var state_id: Int16

}
