//
//  String+Extension.swift
//  Motiv8
//
//  Created by NC2-46 on 26/03/18.
//  Copyright © 2018 NC2-46. All rights reserved.
//

import Foundation
import UIKit
extension String {
    func isValidEmail() -> Bool {
        let regex : NSString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: self)
    }
}

