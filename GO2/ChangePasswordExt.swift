//
//  ChangePasswordExt.swift
//  GO2
//
//  Created by NC2-46 on 27/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
extension ChangePasswordVC {
    
    func setupUI(){
        //Style
        globalView.setBackgroundToView()
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.customNavBar()
        
        setBackBtn()
        
        txtNewPassword.delegate = self
        txtConfirmPassword.delegate = self
        
        txtNewPassword.textType = .password
        txtConfirmPassword.textType = .password
        btnChangePass.backgroundColor = Color.buttonBG
    }
}

extension ChangePasswordVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


//Mark: - Extension For Validation
extension ChangePasswordVC {
    //Mark: - Validate Function for checking fileds proper data.
    func validateData() -> Bool {
       self.view.endEditing(true)
       
        guard !(txtNewPassword.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter New Password")
            return false
        }
        guard (txtNewPassword.isValidPassword) else{
            displayMyAlertMessage(userMessage: "Password length should be at least 6 characters")
            return false
        }
        
        guard !(txtConfirmPassword.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter New Confirm Password")
            return false
        }
        
        guard (txtNewPassword.text == txtConfirmPassword.text) else{
            displayMyAlertMessage(userMessage: "New Password and New Confirm Password does not match.")
            return false
        }
        return true
    }
}
