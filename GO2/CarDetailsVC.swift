//
//  CarDetailsVC.swift
//  GO2
//
//  Created by NC2-46 on 11/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit

class CarDetailsVC: UIViewController {

    //MARK: Views
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var headerBar: UIView!
    @IBOutlet weak var labelBarTitle: UILabel!
    
    @IBOutlet weak var lblOwnerCarTitle: UILabel!
    @IBOutlet weak var iconOwnerCar: UIImageView!
    @IBOutlet weak var heightOfOwnerImgIcon: NSLayoutConstraint!
    @IBOutlet weak var heightOfOwnerList: NSLayoutConstraint!
    @IBOutlet weak var collOwnerCars: UICollectionView!
    @IBOutlet weak var vwOwnerCarList: UIView!
    @IBOutlet weak var vwPager: UIPageControl!
   // @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tblVIewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var collImages: UICollectionView!
    @IBOutlet weak var btnEditPayment: UIButton!
    
    @IBOutlet weak var btnDelete: UIButton!
    var searchVehicleData : SearchVehicleData!
//    var myVehicleData : MyVehicleListData!
    var myVehicleData : VehicleDetailsData!
    var headerTitle: String = "Vehicle Details"
    var navigationTitle = "Vehicle Profile"
    var isMyVehicleDetails: Bool = false
    var isDataComeFromNearbyVehicle: Bool = false
    var vehicleId:Int?
    var brandData: String?
    var modelData: String?
    var plateData: String?
    var vehicleTypeData: String?
    var insuranceIdData: String?
    var insuranceCompanyData: String?
    var insuranceExpityData: String?
    var chassisNumberData: String?
    var registrationNumberData: String?
    var noOfSeatsData: String?
    var tractionData: String?
    var vehiclePartsData = [VehicleDetailsPartStatuses]()
//    var rideTypeData: String?
    var countryData: String?
    var stateData: String?
    var cityData: String?
    var locationData: String?
    var arrOwnerVehicleList = [OwnerVehicleListData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    @IBAction func btnEditPayment(_ sender: Any) {
        if isMyVehicleDetails {
            //show my car details
            btnEditPayment.setTitle("Edit", for: .normal)
            btnDelete.isHidden = false
             print("Go to edit Screen")
            let vc = loadViewController(StoryBoardName: "VehicleList", VCIdentifer: "AddNewCarVC") as! AddNewCarVC
            vc.navigationTitle = "Update Vehicle"
            vc.btnSaveTitle = "Update"
            vc.myVehicleData = self.myVehicleData
            vc.isDetailsUpdate = true

            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            // show car details for payment
            print("Payment Done")
            
            let centerViewController = loadViewController(StoryBoardName: "MakePayment", VCIdentifer: "InvoiceVC") as! InvoiceVC
//                centerViewController.searchVehicleData = searchVehicleData
            centerViewController.isFromNearbyVehicle = self.isDataComeFromNearbyVehicle
            if isDataComeFromNearbyVehicle{
                if self.myVehicleData.offers != nil {
                    centerViewController.cityName = self.cityData!
                    centerViewController.myVehicleData = myVehicleData
                    self.navigationController?.pushViewController(centerViewController, animated: true)
                }
                else {
                    displayMyAlertMessage(userMessage: "No offer available.")
                }
            }else{
                if self.searchVehicleData.offers != nil {
                    centerViewController.cityName = self.cityData!
                    centerViewController.searchVehicleData = searchVehicleData
                    self.navigationController?.pushViewController(centerViewController, animated: true)
                }
                else {
                    displayMyAlertMessage(userMessage: "No offer available.")
                }
            }
        }
    }
    
    @IBAction func btnDelete(_ sender: Any) {
        
        let deleteAlert = UIAlertController(title: "Delete Vehicle", message: "Are you sure you want to delete this vehicle?", preferredStyle: UIAlertControllerStyle.alert)
        
        deleteAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
            MyVehiclesApi.sharedInstance.deleteVehicleRequest(myView: self.view, carID: "\(self.myVehicleData.id ?? 0)", completion: { (hasError) in
                print(hasError)
                self.backTap()
            })
            
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(deleteAlert, animated: true, completion: nil)

        
    }
    
}



