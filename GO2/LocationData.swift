//
//  LocationData.swift
//
//  Created by NC2-46 on 18/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class LocationData: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let states = "states"
    static let countries = "countries"
    static let cities = "cities"
  }

  // MARK: Properties
  public var states: [LocationStates]?
  public var countries: [LocationCountries]?
  public var cities: [LocationCities]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.states].array { states = items.map { LocationStates(json: $0) } }
    if let items = json[SerializationKeys.countries].array { countries = items.map { LocationCountries(json: $0) } }
    if let items = json[SerializationKeys.cities].array { cities = items.map { LocationCities(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = states { dictionary[SerializationKeys.states] = value.map { $0.dictionaryRepresentation() } }
    if let value = countries { dictionary[SerializationKeys.countries] = value.map { $0.dictionaryRepresentation() } }
    if let value = cities { dictionary[SerializationKeys.cities] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.states = aDecoder.decodeObject(forKey: SerializationKeys.states) as? [LocationStates]
    self.countries = aDecoder.decodeObject(forKey: SerializationKeys.countries) as? [LocationCountries]
    self.cities = aDecoder.decodeObject(forKey: SerializationKeys.cities) as? [LocationCities]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(states, forKey: SerializationKeys.states)
    aCoder.encode(countries, forKey: SerializationKeys.countries)
    aCoder.encode(cities, forKey: SerializationKeys.cities)
  }

}
