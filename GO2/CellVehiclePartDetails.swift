//
//  CellVehiclePartDetails.swift
//  GO2
//
//  Created by C115 on 10/11/21.
//  Copyright © 2021 Juan Cardozo. All rights reserved.
//

import UIKit

class CellVehiclePartDetails: UITableViewCell, UICollectionViewDataSource , UICollectionViewDelegate{
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNoPartsFound: UILabel!
    @IBOutlet weak var collVehicleParts: UICollectionView!
    var vehiclePartsData = [VehicleDetailsPartStatuses]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgLogo.contentMode = .scaleAspectFit
        self.collVehicleParts.dataSource = self
        self.collVehicleParts.delegate = self
        self.lblNoPartsFound.text = "No Parts Found."
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 5
        layout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
        collVehicleParts.collectionViewLayout = layout
        if vehiclePartsData.count <= 0{
            self.collVehicleParts.isHidden = true
        }else{
            self.collVehicleParts.isHidden = false
        }
        self.collVehicleParts.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(imgLogo: UIImage,rowTitle : String, rowData:[VehicleDetailsPartStatuses]?, tagvalue:Int,numLines : Int) {
        self.imgLogo.image = imgLogo
        self.lblTitle.text = rowTitle
        if vehiclePartsData.count <= 0{
            self.collVehicleParts.isHidden = true
        }else{
            self.collVehicleParts.isHidden = false
            self.collVehicleParts.reloadData()
        }

    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vehiclePartsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellVehicleParts", for: indexPath as IndexPath) as! CellVehicleParts
        let obj = vehiclePartsData[indexPath.item]
        cell.lblPartName.text = obj.vehiclePart
        cell.backView.layer.borderColor = obj.status == -1 ? UIColor.clear.cgColor : UIColor.gray.cgColor
        cell.backView.layer.borderWidth = obj.status == -1 ? 0 : 1.0
        cell.backView.clipsToBounds = true
        cell.backView.layer.cornerRadius = 5
        cell.lblPartName.backgroundColor = UIColor.clear
        cell.backView.backgroundColor = obj.status == -1 ? UIColor.red : UIColor.white
        cell.lblPartStatus.text = obj.status == -1 ? "" : "\(obj.status ?? 0)"
        cell.lblPartName.textColor = obj.status == -1 ? UIColor.white : UIColor.black
        cell.lblPartStatus.isHidden = obj.status == -1 ? true : false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellSize = CGSize(width: (collectionView.bounds.width - (5 * 10))/4, height: (collectionView.bounds.height - (3 * 10))/2)
        return cellSize
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
}
