//
//  MyCarsNewCarViewController.swift
//  GO2
//
//  Created by Juan Cardozo on 8/6/17.
//  Copyright © 2017 GNM. All rights reserved.
//

import UIKit
import StepSlider

class AddNewCarVC: UIViewController {
    
    // MARK: User Data
    let userDefaults = UserDefaults.standard
//    var myVehicleData : MyVehicleListData!
    var myVehicleData : VehicleDetailsData!
    
    var navigationTitle = ""

    //MARK: - Static Labels
    @IBOutlet weak var labelHeaderTitle: UILabel!

    //MARK: - Textfields/Textareas
    @IBOutlet weak var vehicleBrandTF: UITextFieldX!
    @IBOutlet weak var vehicleModelTF: UITextFieldX!
    @IBOutlet weak var vehiclePlateTF: UITextFieldX!
    @IBOutlet weak var vehicleCategoryTF: UITextFieldX!
    @IBOutlet weak var vehicleTypeTF: UITextFieldX!
    //    @IBOutlet weak var vehicleRideTypeTF: UITextFieldX!
    @IBOutlet weak var lblNoParts: UILabel!
    @IBOutlet weak var collVehicleParts: UICollectionView!
    @IBOutlet weak var vehiclePriceTF: UITextFieldX!
    @IBOutlet weak var vehicleInsuranceIDTF: UITextFieldX!
    @IBOutlet weak var vehicleInsuranceCompanyTF: UITextFieldX!
    @IBOutlet weak var vehicleInsuranceExpirytDateTF: UITextFieldX!
    @IBOutlet weak var vehicleInsuranceChassisNumTF: UITextFieldX!
    @IBOutlet weak var vehicleRegistrationNumTF: UITextFieldX!
    @IBOutlet weak var vehicleNumOfSeatsTF: UITextFieldX!
    @IBOutlet weak var vehicleTractionTypeTFTF: UITextFieldX!
    @IBOutlet weak var vehicleCountryTF: UITextFieldX!
    @IBOutlet weak var vehicleStateTF: UITextFieldX!
    @IBOutlet weak var vehicleCityTF: UITextFieldX!
    @IBOutlet weak var vehicleAddressTV: UITextView!
    @IBOutlet weak var priceViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collImages: UICollectionView!
    @IBOutlet weak var heightVehiclePartsConstant: NSLayoutConstraint!
    @IBOutlet weak var vwShadow: UIView!
    @IBOutlet weak var vwRating: UIView!
    @IBOutlet weak var lblPartName: UILabel!
    @IBOutlet weak var sliderRating: StepSlider!
    @IBOutlet weak var checkBoxNotApplicable: UIImageView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var stackViewButtons: UIStackView!
    @IBOutlet weak var btnSearchAddress: UIButton!
    
    @IBOutlet weak var bottomConstraingOfPriceView: NSLayoutConstraint!
    //MARK: - Views
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var headerBar: UIView!
    //MARK: - UIView / Stackview
    @IBOutlet weak var stackView: UIStackView!
    
    var stateView = UIView()
    var cityView = UIView()
    
    var arrCountrys = [CountriesModel]()//[CD_Country]()
    var arrStates = [StatesModel]()//[CD_State]()
    var arrCities = [CitiesModel]()//[CD_City]()
    var arrTypeCars = [CarTypeData]()
    var arrCarParts = [VehicleDetailsPartStatuses]()
    var arrPartsStatus = [Int]()
    var arrDeletedImageIds = [Int]()
    var arrDeletedPartIds = [Int]()
    var isPartsAvailable = true
//    var arrRideType = [CD_Ride_Type]()
    var arrVehicleCategory = [CD_Vehicle_Category]()
    
    var arrUpdatedParts = [VehicleDetailsPartStatuses]()
    var arrUpdatedPartStatuses = [Int]()
    
    var arrTractionType = NSArray()
    var addressLat: String = "0.0"
    var addressLng: String = "0.0"
    var isNotFound:String = ""
    let vehicleTypePickerViewIdentifier = "vehicleTypePickerView"
    var vehicleTypePickerView = UIPickerView()
    
    let vehicleRideTypePickerViewIdentifier = "vehicleRideTypePickerView"
    var vehicleRideTypePickerView = UIPickerView()
    
    let vehicleTractionTypePickerViewIdentifier = "vehicleTractionTypePickerView"
    var vehicleTractionTypePickerView = UIPickerView()
    
    let countryPickerViewIdentifier = "countryPickerView"
    var countryPickerView = UIPickerView()
    
    let statePickerViewIdentifier = "statePickerView"
    var statePickerView = UIPickerView()
    
    let cityPickerViewIdentifier = "cityPickerView"
    var cityPickerView = UIPickerView()
    
    let rentalExpiryDatePickerIdentifier = "vehicleExpiryDatePicker"
    let rentalExpiryDatePicker = UIDatePicker()
    
    var headerTitle: String = "Add Vehicle"
    var isVehicleTypeSelected: Bool = false
    var isVehicleImageSelected: Bool = false
    var isVehicleTractionTypeSelected: Bool = false
    var isVehicleRideTypeSelected: Bool = false
    var isVehicleCountrySelected: Bool = false
    var isVehicleStateSelected: Bool = false
    var isVehicleCitySelected: Bool = false
    var isVehicleAddressSelected: Bool = false
    var isExpiryDateSelectyed: Bool = false
    var isDetailsUpdate: Bool = false
    let imagePicker = UIImagePickerController()
    var btnSaveTitle = "Save"
    var imagesCount = 1
    var vehicleImages:[UIImage] = []
    var selectedItem:Int?
    
    var selectedCountryID: Int = 0
    var selectedStateID: Int = 0
    var selectedCityID: Int = 0
    var selectedTypeId:Int16?
    var selectedCarPart:Int?
    let dateFormatter: DateFormatter = DateFormatter()
    var selectedPartIndex:Int=0
    var vehicleImagesData = [VehicleDetailsImage]()
    var vehiclePartsData = [VehicleDetailsPartStatuses]()
    
    //MARK: - Buttons
    @IBOutlet weak var btnSave: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    @IBAction func sliderValueChanged(_ sender: StepSlider) {
        
    }
    
    @IBAction func btnSearchAddressClicked(_ sender: Any) {
        let vc = loadViewController(StoryBoardName: "Location", VCIdentifer: "LocationPickerVC") as! LocationPickerVC
        vc.delegateSelectedPlace = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    @IBAction func clickedOnNotApplicable(_ sender: Any) {
        arrPartsStatus[selectedPartIndex] = arrPartsStatus[selectedPartIndex] == -1 ? 0 : -1
        checkBoxNotApplicable.image = arrPartsStatus[selectedPartIndex] == -1 ? UIImage(named: "checked"):UIImage(named: "unchecked")
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        vwRating.isHidden = true
        vwShadow.isHidden = true
    }
    
    @IBAction func btnOkClicked(_ sender: Any) {
        vwRating.isHidden = true
        vwShadow.isHidden = true
        arrPartsStatus[selectedPartIndex] = arrPartsStatus[selectedPartIndex] == -1 ? -1 : Int(sliderRating.index)
        collVehicleParts.reloadData()
    }
    
    @IBAction func saveCar(_ sender: UIButton) {
        self.view.endEditing(true)
        guard validateData() else { return }
        
        if addressLat == "" || addressLng == ""{
            
        }
        
        let brand = vehicleBrandTF.text!
        let plate = vehiclePlateTF.text!
        let model = vehicleModelTF.text!
        let address = vehicleAddressTV.text!
        let lat = addressLat
        let lng = addressLng
        let price = vehiclePriceTF.text!
        let vehicleCategotry = vehicleCategoryTF.text!
        let VehicleTypeID = "\(selectedTypeId ?? 0)"
        var carID : String? = nil
        if isDetailsUpdate {
            carID = "\(myVehicleData.id ?? 0)"
        }
        
//        MyVehiclesApi.sharedInstance.addEditVehicleRequest(myView: self.view, carID: carID, carBrand: brand, carPlate: plate, carModel: model, carType: car_type, carRideType: rideType, carAddress: address, carLat: lat, carLng: lng, carCity: city_id, carState: state_id, carCountry: country_id, carPrice: price, carImage: image) { (hasError) in
        
        if let indexAddImage = vehicleImages.firstIndex(where: {$0 == UIImage(named: "add")}){
            vehicleImages.remove(at: indexAddImage)
        }
        
        var cityID = ""
        if !isDetailsUpdate{
            cityID = isNotFound
        }else{
            cityID =  "\(selectedCityID)"
        }
        
        MyVehiclesApi.sharedInstance.addEditVehicleRequest(myView: self.view, carID: carID, carPlate: plate, vehicleTypeId: VehicleTypeID, carCityId: cityID, carStateId:"\(selectedStateID)", carCountryID:"\(selectedCountryID)", carBrand: brand, carModel: model, carPrice: price, carInsuranceId: vehicleInsuranceIDTF.text!, insurance_company: vehicleInsuranceCompanyTF.text!, insurance_expiry_date: vehicleInsuranceExpirytDateTF.text!, chassis_number: vehicleInsuranceChassisNumTF.text!, registration_number: vehicleRegistrationNumTF.text!, number_of_seats: vehicleNumOfSeatsTF.text!, type_of_traction: vehicleTractionTypeTFTF.text!, carCity: vehicleCityTF.text ?? "", carState: vehicleStateTF.text ?? "", carCountry: vehicleCountryTF.text ?? "", carLat: lat, carLng: lng, carRideType: vehicleCategotry, carAddress: address, vehicleImages:vehicleImagesData, addVehicleImages: vehicleImages, deletedImagesIds: arrDeletedImageIds, deletedPartIds: arrDeletedPartIds, partStatuses:arrPartsStatus, vehicleParts: arrCarParts, arrUpdatedParts: arrUpdatedParts, arrUpdatedPartStatuses: arrUpdatedPartStatuses, isPartsAvailable: isPartsAvailable) { (hasError) in
            
//        MyVehiclesApi.sharedInstance.addEditVehicleRequest(myView: self.view, carID: carID, carBrand: brand, carPlate: plate, carModel: model, carType: car_type, carRideType: rideType, carAddress: address, carLat: lat, carLng: lng, carCity: vehicleCityTF.text ?? "", carState: vehicleStateTF.text ?? "", carCountry: vehicleCountryTF.text ?? "", carCityId: self.isNotFound, carPrice: price, carImage: image) { (hasError) in
        
            if !hasError {
                if self.isDetailsUpdate {
                    self.navigationController?.popToRootViewController(animated: false)
                }
                else {
                    self.backTap()
                }
            }
        }

    }
 
}
