//
//  CellVehicleParts.swift
//  GO2
//
//  Created by C115 on 02/11/21.
//  Copyright © 2021 Juan Cardozo. All rights reserved.
//

import UIKit

class CellVehicleParts: UICollectionViewCell {
    @IBOutlet weak var lblPartName: UILabel!
    @IBOutlet weak var lblPartStatus: UILabel!
    @IBOutlet weak var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        lblPartName.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
//        lblPartName.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        lblPartStatus.clipsToBounds = true
        lblPartStatus.layer.cornerRadius = lblPartStatus.frame.width/2
    }
}
