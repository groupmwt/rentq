//
//  ProfileEditExt.swift
//  GO2
//
//  Created by NC2-46 on 14/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import CountryPickerView

extension ProfileEditVC {
    func setupUI() {
        setBackBtn()
        DispatchQueue.main.async {
            //Style
            self.navigationController?.customNavBar()
            self.globalView.setBackgroundWithTopBarToView()
            
            self.dataView.backgroundColor = Color.DataViewBG
            self.globalView.backgroundColor = Color.DataViewBG
            self.dividerView.backgroundColor = Color.DividerGreen
            self.labelFirstName.setProfileNameGrey()
            self.labelLastName.setProfileNameGrey()
            self.labelEmail.setProfileNameGrey()
            self.labelMobile.setProfileNameGrey()
            self.labelPlaces.setProfileNameGrey()
//            self.txtHomeAddress.addShadow()
//            self.txtWorkAddress.addShadow()
            self.textFieldFirstName.addShadow()
            self.textFieldLastName.addShadow()
            self.textFieldEmail.addShadow()
            self.phoneView.addShadow()
            self.btnEditSave.backgroundColor = Color.buttonBG
        }
        
        let profileImageTap = UITapGestureRecognizer(target: self, action: #selector(self.profileImageTapDetected))
        self.avatarImg.isUserInteractionEnabled = true
        self.avatarImg.addGestureRecognizer(profileImageTap)
        self.avatarImg.layer.cornerRadius = self.avatarImg.frame.width/2.0
        txtHomeAddress.delegate = self
        txtWorkAddress.delegate = self
        self.txtHomeAddress.textColor = .black
        self.txtWorkAddress.textColor = .black
        avatarImg.isUserInteractionEnabled = true
        let finalUrl : String = getConexionAPI_Production() + (userData.image?.url ?? "" )
        let url = URL.init(string: finalUrl)
        self.avatarImg.sd_setShowActivityIndicatorView(true)
        self.avatarImg.sd_setIndicatorStyle(.gray)
        self.avatarImg.sd_setImage(with: url , placeholderImage: nil)
        self.textFieldFirstName.text = userData.firstName
        self.textFieldLastName.text = userData.lastName
        self.textFieldEmail.text = userData.email
        self.textFieldMobile.text = userData.phoneNumber
        self.txtHomeAddress.text = userData.homeAddress
        self.txtWorkAddress.text = userData.officeAddress
        if self.txtHomeAddress.isEmpty {
            self.txtHomeAddress.text = self.homeAddressPlaceholder
            self.txtHomeAddress.textColor = .gray
        }
        if self.txtWorkAddress.isEmpty {
            self.txtWorkAddress.text = self.workAddressPlaceholder
            self.txtWorkAddress.textColor = .gray
        }
        self.mobileCodePicker.dataSource = self
//        self.mobileCodePicker.setCountryByPhoneCode("+\(userData.countryCode ?? "1")")
        self.mobileCodePicker.setCountryByCode(userData.countryCode ?? "")
        btnEditSave.backgroundColor = Color.buttonBG
    }
    
    //Action
    @objc func profileImageTapDetected() {
        imagePicker.delegate = self
        let optionMenu = UIAlertController(title: nil, message: "Add Photo", preferredStyle: .actionSheet)
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default, handler:{
            (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler:{
            (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        
        let cancleAction = UIAlertAction(title: "Cancel", style: .cancel, handler:{
            (alert: UIAlertAction!) -> Void in
            print("Cancel")
        })
        
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(cancleAction)
        optionMenu.popoverPresentationController?.sourceView = avatarImg
        self.present(optionMenu, animated: true, completion: nil)
    }
    
}

extension ProfileEditVC : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.tag == 4 {
            if textView.text == homeAddressPlaceholder {
                textView.text = ""
                textView.textColor = .black
            }
            else {
                textView.textColor = .gray
            }
        }
        else if textView.tag == 5 {
            if textView.text == workAddressPlaceholder {
                textView.text = ""
                textView.textColor = .black
            }
            else {
                textView.textColor = .gray
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.tag == 4 {
            if textView.text.isEmpty {
                textView.text = homeAddressPlaceholder
                textView.textColor = .gray
            }
            else {
                textView.textColor = .black
            }
        }
        else if textView.tag == 5 {
            if textView.text.isEmpty {
                textView.text = workAddressPlaceholder
                textView.textColor = .gray
            }
            else {
                textView.textColor = .black
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        textView.textColor = .black
        return true
    }
    
}

extension ProfileEditVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            openGallery()
        }
    }
    func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            self.avatarImg.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
}


//Mark: - Extension For Validation
extension ProfileEditVC {
    //Mark: - Validate Function for checking fileds proper data.
    func validateData() -> Bool {
        self.view.endEditing(true)
        guard !(textFieldFirstName.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter first name")
            return false
        }
        guard !(textFieldLastName.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter last name")
            return false
        }
        guard !(textFieldMobile.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter phone number")
            return false
        }
        guard (textFieldMobile.isValidMobileNumber) else{
            displayMyAlertMessage(userMessage: "Phone number must be 6-15 digits")
            return false
        }
        return true
    }
}


extension ProfileEditVC: CountryPickerViewDataSource {
    //func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
  //  }
    
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Pick country code"
    }
    
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
    
}
