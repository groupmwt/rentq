//
//  Enum+Struct.swift
//  PubCard
//
//  Created by NC2-46 on 27/06/18.
//  Copyright © 2018 NC2-46. All rights reserved.
//

import Foundation
// Enum
enum VehicleDetailsEnum: Int {
    case Model=0
    case Brand
    case Plate
    case VehicleType
    case InsuranceId
    case InsuranceCompany
    case InsuranceExpiry
    case ChassisNumber
    case RegistrationNumber
    case NumberOfSeats
    case Traction
    case VehicleParts
    case Country
    case State
    case City
    case Location
    static let totalValues = 16
}


enum HistoryDetailsEnum: Int {
    case Name=0
    case Contact
    case Address
    case StartDate
    case EndDate
    case Model
    case Brand
    case Plate
    case VehicleType
    case RideType
    case Country
    case State
    case City=12
    static let totalValues = 13
    
//    case StartDate=0
//    case EndDate
//    case Model
//    case Brand
//    case Plate
//    case VehicleType
//    case RideType
//    case Country
//    case State
//    case City
//    case Name
//    case Contact
//    case Address=12
//    static let totalValues = 13
}
