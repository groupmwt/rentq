//
//  GetNearByVehicleData.swift
//
//  Created by NC2-46 on 21/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class GetNearByVehicleData: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let plate = "plate"
    static let lng = "lng"
    static let id = "id"
    static let address = "address"
    static let model = "model"
    static let carType = "car_type"
    static let lat = "lat"
    static let rideType = "ride_type"
    static let brand = "brand"
  }

  // MARK: Properties
  public var plate: String?
  public var lng: String?
  public var id: Int?
  public var address: String?
  public var model: String?
  public var carType: String?
  public var lat: String?
  public var rideType: String?
  public var brand: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    plate = json[SerializationKeys.plate].string
    lng = json[SerializationKeys.lng].string
    id = json[SerializationKeys.id].int
    address = json[SerializationKeys.address].string
    model = json[SerializationKeys.model].string
    carType = json[SerializationKeys.carType].string
    lat = json[SerializationKeys.lat].string
    rideType = json[SerializationKeys.rideType].string
    brand = json[SerializationKeys.brand].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = plate { dictionary[SerializationKeys.plate] = value }
    if let value = lng { dictionary[SerializationKeys.lng] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = model { dictionary[SerializationKeys.model] = value }
    if let value = carType { dictionary[SerializationKeys.carType] = value }
    if let value = lat { dictionary[SerializationKeys.lat] = value }
    if let value = rideType { dictionary[SerializationKeys.rideType] = value }
    if let value = brand { dictionary[SerializationKeys.brand] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.plate = aDecoder.decodeObject(forKey: SerializationKeys.plate) as? String
    self.lng = aDecoder.decodeObject(forKey: SerializationKeys.lng) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? String
    self.lat = aDecoder.decodeObject(forKey: SerializationKeys.lat) as? String
    self.rideType = aDecoder.decodeObject(forKey: SerializationKeys.rideType) as? String
    self.brand = aDecoder.decodeObject(forKey: SerializationKeys.brand) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(plate, forKey: SerializationKeys.plate)
    aCoder.encode(lng, forKey: SerializationKeys.lng)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(model, forKey: SerializationKeys.model)
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(lat, forKey: SerializationKeys.lat)
    aCoder.encode(rideType, forKey: SerializationKeys.rideType)
    aCoder.encode(brand, forKey: SerializationKeys.brand)
  }

}
