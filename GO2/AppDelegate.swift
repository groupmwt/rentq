//
//  AppDelegate.swift
//  GO2
//
//  Created by GNM on 5/19/17.
//  Copyright © 2017 Juan Cardozo. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import MMDrawerController
import IQKeyboardManagerSwift
import UserNotifications
import Alamofire
import SwiftyJSON
import CoreData
import Firebase
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate, MessagingDelegate{

    var window: UIWindow?
    let userDefaults = UserDefaults.standard
    var centerContainer: MMDrawerController?
    var mainNavigationController : UINavigationController?
    var country : String?
    var state : String?
    var city : String?
    var arrCountrys = [CountriesModel]()
    var arrStates = [StatesModel]()
    var arrCities = [CitiesModel]()
    var locationDict = NSDictionary()
    var locationManager = CLLocationManager()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        showBaseUrl()
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
//        DispatchQueue.main.async {
//            CoreDBManager.sharedDatabase.saveLocation()
//        }
        // Override point for customization after application launch.
        
//        GMSPlacesClient.provideAPIKey("AIzaSyDZMDfPfZmLC2DaDn-hYeGmH_Wj1AKJBho")
//        GMSServices.provideAPIKey("AIzaSyDZMDfPfZmLC2DaDn-hYeGmH_Wj1AKJBho")
        
        GMSPlacesClient.provideAPIKey("AIzaSyDf61jE9GyRxj6fo2cP8z3vQfTrCz9tRJE")
        GMSServices.provideAPIKey("AIzaSyDf61jE9GyRxj6fo2cP8z3vQfTrCz9tRJE")
        // Override point for customization after application launch.
//        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        UINavigationBar.appearance().barStyle = .blackOpaque
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isStatusBarHidden = false
        
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.backgroundColor = Color.screenBG
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        }
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        //mainNavigationController?.navigationBar.tintColor = .white
        addNotificationsPermissions(application : application)
       
//        CoreDBManager.sharedDatabase.saveVehicleType()
        
        goToHome()
        saveJsonFile()
        
        let data = CoreDBManager.sharedDatabase.getDataFromFile()
        locationDict = data
        let arrCountriesFromJson = data.value(forKey: "countries") as? NSArray ?? NSArray()
        self.arrCountrys = JSON(arrCountriesFromJson).to(type: CountriesModel.self) as! [CountriesModel]
        
        let arrStatesFromJson = data.value(forKey: "states") as? NSArray ?? NSArray()
        self.arrStates = JSON(arrStatesFromJson).to(type: StatesModel.self) as! [StatesModel]
        
        let arrCitiesFromJson = data.value(forKey: "cities") as? NSArray ?? NSArray()
        self.arrCities = JSON(arrCitiesFromJson).to(type: CitiesModel.self) as! [CitiesModel]
//        if userDefaults.value(forKey: "locationCountries") == nil || userDefaults.value(forKey: "locationStates") == nil || userDefaults.value(forKey: "locationCities") == nil{
//            let data = CoreDBManager.sharedDatabase.getDataFromFile()
//            userDefaults.set(data.value(forKey: "countries"), forKey: "locationCountries")
//            userDefaults.set(data.value(forKey: "states"), forKey: "locationStates")
//            userDefaults.set(data.value(forKey: "cities"), forKey: "locationCities")
//        }
        return true
    }
    
    func saveJsonFile(){
        if let path = Bundle.main.path(forResource: "Locations", ofType: "json") {
            do {
                let fileUrl = URL(fileURLWithPath: path)
                // Getting data from JSON file using the file URL
                let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
                   let fileName = "Locations.json"
                if let documentPath = paths.first {
                    let filePath = NSMutableString(string: documentPath).appendingPathComponent(fileName)
                    let URL = NSURL.fileURL(withPath: filePath)
                    
                    let fileManager = FileManager.default
                    if fileManager.fileExists(atPath: filePath) {
                        print("FILE AVAILABLE",filePath)
                        
                    } else {
                        print("FILE NOT AVAILABLE")
                        do{
                            try data.write(to: URL)
                            print("file saved")
                        }catch{
                            print("error is .........",error.localizedDescription)
                        }
                    }
                }
            } catch {
                print("Error in reading json",error.localizedDescription)
            }
        }
    }
    
    func showBaseUrl() {
        let alert = UIAlertController(title: "BASE URL", message: "Please enter your base url here", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter Base Url here"
            textField.text = getConexionAPI_Production()
        }
        
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { alertAction -> Void in
            let textField = alert.textFields![0] as UITextField
            print(textField.text!)
            setConexionAPI_Production(url: textField.text!)
            //self.goToHome()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { alertAction -> Void in
            //self.goToHome()
        }))
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func goToHome() {
        if isUserLogin() {
//            CoreDBManager.sharedDatabase.saveVehicleType()
            
            let sideMenuVC = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
            
            let centerVC = UIStoryboard(name: "Map", bundle: nil).instantiateViewController(withIdentifier: "MapVC") as! MapVC

            let leftSideNav = UINavigationController(rootViewController: sideMenuVC)
            let centerNav = UINavigationController(rootViewController: centerVC)
            centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav,rightDrawerViewController:nil)
            centerContainer?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.all
            centerContainer?.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
            window?.rootViewController = centerContainer
        }
        else {
//            CoreDBManager.sharedDatabase.saveVehicleType()
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginController")
            
            mainNavigationController = UINavigationController.init(rootViewController: vc)
            mainNavigationController?.navigationBar.barStyle = .blackOpaque
            mainNavigationController?.isNavigationBarHidden = true
            window?.rootViewController = mainNavigationController
        }
    }
    
    // MARK: - App Functions
    func addNotificationsPermissions(application : UIApplication){
//        UNUserNotificationCenter.current().delegate = self
//        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//        UNUserNotificationCenter.current().requestAuthorization(
//            options: authOptions,
//            completionHandler: {_, _ in })
//
//        application.registerForRemoteNotifications()
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { _, _ in }
          )
        } else {
          let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
              UserDefaults.standard.set(token, forKey: kDToken)
          }
        }
    }
    
    // MARK: - Notifications Handler
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("DToken",token)
        UserDefaults.standard.set(token, forKey: kDToken)
        UserDefaults.standard.synchronize()
        Messaging.messaging().apnsToken = deviceToken
        Messaging.messaging().isAutoInitEnabled = true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        let aps = userInfo[AnyHashable("0")]! as! NSDictionary
        print("aps",aps)
                
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
        
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("userInfo",response)
        var vehicleData : MyHistoryData!
        let userInfo = response.notification.request.content.userInfo
        let data = userInfo as NSDictionary
        print("data",data)
        
        let apiReturn = JSON(data)
        if let object = apiReturn.to(type: MyHistoryData.self){
            vehicleData = object as! MyHistoryData
        }
        let centerViewController = UIStoryboard(name: "History", bundle: nil).instantiateViewController(withIdentifier: "HistoryDetailsVC") as! HistoryDetailsVC
        
        centerViewController.historyDetails = vehicleData
        centerViewController.isViewPresent = true
        let navigationController  = self.window?.rootViewController
        navigationController?.present(centerViewController, animated: true, completion: nil)
        UIApplication.shared.applicationIconBadgeNumber = 0;
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
       application.applicationIconBadgeNumber = 0
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
       application.applicationIconBadgeNumber = 0
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        CoreDBManager.sharedDatabase.saveContext()
    }
   
}

extension AppDelegate :  CLLocationManagerDelegate
{

    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }

    // Below Mehtod will print error if not able to update location.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error Location")
    }


    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        //Access the last object from locations to get perfect current location
        if let location = locations.last {

            let geocoder = GMSGeocoder()
            let myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
            geocoder.reverseGeocodeCoordinate(myLocation) { response, error in
                if let address = response?.firstResult() {
//                    let r = address.country?.folding(options: .diacriticInsensitive, locale: nil)
                    AppDel.country = address.country
                    AppDel.state = address.administrativeArea
                    AppDel.city = address.locality
                    // 3
                }
            }
            
//            let myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
//
//            geocode(latitude: myLocation.latitude, longitude: myLocation.longitude) { placemark, error in
//                guard let placemark = placemark, error == nil else { return }
//                // you should always update your UI in the main thread
//                DispatchQueue.main.async {
//                    //  update UI here
//                    print("address1:", placemark.thoroughfare ?? "")
//                    print("address2:", placemark.subThoroughfare ?? "")
//                    print("city:",     placemark.locality ?? "")
//                    print("state:",    placemark.administrativeArea ?? "")
//                    print("zip code:", placemark.postalCode ?? "")
//                    print("country:",  placemark.country ?? "")
//                    self.country = placemark.country ?? ""
//                    self.state = placemark.administrativeArea ?? ""
//                    self.city = placemark.locality ?? ""
//                }
//            }
        }
        manager.stopUpdatingLocation()
        
    }
}
