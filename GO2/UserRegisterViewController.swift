//
//  UserRegisterViewController.swift
//  GO2
//
//  Created by Juan Cardozo on 30/6/17.
//  Copyright © 2017 GNM. All rights reserved.
//

import UIKit
import CountryPickerView

class UserRegisterViewController: UIViewController {
  
    //MARK: - Views
    @IBOutlet weak var globalView: UIView!
 
    //MARK: - textfields
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var mobileCodePicker: CountryPickerView!
    @IBOutlet weak var btnPickCountryCode: UIButton!
    @IBOutlet weak var phoneView: UIViewX!
    @IBOutlet weak var btnSignup: UIButtonX!
    
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
       
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Sign Up"
        
    }
    
    
    @IBAction func btnCountryCodeAction(_ sender: Any) {
        if let nav = self.navigationController {
            self.title = ""
            self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back_icon")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "back_icon")
            mobileCodePicker.showCountriesList(from: nav)
        }
    }
    
    
    //MARK: Login Action
    @IBAction func signUpAction(_ sender: UIButton) {
        self.view.endEditing(true)
        guard validateData() else { return }
        let userFirstName = txtFirstName.text!
        let userLastName = txtLastName.text!
        let userEmail = txtEmail.text!
        let userMobile = txtPhone.text!
        let userPassword = txtPassword.text!
        let userPhoneCode = mobileCodePicker.selectedCountry.phoneCode.replacingOccurrences(of: "+", with: "")
        
        
        // MARK: API Request
        UserApi.sharedInstance.registerRequest(myView: self.view, firstName: userFirstName, lastname: userLastName, email: userEmail, phonenumber: userMobile, countryCode: userPhoneCode, password: userPassword) { (user) in
   
            print("New Case Request: \(String(describing: user))")
            
            if user != nil {
                self.redirectToDashboard()
            }
            else {
                displayMyAlertMessage(userMessage: "Please check your Internet Connection")
            }
        }
    }

}



