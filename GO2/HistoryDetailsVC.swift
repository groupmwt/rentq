//
//  HistoryDetailsVC.swift
//  GO2
//
//  Created by NC2-46 on 27/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit

class HistoryDetailsVC: UIViewController {
    var historyDetails : MyHistoryData!
    var countryData: String?
    var stateData: String?
    var cityData: String?
    var isViewPresent : Bool = false
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var headerBar: UIView!
    @IBOutlet weak var labelBarTitle: UILabel!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var btnCloseHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tblVIewHeight: NSLayoutConstraint!
    var headerTitle: String = "Vehicle Profile"
    var navigationTitle: String = "History Details"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }

    @IBAction func btnClose(_ sender: Any) {
       self.dismiss(animated: true, completion: nil)
    }

}
