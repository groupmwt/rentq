//
//  Statusbar+Extension.swift
//  Motiv8
//
//  Created by NC2-46 on 26/03/18.
//  Copyright © 2018 NC2-46. All rights reserved.
//

import Foundation
import UIKit
extension UIApplication {
    //MARK: - set statusBar View
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
