//
//  UISegmentedControl+Extension.swift
//  Motiv8
//
//  Created by NC2-46 on 24/03/18.
//  Copyright © 2018 NC2-46. All rights reserved.
//

import Foundation
import UIKit
extension UISegmentedControl{
    func removeBorder(){
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.clear.cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
        
        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.clear.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        self.tintColor = UIColor(red: 21/255, green: 1/255, blue: 190/255, alpha: 1.0)
        self.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 21/255, green: 1/255, blue: 190/255, alpha: 1.0)], for: .normal)
        self.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 21/255, green: 1/255, blue: 190/255, alpha: 1.0)], for: .selected)
        self.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 21/255, green: 1/255, blue: 190/255, alpha: 1.0)], for: .highlighted)
        let font = UIFont.systemFont(ofSize: 16)
        self.setTitleTextAttributes([NSAttributedStringKey.font: font],
                                                for: .normal)
    }
    
    func addUnderlineForSelectedSegment(vcWidth : CGFloat){
        removeBorder()
        let underlineWidth: CGFloat = vcWidth / CGFloat(self.numberOfSegments)
            //self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 1.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor(red: 21/255, green: 1/255, blue: 190/255, alpha: 1.0)
        underline.tag = 1
        self.addSubview(underline)
    }
    
    func changeUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}

        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        UIView.animate(withDuration: 0.1, animations: {
            underline.frame.origin.x = underlineFinalXPosition
        })
    }
}
