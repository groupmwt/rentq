

import Foundation

struct TinyToastModel {
    var message: String
    var valign: TinyToastDisplayVAlign
    var duration: TimeInterval
    
    init(message: String, valign: TinyToastDisplayVAlign, duration: TimeInterval) {
        self.message = message
        self.valign = valign
        self.duration = duration
    }
}
