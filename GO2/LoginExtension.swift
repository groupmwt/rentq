//
//  LoginExtension.swift
//  GO2
//
//  Created by NC2-46 on 10/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import MMDrawerController

extension LoginController {
    func setupUI() {
        navigationController?.customNavBar()
        emailTextField.delegate = self
        passwordTextField.delegate = self

        emailTextField.textType = .emailAddress
        passwordTextField.textType = .password
        
        loginBtn.backgroundColor = Color.buttonBG
    }
    
}


extension LoginController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//Mark: - Extension For Validation
extension LoginController{
    //Mark: - Validate Function for checking fileds proper data.
    func validateData() -> Bool {
        guard !(emailTextField.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter email address")
            return false
        }
        guard (emailTextField.hasValidEmail) else{
            displayMyAlertMessage(userMessage: "Please enter valid email address")
            return false
        }
        guard !(passwordTextField.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter password")
            return false
        }
        return true
    }
}
