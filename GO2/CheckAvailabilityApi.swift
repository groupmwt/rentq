//
//  CheckAvailabilityApi.swift
//  GO2
//
//  Created by NC2-46 on 23/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CheckAvailabilityApi {
    
    // MARK: - Singelton
    static let sharedInstance:CheckAvailabilityApi = CheckAvailabilityApi()
    
    private init() {}
  
    //private let pathUrl = getConexionAPI_Production() + general.versionPath
    
    
    
    // ********************************************************
    // MARK: get offers calendar API Request
    // ********************************************************
    func get_offers_calendarRequest(myView: UIView,carID:String, year:String, month:String, completion: @escaping ([CheckAvailabilityData]?) -> Void)  {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)

        let parameters : [String : AnyObject] = [
            "year": year as AnyObject,
            "month": month as AnyObject
        ]
        
        let Auth_header = [ "access-token": getAccessToken(),
                            "client": getClientToken(),
                            "uid": getUID()]
        
        print("parameters: \(parameters)")
        print("parameters: \(Auth_header)")
        
        let urlString = getPathUrl() + APIS.addNewCar + "/\(carID)" + APIS.getOffersCalendarPath
        
        Alamofire.request(urlString, method: .get, parameters: parameters, headers: Auth_header).validate().responseJSON{
            response in
            var checkAvailabilityResult = [CheckAvailabilityData]()
           
            
            switch response.result {
            case .success:
                print("get_offers_calendarRequest - API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                        completion(nil)
                    }
                    else {
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        
                        //TKP
                        let result = value["data"] as? NSArray
                        let apiReturn = JSON(result)
//                        let apiReturn = JSON(value)
                        if let object = apiReturn.to(type: CheckAvailabilityData.self){
                            checkAvailabilityResult = object as! [CheckAvailabilityData]
                        }
                        print(checkAvailabilityResult)
                       
                        completion(checkAvailabilityResult)
                    }
                }
                break
                
            case .failure(let error):
                print("ERROR IN CONECTION: \(error)")
                displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                completion(nil)
                break
            }
        }
    }
    
    
    
    
    // ********************************************************
    // MARK: edit offers calendar API Request
    // ********************************************************
    func updateOffersRequest(myView: UIView,carID:String, offerID:String,price:String, completion: @escaping (CheckAvailabilityData) -> Void)  {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header = [ "access-token": getAccessToken(),
                            "client": getClientToken(),
                            "uid": getUID() ]
        
        print("parameters: \(Auth_header)")
        
        let urlString = getPathUrl() + APIS.addNewCar + "/\(carID)" + APIS.updateOffersCalendarPath + "/\(offerID)"
 
        Alamofire.upload(multipartFormData: { (multipartFormData) in

            multipartFormData.append(price.data(using: .utf8)!, withName: "price")
            
        }, usingThreshold: UInt64.init(), to: urlString, method: .patch, headers: Auth_header) { encodingResult in
            var checkAvailabilityResult : CheckAvailabilityData!
            switch encodingResult {
            case .success(let upload, _,_ ):
                upload.responseJSON { response in
                    if let value: AnyObject = response.result.value as AnyObject? {
                        
                        if let errors = value["error"] as? String {
                            displayMyAlertMessage(userMessage: errors)
                            myView.stopIndicator(indicator: indicator, container: container)
                        }
                        else {
                            updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                            let result = value["data"] as! NSDictionary
                            
                            let apiReturn = JSON(result)
                            if let object = apiReturn.to(type: CheckAvailabilityData.self){
                                checkAvailabilityResult = object as? CheckAvailabilityData
                            }
                            print(checkAvailabilityResult)
                            
                            completion(checkAvailabilityResult)
                        }
                    }
                    else {
                        displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                }
                break
            case .failure(let encodingError):
                print("ERROR IN CONECTION: \(encodingError)")
                displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                break
                
            }
        }
    }
    
  
    // ********************************************************
    // MARK: add offers calendar API Request
    // ********************************************************
    func addOffersRequest(myView: UIView,carID:String, startDate:String,endDate:String,price:String, completion: @escaping (_ hasError: Bool) -> Void)  {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header = [ "access-token": getAccessToken(),
                            "client": getClientToken(),
                            "uid": getUID() ]
        
        print("parameters: \(Auth_header)")
        
        let urlString = getPathUrl() + APIS.addNewCar + "/\(carID)" + APIS.addOffersCalendarPath
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(startDate.data(using: .utf8)!, withName: "start_date")
            multipartFormData.append(endDate.data(using: .utf8)!, withName: "end_date")
            multipartFormData.append(price.data(using: .utf8)!, withName: "price")
            
        }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: Auth_header) { encodingResult in
            switch encodingResult {
            case .success(let upload, _,_ ):
                upload.responseJSON { response in
                    if let value: AnyObject = response.result.value as AnyObject? {
                        
                        if let status = value["status"] as? Bool  {
                            if status == false {
                                if let errors = value["errors"] as? NSArray {
                                    var mess = ""
                                    for err in errors {
                                        mess += (err as! String) + "\n"
                                    }
                                    displayMyAlertMessage(userMessage: mess)
                                }
                                
                                
                                myView.stopIndicator(indicator: indicator, container: container)
                                //completion(true)
                            }
                            else {
                                myView.stopIndicator(indicator: indicator, container: container)
                                updateHeaderData(response: JSON((response.response?.allHeaderFields)!))

                                completion(false)
                            }
                        }
                        else {
                            displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
                            myView.stopIndicator(indicator: indicator, container: container)
                            //completion(true)
                        }
                    }
                    
                    else {
                        displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                }
                break
            case .failure(let encodingError):
                print("ERROR IN CONECTION: \(encodingError)")
                displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                break
                
            }
        }
    }
    
    // ********************************************************
    // MARK: add offers calendar API Request
    // ********************************************************
    func removeOffersRequest(myView: UIView,carID:String, startDate:String,endDate:String, completion: @escaping (_ hasError: Bool) -> Void)  {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header = [ "access-token": getAccessToken(),
                            "client": getClientToken(),
                            "uid": getUID() ]
        
        print("parameters: \(Auth_header)")
        
        let urlString = getPathUrl() + APIS.addNewCar + "/\(carID)" + APIS.deleteOffersCalendarPath
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(startDate.data(using: .utf8)!, withName: "start_date")
            multipartFormData.append(endDate.data(using: .utf8)!, withName: "end_date")
            
        }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: Auth_header) { encodingResult in
            switch encodingResult {
            case .success(let upload, _,_ ):
                upload.responseJSON { response in
                    if let value: AnyObject = response.result.value as AnyObject? {
                        
                        if let status = value["status"] as? Bool  {
                            if status == false {
                                if let errors = value["errors"] as? NSArray {
                                    var mess = ""
                                    for err in errors {
                                        mess += (err as! String) + "\n"
                                    }
                                    displayMyAlertMessage(userMessage: mess)
                                }
                                
                                
                                myView.stopIndicator(indicator: indicator, container: container)
                                //completion(true)
                            }
                            else {
                                myView.stopIndicator(indicator: indicator, container: container)
                                updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                                
                                completion(false)
                            }
                        }
                        else {
                            displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
                            myView.stopIndicator(indicator: indicator, container: container)
                            //completion(true)
                        }
                    }
                        
                    else {
                        displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                }
                break
            case .failure(let encodingError):
                print("ERROR IN CONECTION: \(encodingError)")
                displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                break
                
            }
        }
    }
}
