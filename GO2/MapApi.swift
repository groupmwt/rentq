//
//  MapApi.swift
//  GO2
//
//  Created by NC2-46 on 17/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation

//
//  UserApi.swift
//  GO2
//
//  Created by GNM on 5/23/17.
//  Copyright © 2017 Juan Cardozo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MapApi {
    
    // MARK: - Singelton
    static let sharedInstance:MapApi = MapApi()
    
    private init() {}

    
    //    private let pathUrl = general.conexionAPI_Production + general.versionPath
   // private let pathUrl = getConexionAPI_Production() + general.versionPath
    
    // MARK: - ApiMethods
    
    // ********************************************************
    // MARK: get_nearby_vehicles API Request
    // ********************************************************
    func getNearbyVehiclesRequest(myView: UIView,cityName:String, latitude:String, longitude:String, completion: @escaping ([GetNearByVehicleData]) -> Void) {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID()]

        
        //let urlString = "\(myBaseURL)/{name:adriana lima}"
//        guard let urlString = cityName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
//            //Invalid URL
//            return
//        }
        
//        let encodedURL = getPathUrl() + APIS.getNearbyVehiclesPath + urlString
        let encodedURL = getPathUrl() + APIS.getAllMyCars + APIS.getNearbyVehiclesPath + "?lat=\(latitude)&lng=" + "\(longitude)"
        print("URL: \(encodedURL)")
        print("Parameter: \(Auth_header)")
//        Alamofire.request(encodedURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON { (response) in
        
        Alamofire.request(encodedURL, method: .get, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON { (response) in
            var nearByVehicleData = [GetNearByVehicleData]()
            var errorMessage = ""
            switch response.result {
                
            case .success:
                print("getNearbyVehiclesRequest -- API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                    else {
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        let result = value["data"] as! NSArray
                        
                        let apiReturn = JSON(result)
                        if let object = apiReturn.to(type: GetNearByVehicleData.self){
                            nearByVehicleData = object as! [GetNearByVehicleData]
                        }
                       // print(carType)
                        myView.stopIndicator(indicator: indicator, container: container)
                        completion(nearByVehicleData)
                    }
                }
                break
                
            case .failure( _):
                
                if let data = response.data {
                    let responseJSON = JSON(data)
                    
                    if let message: String = responseJSON["errors"].array?[0].rawString() {
                        if !message.isEmpty {
                            errorMessage = message
                        }
                    }
                }
                
                print("ERROR IN CONECTION: \(errorMessage)")
                displayMyAlertMessage(userMessage: errorMessage)
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
 
    
}
