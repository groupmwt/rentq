//
//  BookingData.swift
//
//  Created by NC2-46 on 23/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class BookingData: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let cityId = "city_id"
    static let carId = "car_id"
    static let id = "id"
    static let carType = "car_type"
    static let startDate = "start_date"
    static let rideType = "ride_type"
    static let endDate = "end_date"
  }

  // MARK: Properties
  public var cityId: Int?
  public var carId: Int?
  public var id: Int?
  public var carType: String?
  public var startDate: String?
  public var rideType: String?
  public var endDate: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    cityId = json[SerializationKeys.cityId].int
    carId = json[SerializationKeys.carId].int
    id = json[SerializationKeys.id].int
    carType = json[SerializationKeys.carType].string
    startDate = json[SerializationKeys.startDate].string
    rideType = json[SerializationKeys.rideType].string
    endDate = json[SerializationKeys.endDate].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = cityId { dictionary[SerializationKeys.cityId] = value }
    if let value = carId { dictionary[SerializationKeys.carId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = carType { dictionary[SerializationKeys.carType] = value }
    if let value = startDate { dictionary[SerializationKeys.startDate] = value }
    if let value = rideType { dictionary[SerializationKeys.rideType] = value }
    if let value = endDate { dictionary[SerializationKeys.endDate] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.cityId = aDecoder.decodeObject(forKey: SerializationKeys.cityId) as? Int
    self.carId = aDecoder.decodeObject(forKey: SerializationKeys.carId) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? String
    self.startDate = aDecoder.decodeObject(forKey: SerializationKeys.startDate) as? String
    self.rideType = aDecoder.decodeObject(forKey: SerializationKeys.rideType) as? String
    self.endDate = aDecoder.decodeObject(forKey: SerializationKeys.endDate) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(cityId, forKey: SerializationKeys.cityId)
    aCoder.encode(carId, forKey: SerializationKeys.carId)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(startDate, forKey: SerializationKeys.startDate)
    aCoder.encode(rideType, forKey: SerializationKeys.rideType)
    aCoder.encode(endDate, forKey: SerializationKeys.endDate)
  }

}
