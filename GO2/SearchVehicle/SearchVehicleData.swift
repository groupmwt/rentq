//
//  SearchVehicleData.swift
//
//  Created by NC2-46 on 29/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SearchVehicleData: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let cityId = "city_id"
    static let updatedAt = "updated_at"
    static let address = "address"
    static let countryId = "country_id"
    static let end = "end"
    static let model = "model"
    static let lat = "lat"
    static let softDeleted = "soft_deleted"
    static let start = "start"
    static let brand = "brand"
    static let stateId = "state_id"
    static let lng = "lng"
    static let plate = "plate"
    static let carType = "car_type"
    static let id = "id"
    static let image = "image"
    static let createdAt = "created_at"
    static let userId = "user_id"
    static let offers = "offers"
    static let rideType = "ride_type"
  }

  // MARK: Properties
  public var cityId: Int?
  public var updatedAt: String?
  public var address: String?
  public var countryId: Int?
  public var end: String?
  public var model: String?
  public var lat: Double?
  public var softDeleted: Bool? = false
  public var start: String?
  public var brand: String?
  public var stateId: Int?
  public var lng: Double?
  public var plate: String?
  public var carType: String?
  public var id: Int?
  public var image: SearchVehicleImage?
  public var createdAt: String?
  public var userId: Int?
  public var offers: [SearchVehicleOffers]?
  public var rideType: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    cityId = json[SerializationKeys.cityId].int
    updatedAt = json[SerializationKeys.updatedAt].string
    address = json[SerializationKeys.address].string
    countryId = json[SerializationKeys.countryId].int
    end = json[SerializationKeys.end].string
      start = json[SerializationKeys.start].string
    model = json[SerializationKeys.model].string
    lat = json[SerializationKeys.lat].double
    softDeleted = json[SerializationKeys.softDeleted].boolValue
    
    brand = json[SerializationKeys.brand].string
    stateId = json[SerializationKeys.stateId].int
    lng = json[SerializationKeys.lng].double
    plate = json[SerializationKeys.plate].string
    carType = json[SerializationKeys.carType].string
    id = json[SerializationKeys.id].int
    image = SearchVehicleImage(json: json[SerializationKeys.image])
    createdAt = json[SerializationKeys.createdAt].string
    userId = json[SerializationKeys.userId].int
    if let items = json[SerializationKeys.offers].array { offers = items.map { SearchVehicleOffers(json: $0) } }
    rideType = json[SerializationKeys.rideType].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = cityId { dictionary[SerializationKeys.cityId] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = countryId { dictionary[SerializationKeys.countryId] = value }
    if let value = end { dictionary[SerializationKeys.end] = value }
      if let value = start { dictionary[SerializationKeys.start] = value }
    if let value = model { dictionary[SerializationKeys.model] = value }
    if let value = lat { dictionary[SerializationKeys.lat] = value }
    dictionary[SerializationKeys.softDeleted] = softDeleted
    
    if let value = brand { dictionary[SerializationKeys.brand] = value }
    if let value = stateId { dictionary[SerializationKeys.stateId] = value }
    if let value = lng { dictionary[SerializationKeys.lng] = value }
    if let value = plate { dictionary[SerializationKeys.plate] = value }
    if let value = carType { dictionary[SerializationKeys.carType] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = image { dictionary[SerializationKeys.image] = value.dictionaryRepresentation() }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = offers { dictionary[SerializationKeys.offers] = value.map { $0.dictionaryRepresentation() } }
    if let value = rideType { dictionary[SerializationKeys.rideType] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.cityId = aDecoder.decodeObject(forKey: SerializationKeys.cityId) as? Int
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.countryId = aDecoder.decodeObject(forKey: SerializationKeys.countryId) as? Int
    self.end = aDecoder.decodeObject(forKey: SerializationKeys.end) as? String
      self.start = aDecoder.decodeObject(forKey: SerializationKeys.start) as? String
    self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
    self.lat = aDecoder.decodeObject(forKey: SerializationKeys.lat) as? Double
    self.softDeleted = aDecoder.decodeBool(forKey: SerializationKeys.softDeleted)
    
    self.brand = aDecoder.decodeObject(forKey: SerializationKeys.brand) as? String
    self.stateId = aDecoder.decodeObject(forKey: SerializationKeys.stateId) as? Int
    self.lng = aDecoder.decodeObject(forKey: SerializationKeys.lng) as? Double
    self.plate = aDecoder.decodeObject(forKey: SerializationKeys.plate) as? String
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? SearchVehicleImage
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
    self.offers = aDecoder.decodeObject(forKey: SerializationKeys.offers) as? [SearchVehicleOffers]
    self.rideType = aDecoder.decodeObject(forKey: SerializationKeys.rideType) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(cityId, forKey: SerializationKeys.cityId)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(countryId, forKey: SerializationKeys.countryId)
    aCoder.encode(end, forKey: SerializationKeys.end)
      aCoder.encode(start, forKey: SerializationKeys.start)
    aCoder.encode(model, forKey: SerializationKeys.model)
    aCoder.encode(lat, forKey: SerializationKeys.lat)
    aCoder.encode(softDeleted, forKey: SerializationKeys.softDeleted)
    
    aCoder.encode(brand, forKey: SerializationKeys.brand)
    aCoder.encode(stateId, forKey: SerializationKeys.stateId)
    aCoder.encode(lng, forKey: SerializationKeys.lng)
    aCoder.encode(plate, forKey: SerializationKeys.plate)
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(userId, forKey: SerializationKeys.userId)
    aCoder.encode(offers, forKey: SerializationKeys.offers)
    aCoder.encode(rideType, forKey: SerializationKeys.rideType)
  }

}
