//
//  MakePaymentBaseClass.swift
//
//  Created by NC2-46 on 23/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class MakePaymentBaseClass: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let responseCode = "response_code"
    static let descriptionValue = "description"
    static let message = "message"
    static let code = "code"
  }

  // MARK: Properties
  public var responseCode: String?
  public var descriptionValue: String?
  public var message: String?
  public var code: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    responseCode = json[SerializationKeys.responseCode].string
    descriptionValue = json[SerializationKeys.descriptionValue].string
    message = json[SerializationKeys.message].string
    code = json[SerializationKeys.code].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = responseCode { dictionary[SerializationKeys.responseCode] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.responseCode = aDecoder.decodeObject(forKey: SerializationKeys.responseCode) as? String
    self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.code = aDecoder.decodeObject(forKey: SerializationKeys.code) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(responseCode, forKey: SerializationKeys.responseCode)
    aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(code, forKey: SerializationKeys.code)
  }

}
