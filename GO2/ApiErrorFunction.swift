//
//  ApiErrorFunction.swift
//  GO2
//
//  Created by NC2-46 on 17/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

    // ********************************************************
    // MARK: Process API Error Case
    // ********************************************************
public func processApiError(apiReturn:JSON){
    
    var alertTitle:String = ""
    var messageToDisplay:String = ""
    
    print("Alamofire status Error")
    alertTitle = "Error"
    let msjTemp = apiReturn["errors"]
    let msjChat = apiReturn["error_body"]
    
    print("------> msjTemp: \(msjTemp)")
    print("------> msjChat: \(msjChat)")
    
    print(msjTemp)
    var errorTxt = "";
    
    for (index, text) in msjTemp {
        print("\(index): \(text)")
        let msjtext = text.rawString()
        errorTxt = errorTxt + msjtext! + ", "
    }
    
    for (index, text) in msjChat {
        print("\(index): \(text)")
        let msjtext = text.rawString()
        errorTxt = errorTxt + msjtext! + ", "
    }
    
    print("errorTxt: \(errorTxt)")
    
    //var truncated = errorTxt.substringToIndex(errorTxt.endIndex.predecessor())
    var truncated = errorTxt.substring(to: errorTxt.endIndex)
    
    //truncated = truncated.substringToIndex(truncated.endIndex.predecessor())
    truncated = truncated.substring(to: truncated.endIndex)
    
    messageToDisplay = truncated
    
    displayMyAlertMessage(userMessage: messageToDisplay)
}



// ********************************************************
// MARK: Display Alert Message
// ********************************************************
public func displayMyAlertMessage(userMessage:String){
   
    let mess = userMessage == "" ? "Server Connection Failed" : userMessage
    TinyToast.shared.show(message: mess, valign: .bottom, duration: .normal)
    
}


public func updateHeaderData(response : JSON)
{
    let apiReturnHeaders = response
    let userDefaults = UserDefaults.standard
    let token = apiReturnHeaders["access-token"].rawString()
    let client = apiReturnHeaders["client"].rawString()
    let uid = apiReturnHeaders["uid"].rawString()
    
    userDefaults.set(token, forKey: kAccessToken)
    userDefaults.set(client, forKey: kClient)
    userDefaults.set(uid, forKey: kUid)
    userDefaults.synchronize()
}

