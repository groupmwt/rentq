//
//  ProfileEditVC.swift
//  GO2
//
//  Created by NC2-46 on 14/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit
import CountryPickerView

class ProfileEditVC: UIViewController {
    
    //MARK: Data User
    var homeAddressPlaceholder = "Home Address Here"
    var workAddressPlaceholder = "Work Address Here"
    let imagePicker = UIImagePickerController()
    var userData : EditProfileData!
    
    //MARK: Static Labels
    @IBOutlet weak var labelFirstName: UILabel!
    @IBOutlet weak var labelLastName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelMobile: UILabel!
    @IBOutlet weak var labelPlaces: UILabel!
    @IBOutlet weak var txtHomeAddress: UITextView!
    @IBOutlet weak var txtWorkAddress: UITextView!
    
    //MARK: Images
    @IBOutlet weak var avatarImg: UIImageViewX!
    
    //MARK: TextFields
    @IBOutlet weak var textFieldFirstName: UITextFieldX!
    @IBOutlet weak var textFieldLastName: UITextFieldX!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldMobile: UITextField!
    
    //MARK: - Views
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var mobileCodePicker: CountryPickerView!
    @IBOutlet weak var phoneView: UIViewX!
    
    @IBOutlet weak var adressViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnPickCountryCode: UIButton!
    @IBOutlet weak var btnEditSave: UIButtonX!
    @IBOutlet weak var imgPencilMark1: UIImageView!
    @IBOutlet weak var imgPencilMark2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Edit Profile"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func btnCountryCodeAction(_ sender: UIButton) {
        if let nav = self.navigationController {
            self.title = ""
            self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back_icon")
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "back_icon")
            mobileCodePicker.showCountriesList(from: nav)
        }
    }
    
    
    @IBAction func btnEditSave(_ sender: UIButtonX) {
        self.view.endEditing(true)
        guard validateData() else { return }
        
        if txtHomeAddress.text == homeAddressPlaceholder {
            txtHomeAddress.text = ""
        }
        
        if txtWorkAddress.text == workAddressPlaceholder {
            txtWorkAddress.text = ""
        }
 
        let userPhoneCode = mobileCodePicker.selectedCountry.code.replacingOccurrences(of: "+", with: "")
        // MARK: API Request
        UserApi.sharedInstance.updateProfileRequest(myView: self.view, firstName: textFieldFirstName.text!, lastname: textFieldLastName.text!, phonenumber: textFieldMobile.text!, countryCode: userPhoneCode, profileImage: avatarImg.image, homeAddress: txtHomeAddress.text!, officeAddress: txtWorkAddress.text!, completion: { (profileData) in
            displayMyAlertMessage(userMessage: "Profile Updated Successfully")
            self.backTap()
            //TinyToast.shared.dismissAll()
        })
        
    }
    
}
