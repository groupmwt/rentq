//
//  CD_State+CoreDataProperties.swift
//  GO2
//
//  Created by NC2-46 on 17/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//
//

import Foundation
import CoreData


extension CD_State {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CD_State> {
        return NSFetchRequest<CD_State>(entityName: "CD_State")
    }

    @NSManaged public var country_id: Int16
    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var city: NSSet?

}

// MARK: Generated accessors for city
extension CD_State {

    @objc(addCityObject:)
    @NSManaged public func addToCity(_ value: CD_State)

    @objc(removeCityObject:)
    @NSManaged public func removeFromCity(_ value: CD_State)

    @objc(addCity:)
    @NSManaged public func addToCity(_ values: NSSet)

    @objc(removeCity:)
    @NSManaged public func removeFromCity(_ values: NSSet)

}
