//
//  UIViewController+Extension.swift
//  Motiv8
//
//  Created by NC2-46 on 26/03/18.
//  Copyright © 2018 NC2-46. All rights reserved.
//

import Foundation
import UIKit
import MMDrawerController

extension UIViewController{

    // MARK: Display Message Alert
    func displayAlertWithAction(userMessage:String, userTitle:String,alertStyle: UIAlertControllerStyle,arrActionName:[String],arrActionStyle:[UIAlertActionStyle], completion: @escaping (_ actionIndex:Int) ->()) -> Void {
        
        let myAlert = UIAlertController(title: userTitle, message: userMessage, preferredStyle: alertStyle)
        
        
        for index in 0..<arrActionName.count {
            let action = UIAlertAction(title: arrActionName[index], style: arrActionStyle[index]) { (action) in
                completion(index)
            }
            myAlert.addAction(action)
        }
        
        myAlert.popoverPresentationController?.sourceView = self.view
        self.present(myAlert, animated:true, completion:nil)
    }
    
    
    // MARK: - Load Activity
    func loadViewController(StoryBoardName : String , VCIdentifer : String) -> UIViewController
    {
        let vc = UIStoryboard(name: StoryBoardName, bundle: nil).instantiateViewController(withIdentifier: VCIdentifer)
        return vc
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= CGFloat(UserDefaults.standard.float(forKey: "viewHeight"))
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
               self.view.frame.origin.y = 0
            }
        }
    }
    
    //MARK: - AlertView Setting
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Done", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }

    
    func setNavigationTitleImage(imageName: String) {
        // Do any additional setup after loading the view.
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 38, height: 38))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: imageName)
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    func hideMenu()
    {
        let sideMenuVC = loadViewController(StoryBoardName: "Menu", VCIdentifer: "MenuViewController") as! MenuViewController
        self.mm_drawerController.leftDrawerViewController = sideMenuVC
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "menu"), for: .normal)
        
        button.addTarget(self, action: #selector(self.hideDrawer(_:)), for: .touchUpInside)
        
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        let barButtonItem = UIBarButtonItem(customView: button)
        
        navigationItem.leftBarButtonItem = barButtonItem
       
    }
    @objc func hideDrawer(_ sender: NSObject)
    {
        self.view.endEditing(false)
        self.mm_drawerController.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }

    func setBackBtn(isTemplate : Bool = false,navTintColor : UIColor? = nil) {
        let backBtn : UIButton = UIButton(frame : CGRect(x:0,y:0,width: 20, height :44))
        var backImage: UIImage = #imageLiteral(resourceName: "back_icon")
        if isTemplate {
            backImage = backImage.withRenderingMode(.alwaysTemplate)
            backBtn.tintColor = navTintColor
        }
        backBtn.setImage(backImage, for: .normal)
        backBtn.addTarget(self, action: #selector(self.backTap), for: .touchDown)
        let barButton : UIBarButtonItem = UIBarButtonItem(customView : backBtn)
        self.navigationItem.leftBarButtonItem = barButton
        if (self.mm_drawerController) != nil {
            self.mm_drawerController.leftDrawerViewController = nil
        }
        
    }
    
    @objc func backTap(animation:Bool = true){
        popViewController(animation: animation)
    }
    
    func popViewController(animation:Bool = true){
        self.navigationController?.popViewController(animated: animation)
    }
    
   
    
    //MARK:- RedirectToDashboard
    func redirectToDashboard() {
//        CoreDBManager.sharedDatabase.saveVehicleType()
        CoreDBManager.sharedDatabase.saveVehicleCategory()
        let sideMenuVC = loadViewController(StoryBoardName: "Menu", VCIdentifer: "MenuViewController") as! MenuViewController
        
        let centerVC = loadViewController(StoryBoardName: "Map", VCIdentifer: "MapVC") as! MapVC
        
        let leftSideNav = UINavigationController(rootViewController: sideMenuVC)
        let centerNav = UINavigationController(rootViewController: centerVC)
        AppDel.centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav,rightDrawerViewController:nil)
        AppDel.centerContainer?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.all
        AppDel.centerContainer?.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
        AppDel.mainNavigationController?.pushViewController(AppDel.centerContainer!,  animated: false)
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
}



