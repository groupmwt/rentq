//
//  RentCarsHomeViewController.swift
//  GO2
//
//  Created by Juan Cardozo on 6/3/17.
//  Copyright © 2017 GNM. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class MapVC: UIViewController,UISearchDisplayDelegate{
    var subView = UIView()
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    var searchMSG:String = ""
    var locationManager = CLLocationManager()
    var nearByVehicleData = [GetNearByVehicleData]()
    var arrFilterResultData = [SearchVehicleData]()
    var isFilterResult : Bool = false
    var focusOnCorrdinate : CLLocationCoordinate2D!
    var isGetNearApiCalled :Bool = false
    var fetcher: GMSAutocompleteFetcher?
    var arrayPlaceResult = NSMutableArray()
    //MARK: Data User
    let userDefaults = UserDefaults.standard
    
    //MARK: Map
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchBar: UITextField!
    //MARK: Buttons
    @IBOutlet weak var lblBookCar: UILabel!
    @IBOutlet weak var lblMyCar: UILabel!
    //MARK: Views
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    
    @IBOutlet weak var searchBarView: UIViewX!

    
    //var isSearchResult: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let arrCountrys = CoreDBManager.sharedDatabase.getAllCountry()
        
//        if arrCountrys.count == 0 {
//            CoreDBManager.sharedDatabase.saveLocation()
//        }
//        CoreDBManager.sharedDatabase.saveVehicleType()
        if (UserDefaults.standard.bool(forKey: kSetting)) {
            
            UserDefaults.standard.set(false, forKey: kSetting)
            openBookingVC(animated : false)
        }
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideMenu()
//        CoreDBManager.sharedDatabase.saveVehicleType()
//        CoreDBManager.sharedDatabase.saveLocation()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        DispatchQueue.main.async {
            self.searchController?.searchBar.resignFirstResponder()
        }
//        CoreDBManager.sharedDatabase.saveVehicleType()
        TinyToast.shared.dismissAll()
    }
    
    
    @IBAction func btnBooking(_ sender: Any) {
        openBookingVC()
    }
    
    @IBAction func btnMyCar(_ sender: Any) {
        let centerViewController = loadViewController(StoryBoardName: "VehicleList", VCIdentifer: "MyCarsListVC") as! MyCarsListVC
         centerViewController.navigationTitle = "My Vehicles"
        self.navigationController?.pushViewController(centerViewController, animated: true)
    }
    
    
    func openBookingVC(animated:Bool = true) {
        TinyToast.shared.dismissAll()
        let centerViewController = loadViewController(StoryBoardName: "BookVehicles", VCIdentifer: "BookVehiclesHomeVC") as! BookVehiclesHomeVC
        self.navigationController?.pushViewController(centerViewController, animated: animated)
    }
    
}

