//
//  ChangePasswordData.swift
//
//  Created by NC2-46 on 28/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ChangePasswordData: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let email = "email"
    static let phoneNumber = "phone_number"
    static let uid = "uid"
    static let lastName = "last_name"
    static let id = "id"
    static let image = "image"
    static let firstName = "first_name"
    static let createdAt = "created_at"
    static let deviceToken = "device_token"
    static let provider = "provider"
    static let allowPasswordChange = "allow_password_change"
  }

  // MARK: Properties
  public var updatedAt: String?
  public var email: String?
  public var phoneNumber: String?
  public var uid: String?
  public var lastName: String?
  public var id: Int?
  public var image: ChangePasswordImage?
  public var firstName: String?
  public var createdAt: String?
  public var deviceToken: String?
  public var provider: String?
  public var allowPasswordChange: Bool? = false

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    updatedAt = json[SerializationKeys.updatedAt].string
    email = json[SerializationKeys.email].string
    phoneNumber = json[SerializationKeys.phoneNumber].string
    uid = json[SerializationKeys.uid].string
    lastName = json[SerializationKeys.lastName].string
    id = json[SerializationKeys.id].int
    image = ChangePasswordImage(json: json[SerializationKeys.image])
    firstName = json[SerializationKeys.firstName].string
    createdAt = json[SerializationKeys.createdAt].string
    deviceToken = json[SerializationKeys.deviceToken].string
    provider = json[SerializationKeys.provider].string
    allowPasswordChange = json[SerializationKeys.allowPasswordChange].boolValue
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = phoneNumber { dictionary[SerializationKeys.phoneNumber] = value }
    if let value = uid { dictionary[SerializationKeys.uid] = value }
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = image { dictionary[SerializationKeys.image] = value.dictionaryRepresentation() }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = deviceToken { dictionary[SerializationKeys.deviceToken] = value }
    if let value = provider { dictionary[SerializationKeys.provider] = value }
    dictionary[SerializationKeys.allowPasswordChange] = allowPasswordChange
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.phoneNumber = aDecoder.decodeObject(forKey: SerializationKeys.phoneNumber) as? String
    self.uid = aDecoder.decodeObject(forKey: SerializationKeys.uid) as? String
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? ChangePasswordImage
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.deviceToken = aDecoder.decodeObject(forKey: SerializationKeys.deviceToken) as? String
    self.provider = aDecoder.decodeObject(forKey: SerializationKeys.provider) as? String
    self.allowPasswordChange = aDecoder.decodeBool(forKey: SerializationKeys.allowPasswordChange)
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(phoneNumber, forKey: SerializationKeys.phoneNumber)
    aCoder.encode(uid, forKey: SerializationKeys.uid)
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(deviceToken, forKey: SerializationKeys.deviceToken)
    aCoder.encode(provider, forKey: SerializationKeys.provider)
    aCoder.encode(allowPasswordChange, forKey: SerializationKeys.allowPasswordChange)
  }

}
