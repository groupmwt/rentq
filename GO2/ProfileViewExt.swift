//
//  ProfileViewExt.swift
//  GO2
//
//  Created by NC2-46 on 22/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import CountryPickerView
    
extension ProfileViewController {
    func setupUI() {
        hideMenu()
        self.title = "My Profile"
        
        DispatchQueue.main.async {
            //Style
            self.navigationController?.customNavBar()
            self.globalView.setBackgroundWithTopBarToView()
            
            self.dataView.backgroundColor = Color.DataViewBG
            self.globalView.backgroundColor = Color.DataViewBG
            self.dividerView.backgroundColor = Color.DividerGreen
            self.labelFirstName.setProfileNameGrey()
            self.labelLastName.setProfileNameGrey()
            self.labelEmail.setProfileNameGrey()
            self.labelMobile.setProfileNameGrey()
            self.labelPlaces.setProfileNameGrey()
            self.textFieldFirstName.addShadow()
            self.textFieldLastName.addShadow()
            self.textFieldEmail.addShadow()
            self.phoneView.addShadow()
            self.mobileCodePicker.setCountryByCode("DO")
            self.avatarImg.layer.cornerRadius = self.avatarImg.frame.width/2.0
            self.btnEditSave.backgroundColor = Color.buttonBG
            self.btnChangePassword.backgroundColor = Color.buttonBG
        }
        
    }
    
    func loadUserValues(){
        self.txtHomeAddress.removePlaceHolder()
        self.txtWorkAddress.removePlaceHolder()
        
        UserApi.sharedInstance.getUserDetailsRequest(myView: self.view) { (data) in

            let finalUrl : String = getConexionAPI_Production() + (data.image?.url ?? "" )
            let url = URL.init(string: finalUrl)
            self.avatarImg.sd_setShowActivityIndicatorView(true)
            self.avatarImg.sd_setIndicatorStyle(.gray)
            self.avatarImg.sd_setImage(with: url , placeholderImage: nil)
            self.textFieldFirstName.text = data.firstName
            self.textFieldLastName.text = data.lastName
            self.textFieldEmail.text = data.email
            self.textFieldMobile.text = data.phoneNumber
            self.txtHomeAddress.text = data.homeAddress
            self.txtWorkAddress.text = data.officeAddress
//            self.mobileCodePicker.setCountryByPhoneCode("+\(data.countryCode ?? "1")")
            self.mobileCodePicker.setCountryByCode(data.countryCode ?? "")
            self.userData = data
            if self.txtHomeAddress.isEmpty {
                self.txtHomeAddress.addPlaceHolder(self.homeAddressPlaceholder)
            }
            if self.txtWorkAddress.isEmpty {
                self.txtWorkAddress.addPlaceHolder(self.workAddressPlaceholder)
            }
            self.view.stopIndicator(indicator: indicator, container: container)
        }
    }
}


