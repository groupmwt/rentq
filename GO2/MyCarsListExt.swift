//
//  MyCarsListExt.swift
//  GO2
//
//  Created by NC2-46 on 13/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
extension MyCarsListVC{
    
    
    func setupUI() {
        if fromMenu {
            hideMenu()
        }
        else {
            setBackBtn()
        }
        //Style
        navigationController?.customNavBar()
        //navigationItem.customTitle()
        self.title = navigationTitle       
        
        dataTableView.delegate = self
        dataTableView.dataSource = self
        
       loadDataFromApi(myView: self.view)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.loadDataFromApi), for: UIControlEvents.valueChanged)
        dataTableView.addSubview(refreshControl)
        
    }
    
    @objc func loadDataFromApi(myView : UIView = UIView()) {
        MyVehiclesApi.sharedInstance.getMyVehiclesListRequest(myView: myView) { (myVehicleList) in
            self.arrMyVehicleList = myVehicleList
            self.arrMyVehicleList.reverse()
            self.dataTableView.reloadData()
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            
        }
    }
}

extension MyCarsListVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if arrMyVehicleList.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "No vehicle available.\nTap on + icon for add new vehicle."
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            noDataLabel.numberOfLines = 5
            noDataLabel.lineBreakMode = .byWordWrapping
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMyVehicleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MyCarCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MyCarTableViewCell
        if arrMyVehicleList[indexPath.row].images?.count ?? 0 > 0{
            cell.configureCell(vehicleImageURL: arrMyVehicleList[indexPath.row].images?[0].url,
                vehicleBrand: arrMyVehicleList[indexPath.row].brand ?? "",
                vehiclePlate: arrMyVehicleList[indexPath.row].plate ?? "",
                vehicleModel: arrMyVehicleList[indexPath.row].model ?? "",
                vehicleAddress: arrMyVehicleList[indexPath.row].address ?? "", tagvalue: indexPath.row)
        }else{
            cell.configureCell(vehicleImageURL: "",
                vehicleBrand: arrMyVehicleList[indexPath.row].brand ?? "",
                vehiclePlate: arrMyVehicleList[indexPath.row].plate ?? "",
                vehicleModel: arrMyVehicleList[indexPath.row].model ?? "",
                vehicleAddress: arrMyVehicleList[indexPath.row].address ?? "", tagvalue: indexPath.row)
        }
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let centerViewController = loadViewController(StoryBoardName: "CarDetails", VCIdentifer: "CarDetailsVC") as! CarDetailsVC
        centerViewController.headerTitle = "Vehicle Profile"
        centerViewController.navigationTitle = "Vehicle Profile"
        centerViewController.isMyVehicleDetails = true
        centerViewController.myVehicleData = arrMyVehicleList[indexPath.row]
        self.navigationController?.pushViewController(centerViewController, animated: true)
    }
    
}
