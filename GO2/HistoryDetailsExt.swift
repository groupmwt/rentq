//
//  HistoryDetailsExt.swift
//  GO2
//
//  Created by NC2-46 on 27/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation

extension HistoryDetailsVC
{
    func setupUI()
    {
        
        labelBarTitle.text = headerTitle
        //Style
        navigationController?.customNavBar()
       // navigationItem.customTitle()
        self.title = navigationTitle
        setBackBtn()
        
        globalView.setBackgroundWithTopBarToView()
        headerBar.backgroundColor = Color.SubHeaderColor
        labelBarTitle.setTextHeader()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true
    
        tableView.tableFooterView = UIView()
        tblVIewHeight.constant = CGFloat(((HistoryDetailsEnum.totalValues - 1) * 60) + 100)
        let finalUrl = getConexionAPI_Production() + (historyDetails.image?.url ?? "")!
        let url = URL.init(string: finalUrl)
        self.imgProfile.sd_setShowActivityIndicatorView(true)
        self.imgProfile.sd_setIndicatorStyle(.gray)
        self.imgProfile.sd_setImage(with: url , placeholderImage: nil)
        self.imgProfile.contentMode = .scaleAspectFit
        self.imgProfile.clipsToBounds = true
        
        if isViewPresent {
            btnCloseHeight.constant = 60
        }
        else {
             btnCloseHeight.constant = 0
        }
        BookingCarApi.sharedInstance.getLocationRequest(myView: self.view) { (location) in
            print(location)
            
            if location.countries != nil{
                for country in location.countries! {
                    if country.id! == self.historyDetails.countryId! {
                        self.countryData = country.name!
                    }
                }
            }
            if location.states != nil{
                for state in location.states! {
                    if state.id! == self.historyDetails.stateId! {
                        self.stateData = state.name!
                    }
                }
            }
            if location.cities != nil{
                for city in location.cities! {
                    if city.id! == self.historyDetails.cityId! {
                        self.cityData = city.name!
                    }
                }
            }
            self.tableView.reloadData()
        }
        
    }
}


//MARK: - TableView Data Source and delegate Method
extension HistoryDetailsVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HistoryDetailsEnum.totalValues
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailsTVC") as! CarDetailsTVC
        cell.isUserInteractionEnabled = false
        switch indexPath.row {
        case HistoryDetailsEnum.StartDate.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "rental_time"),rowTitle: "Start Date :", rowData: historyDetails.startDate ?? "", tagvalue: indexPath.row, numLines: 1)
            
            return cell
        case HistoryDetailsEnum.EndDate.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "rental_time"),rowTitle: "End Date :", rowData: historyDetails.endDate ?? "", tagvalue: indexPath.row, numLines: 1)
            
            return cell
        
        case HistoryDetailsEnum.Model.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "model"), rowTitle: "Model :", rowData: historyDetails.model ?? "", tagvalue: indexPath.row, numLines: 1)
            return cell
        case HistoryDetailsEnum.Brand.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "brand"),rowTitle: "Brand :", rowData: historyDetails.brand ?? "", tagvalue: indexPath.row, numLines: 1)
            
            return cell
        case HistoryDetailsEnum.Plate.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "plate"), rowTitle: "Plate :", rowData: historyDetails.plate ?? "" ,tagvalue: indexPath.row, numLines: 1)
            
            return cell
        case HistoryDetailsEnum.VehicleType.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "vehicleType"),rowTitle: "Vehicle Type", rowData: historyDetails.carType ?? "", tagvalue: indexPath.row, numLines: 1)
            
            return cell
        case HistoryDetailsEnum.RideType.rawValue:
            let ride_type = (historyDetails.rideType ?? "").replacingOccurrences(of: "_", with: "")
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "rideType"),rowTitle: "Ride Type", rowData: ride_type, tagvalue: indexPath.row, numLines: 1)
            
            return cell
        case HistoryDetailsEnum.Country.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "country"), rowTitle: "Country", rowData: countryData ?? "", tagvalue: indexPath.row, numLines: 1)
            
            return cell
        case HistoryDetailsEnum.State.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "State"), rowTitle: "State", rowData: stateData ?? "", tagvalue: indexPath.row, numLines: 1)
            
            return cell
        case HistoryDetailsEnum.City.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "city"), rowTitle: "City", rowData: cityData ?? "", tagvalue: indexPath.row, numLines: 1)
            
            return cell
        case HistoryDetailsEnum.Name.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "usernameGray"), rowTitle: "Name", rowData: historyDetails.name ?? "", tagvalue: indexPath.row, numLines: 1)
            
            return cell
        case HistoryDetailsEnum.Contact.rawValue:
            let fullContact = "+\(historyDetails.countryCode ?? "1") " + (historyDetails.phoneNumber ?? "")
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "phoneGray"), rowTitle: "Contact", rowData: fullContact, tagvalue: indexPath.row, numLines: 1)
            cell.isUserInteractionEnabled = true
            return cell
        case HistoryDetailsEnum.Address.rawValue:
            cell.configureCell(imgLogo: #imageLiteral(resourceName: "address"), rowTitle: "Address", rowData: historyDetails.address ?? "", tagvalue: indexPath.row, numLines: 5)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         switch indexPath.row {
         case HistoryDetailsEnum.Contact.rawValue:
            var fullContact = "+\(historyDetails.countryCode ?? "1") " + (historyDetails.phoneNumber ?? "")
            fullContact = fullContact.replacingOccurrences(of: " ", with: "")
            if let url = URL(string: "tel://\(fullContact)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            break
         default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case HistoryDetailsEnum.Address.rawValue:
            return 80
        default:
            return 60
        }
    }
    
    
   
}
