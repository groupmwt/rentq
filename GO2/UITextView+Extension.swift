//
//  UITextView+Extension.swift
//  GO2
//
//  Created by NC2-46 on 28/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation

extension UITextView {
    /// SwifterSwift: Check if text field is empty.
    public var isEmpty: Bool {
        if self.text != nil {
            let text =  self.text!.trimmingCharacters(in: NSCharacterSet.whitespaces)
            if text.count < 1 {
                return true
            }
        }
        else {
            return true
        }
        
        return text?.isEmpty == true
    }

    func addGradient(){
        
        let colorTop = UIColor(red:0.78, green:0.77, blue:0.74, alpha:1.0).cgColor
        let colorBottom = UIColor(red:0.98, green:0.99, blue:0.98, alpha:1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        
        let new_width = self.bounds.size.width
        let new_height = self.bounds.size.height
        
        if Go2Helpers.isLessThanIPhone6(){
            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 14)!
            self.font = MVBoliFont
            self.textColor = Color.TextGrey
        }
        else if Go2Helpers.isPlus(){
            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 16)!
            self.font = MVBoliFont
            self.textColor = Color.TextGrey
        }
        else{
            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 15)!
            self.font = MVBoliFont
            self.textColor = Color.TextGrey
        }
        
        gradientLayer.frame = self.bounds
        gradientLayer.frame = CGRect(x:0, y:0, width: new_width*100, height: new_height)
        gradientLayer.cornerRadius = 4.0
        
        self.layer.addSublayer(gradientLayer)
        self.layer.cornerRadius = 4.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.insertSublayer(gradientLayer, at: 0)
        
    }
    
    func setTextGrey(){
        
        var fontSize = 0
        if Go2Helpers.isLessThanIPhone6(){
            fontSize = 12
        }
        else if Go2Helpers.isPlus(){
            fontSize = 14
        }
        else{
            fontSize = 13
        }
        
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = MVBoliFont
        self.textColor = Color.TextGrey
        
    }
    
    func addShadow(){
        
        if Go2Helpers.isLessThanIPhone6(){
            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 14)!
            self.font = MVBoliFont
            self.textColor = Color.TextGrey
        }
        else if Go2Helpers.isPlus(){
            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 16)!
            self.font = MVBoliFont
            self.textColor = Color.TextGrey
        }
        else{
            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 15)!
            self.font = MVBoliFont
            self.textColor = Color.TextGrey
        }
    }
    
    func addPlaceHolder(_ placeholderText : String) {
        let lable = UILabel(frame: CGRect(x: self.frame.minX, y: self.frame.minY, width: self.frame.width, height: self.frame.height))
        lable.text = placeholderText
        lable.textColor = .gray
        lable.tag = 555
        lable.textAlignment = .left
        lable.lineBreakMode = .byWordWrapping
        lable.numberOfLines = 5
        self.addSubview(lable)
        self.clipsToBounds = true
    }
    
    func removePlaceHolder() {
        for view in self.subviews{
            if view.tag == 555 {
                view.removeFromSuperview()
            }
            
        }
    }

}

