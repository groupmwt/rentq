//
//  Extensions.swift
//  GO2
//
//  Created by GNM on 5/22/17.
//  Copyright © 2017 Juan Cardozo. All rights reserved.
//

import Foundation
import UIKit
//import CountryPickerView

extension UIView{
    
    func setBackgroundToView() {
        
        var new_width = UIScreen.main.bounds.size.width
        var new_height = self.bounds.size.height
        
        if Go2Helpers.isLessThanIPhone6(){
            new_height = 513
            new_width = 320
        }
        else if Go2Helpers.isPlus(){
            new_height = 681
            new_width = 415
        }
        else{
            new_height = 612
            new_width = 375
        }
        
        let imageViewBackground = UIImageView(frame: CGRect(x:0, y:0, width: new_width, height: new_height))
//        imageViewBackground.image = UIImage(named: "bg")
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIViewContentMode.scaleToFill
        
        self.addSubview(imageViewBackground)
        self.sendSubview(toBack: imageViewBackground)
        
    }
    
    func setBackgroundWithTopBarToView() {
        
        var new_width = UIScreen.main.bounds.size.width
        var new_height = self.bounds.size.height
        
        if Go2Helpers.isLessThanIPhone6(){
            new_height = 469
            new_width = 320
        }
        else if Go2Helpers.isPlus(){
            new_height = 637
            new_width = 415
        }
        else{
            new_height = 568
            new_width = 375
        }
        
        let imageViewBackground = UIImageView(frame: CGRect(x:0, y:0, width: new_width, height: new_height))
        self.backgroundColor = Color.DataViewBG
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIViewContentMode.scaleToFill
        
        self.addSubview(imageViewBackground)
        self.sendSubview(toBack: imageViewBackground)
        
    }
    
    
    
    func setSubViewPopUp(){
        
        self.layer.cornerRadius = 6.0;
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 0.4;
        
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 6.0
        self.layer.shadowOpacity = 0.6
        
        var new_width = CGFloat(0)
        
        if Go2Helpers.isLessThanIPhone6(){
            new_width = 320-30
        }
        else if Go2Helpers.isPlus(){
            new_width = 415-30
        }
        else{
            new_width = 375-30
        }
        
        let newLayer = CAGradientLayer()
        newLayer.colors = [Color.GradientPopUpStart.cgColor, Color.GradientPopUpEnd.cgColor]
        newLayer.frame = CGRect(x: 0, y: 0, width: new_width, height: 150)
        newLayer.cornerRadius = 6.0
        
        self.layer.addSublayer(newLayer)
        self.layer.insertSublayer(newLayer, at: 0)
    }
    
    
    func displayIndicator(indicator:UIActivityIndicatorView, container:UIView, loadingView: UIView){
        
        //MARK: - Conteiner View
        container.frame = self.frame
        container.center = self.center
        container.backgroundColor = UIColor(red:0.69, green:0.68, blue:0.67, alpha:1.0).withAlphaComponent(0.4)
        
        //MARK: - Loading View
        loadingView.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 80.0)
        loadingView.center = self.center
        loadingView.backgroundColor = UIColor.clear
        
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 15
        
        //MARK: - Activity Indicator
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        indicator.color = Color.FooterColor
        indicator.frame = CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0)
        indicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        
        //MARK: - Show Indicator
        loadingView.addSubview(indicator)
        container.addSubview(loadingView)
        self.addSubview(container)
        container.isHidden = false
        indicator.startAnimating()
        
    }
    
    func stopIndicator(indicator:UIActivityIndicatorView, container:UIView){
        
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
        container.isHidden = true
        container.removeFromSuperview()
        
    }
    
}

extension UINavigationController {

    func customNavBar() {
        
        self.navigationBar.backgroundColor = Color.GradientMenuStart
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.barTintColor = Color.GradientMenuStart
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func customNavBarTitle(new_title:String) {
        
        self.navigationBar.backgroundColor = Color.GradientMenuStart
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.barTintColor = Color.GradientMenuStart
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.navigationBar.topItem?.title = new_title
        
    }
}

extension UINavigationItem {
    
    func customTitle() {
        
        let image = UIImage(named: "h_logo")
        
        self.titleView = UIImageView(image: image)
        self.titleView?.frame = CGRect(x:0, y:0, width:0, height:22)
        self.titleView?.contentMode = .scaleAspectFit
        
    }

}


extension UITextField{
    
    func addGradient(){
        
        let colorTop = UIColor(red:0.78, green:0.77, blue:0.74, alpha:1.0).cgColor
        let colorBottom = UIColor(red:0.98, green:0.99, blue:0.98, alpha:1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        
        let new_width = self.bounds.size.width
        let new_height = self.bounds.size.height
        
        if Go2Helpers.isLessThanIPhone6(){
            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 14)!
            self.font = MVBoliFont
            self.textColor = Color.TextGrey
        }
        else if Go2Helpers.isPlus(){
            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 16)!
            self.font = MVBoliFont
            self.textColor = Color.TextGrey
        }
        else{
            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 15)!
            self.font = MVBoliFont
            self.textColor = Color.TextGrey
        }
        
        gradientLayer.frame = self.bounds
        gradientLayer.frame = CGRect(x:0, y:0, width: new_width*100, height: new_height)
        gradientLayer.cornerRadius = 4.0
        
        self.layer.addSublayer(gradientLayer)
        self.layer.cornerRadius = 4.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.insertSublayer(gradientLayer, at: 1)
    }
    
    func addShadow(){
        
//        if Go2Helpers.isLessThanIPhone6(){
//            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 14)!
//            self.font = MVBoliFont
//            self.textColor = Color.TextGrey
//        }
//        else if Go2Helpers.isPlus(){
//            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 16)!
//            self.font = MVBoliFont
//            self.textColor = Color.TextGrey
//        }
//        else{
//            let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 15)!
//            self.font = MVBoliFont
//            self.textColor = Color.TextGrey
//        }
//
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: 16)!
        self.font = MVBoliFont
        //self.textColor = Color.TextGrey
        self.layer.cornerRadius = 4.0
        self.layer.borderWidth = 0.8
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 0.6
  
    }
    
}



extension UILabel{
    
    func setTextClear(){
        
        var fontSize = 0
        if Go2Helpers.isLessThanIPhone6(){
            fontSize = 14
        }
        else if Go2Helpers.isPlus(){
            fontSize = 16
        }
        else{
            fontSize = 15
        }
        
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = MVBoliFont
        self.textColor = Color.TextClear
        
    }
    
    func setTextGrey(){
        
        var fontSize = 0
        if Go2Helpers.isLessThanIPhone6(){
            fontSize = 16
        }
        else if Go2Helpers.isPlus(){
            fontSize = 18
        }
        else{
            fontSize = 17
        }
        
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = MVBoliFont
        self.textColor = Color.TextGrey
        
    }
    
    func setTextGreyBold(){
        
        var fontSize = 0
        if Go2Helpers.isLessThanIPhone6(){
            fontSize = 14
        }
        else if Go2Helpers.isPlus(){
            fontSize = 16
        }
        else{
            fontSize = 15
        }
        
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = MVBoliFont
        self.textColor = Color.TextGrey
        
    }
    
    func setTextWhite(){
        
        var fontSize = 0
        if Go2Helpers.isLessThanIPhone6(){
            fontSize = 14
        }
        else if Go2Helpers.isPlus(){
            fontSize = 16
        }
        else{
            fontSize = 15
        }
        
        let RQFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = RQFont
        self.textColor = UIColor.white
        
    }
    
    func setTitleGrey(){
        
        var fontSize = 0
        if Go2Helpers.isLessThanIPhone6(){
            fontSize = 16
        }
        else if Go2Helpers.isPlus(){
            fontSize = 18
        }
        else{
            fontSize = 17
        }
        
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = MVBoliFont
        self.textColor = Color.TextGrey
        
    }
    
    func setProfileNameGrey(){
        
        let fontSize = 17
//        if Go2Helpers.isLessThanIPhone6(){
//            fontSize = 15
//        }
//        else if Go2Helpers.isPlus(){
//            fontSize = 17
//        }
//        else{
//            fontSize = 16
//        }
        
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = MVBoliFont
        self.textColor = Color.TextGrey
        
    }
    
    func setTextMiniGrey(){
        
        var fontSize = 0
        if Go2Helpers.isLessThanIPhone6(){
            fontSize = 10
        }
        else if Go2Helpers.isPlus(){
            fontSize = 12
        }
        else{
            fontSize = 11
        }
        
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = MVBoliFont
        self.textColor = Color.TextGrey
        
    }
    
    
    func setTextFormGrey(){
        
        var fontSize = 0
        if Go2Helpers.isLessThanIPhone6(){
            fontSize = 15
        }
        else if Go2Helpers.isPlus(){
            fontSize = 17
        }
        else{
            fontSize = 16
        }
        
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = MVBoliFont
        self.textColor = Color.TextGrey
        
    }
    
    func setTextHeader(){
        
        var fontSize = 0
        if Go2Helpers.isLessThanIPhone6(){
            fontSize = 18
        }
        else if Go2Helpers.isPlus(){
            fontSize = 20
        }
        else{
            fontSize = 19
        }
        
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = MVBoliFont
        self.textColor = Color.TextGrey
        
    }
    
    func setTextPopUp(){
        
        var fontSize = 0
        if Go2Helpers.isLessThanIPhone6(){
            fontSize = 10
        }
        else if Go2Helpers.isPlus(){
            fontSize = 12
        }
        else{
            fontSize = 11
        }
        
        let MVBoliFont : UIFont = UIFont(name: "Calibri", size: CGFloat(fontSize))!
        self.font = MVBoliFont
        self.textColor = Color.TextGrey
        
    }

    
}


extension UIViewX {
    
    func addShadow(){
        
        self.layer.cornerRadius = 4.0
        self.layer.borderWidth = 0.8
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 0.6
        
    }
 
 
}


