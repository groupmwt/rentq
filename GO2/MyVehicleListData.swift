//
//  MyVehicleListData.swift
//
//  Created by NC2-46 on 20/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class MyVehicleListData: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let cityId = "city_id"
    static let address = "address"
    static let countryId = "country_id"
    static let model = "model"
    static let lat = "lat"
    static let carType = "car_type"
    static let brand = "brand"
    static let stateId = "state_id"
    static let plate = "plate"
    static let lng = "lng"
    static let id = "id"
    static let image = "image"
    static let rideType = "ride_type"
  }

  // MARK: Properties
  public var cityId: Int?
  public var address: String?
  public var countryId: Int?
  public var model: String?
  public var lat: String?
  public var carType: String?
  public var brand: String?
  public var stateId: Int?
  public var plate: String?
  public var lng: String?
  public var id: Int?
  public var image: MyVehicleListImage?
  public var rideType: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    cityId = json[SerializationKeys.cityId].int
    address = json[SerializationKeys.address].string
    countryId = json[SerializationKeys.countryId].int
    model = json[SerializationKeys.model].string
    lat = json[SerializationKeys.lat].string
    carType = json[SerializationKeys.carType].string
    brand = json[SerializationKeys.brand].string
    stateId = json[SerializationKeys.stateId].int
    plate = json[SerializationKeys.plate].string
    lng = json[SerializationKeys.lng].string
    id = json[SerializationKeys.id].int
    image = MyVehicleListImage(json: json[SerializationKeys.image])
    rideType = json[SerializationKeys.rideType].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = cityId { dictionary[SerializationKeys.cityId] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = countryId { dictionary[SerializationKeys.countryId] = value }
    if let value = model { dictionary[SerializationKeys.model] = value }
    if let value = lat { dictionary[SerializationKeys.lat] = value }
    if let value = carType { dictionary[SerializationKeys.carType] = value }
    if let value = brand { dictionary[SerializationKeys.brand] = value }
    if let value = stateId { dictionary[SerializationKeys.stateId] = value }
    if let value = plate { dictionary[SerializationKeys.plate] = value }
    if let value = lng { dictionary[SerializationKeys.lng] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = image { dictionary[SerializationKeys.image] = value.dictionaryRepresentation() }
    if let value = rideType { dictionary[SerializationKeys.rideType] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.cityId = aDecoder.decodeObject(forKey: SerializationKeys.cityId) as? Int
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.countryId = aDecoder.decodeObject(forKey: SerializationKeys.countryId) as? Int
    self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
    self.lat = aDecoder.decodeObject(forKey: SerializationKeys.lat) as? String
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? String
    self.brand = aDecoder.decodeObject(forKey: SerializationKeys.brand) as? String
    self.stateId = aDecoder.decodeObject(forKey: SerializationKeys.stateId) as? Int
    self.plate = aDecoder.decodeObject(forKey: SerializationKeys.plate) as? String
    self.lng = aDecoder.decodeObject(forKey: SerializationKeys.lng) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? MyVehicleListImage
    self.rideType = aDecoder.decodeObject(forKey: SerializationKeys.rideType) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(cityId, forKey: SerializationKeys.cityId)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(countryId, forKey: SerializationKeys.countryId)
    aCoder.encode(model, forKey: SerializationKeys.model)
    aCoder.encode(lat, forKey: SerializationKeys.lat)
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(brand, forKey: SerializationKeys.brand)
    aCoder.encode(stateId, forKey: SerializationKeys.stateId)
    aCoder.encode(plate, forKey: SerializationKeys.plate)
    aCoder.encode(lng, forKey: SerializationKeys.lng)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(rideType, forKey: SerializationKeys.rideType)
  }

}
