//
//  CD_Country+CoreDataProperties.swift
//  GO2
//
//  Created by NC2-46 on 17/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//
//

import Foundation
import CoreData


extension CD_Country {
    
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<CD_Country> {
        return NSFetchRequest<CD_Country>(entityName: "CD_Country")
    }

    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var state: NSSet?
   
//    convenience init?(id : Int, name : String) {
//        self.init()
//        self.id = Int16(id)
//        self.name = name
//    }
}

// MARK: Generated accessors for state
extension CD_Country {

    @objc(addStateObject:)
    @NSManaged public func addToState(_ value: CD_Country)

    @objc(removeStateObject:)
    @NSManaged public func removeFromState(_ value: CD_Country)

    @objc(addState:)
    @NSManaged public func addToState(_ values: NSSet)

    @objc(removeState:)
    @NSManaged public func removeFromState(_ values: NSSet)

    
    
}
