//
//  UserApi.swift
//  GO2
//
//  Created by GNM on 5/23/17.
//  Copyright © 2017 Juan Cardozo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UserApi {
    
    // MARK: - Singelton
    static let sharedInstance:UserApi = UserApi()
    
    private init() {}
    
    
    private var pathUrl = getConexionAPI_Production() + general.versionPath
    
    // MARK: - ApiMethods

    // ********************************************************
    // MARK: Login API Request
    // ********************************************************
    func loginRequest(myView: UIView, email:String, password:String, completion: @escaping (_ user:User?) ->()) -> Void {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let parameters : [String : AnyObject] = [
            "email": email as AnyObject,
            "password": password as AnyObject,
            "device_token": getDeviceToken() as AnyObject,
            "platform": "ios" as AnyObject
        ]
        
        let urlString = getPathUrl() + APIS.loginPath
        
        print("URL: \(urlString)")
        print("Parameter: \(parameters)")
        
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            response in
            
            switch response.result {
                
            case .success:
                
                var userAux:User?
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    let apiReturn = JSON(value)
                    //let apiReturnHeaders = JSON((response.response?.allHeaderFields)!)
                    myView.stopIndicator(indicator: indicator, container: container)
                    
                    if (apiReturn.null != nil) {
                        processApiError(apiReturn: apiReturn)
                    }
                    else {
                        let userDefaults = UserDefaults.standard
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        userAux = User(email: email, apiKey: "1234")
                        userDefaults.set(true, forKey: kIsUserLoggedIn)
                    }
                    completion(userAux)
                }
                break
                
            case .failure(let error):
                print("ERROR IN CONECTION: \(error)")
                displayMyAlertMessage(userMessage: "Email address or Password is incorrect")
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
    
    // ********************************************************
    // MARK: Forgot Password API Request
    // ********************************************************
    func forgotPasswordRequest(myView: UIView, email:String,completion: @escaping (_ message:String?) ->()) -> Void {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let parameters : [String : AnyObject] = [
            "email": email as AnyObject
        ]
        
        let urlString = getPathUrl() + APIS.forgotPasswordPath
        
        print("URL: \(urlString)")
        print("Parameter: \(parameters)")
        
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{
            response in
            
            switch response.result {
                
            case .success:
                
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    var mess :String? = nil
                    let apiReturn = JSON(value)
                    _ = JSON((response.response?.allHeaderFields)!)
                    myView.stopIndicator(indicator: indicator, container: container)
                    
                    if (apiReturn.null != nil) {
                        processApiError(apiReturn: apiReturn)
                    }
                    else {
                        if value["status"] as? Bool == true {
                            mess = "A password has been sent to provided email address"
                            completion(mess)
                        }
                        
                    }
                    
                }
                break
                
            case .failure(let _):
               var errorMessage = ""
                if let data = response.data {
                    let responseJSON = JSON(data)
                    
                    if let message: String = responseJSON["errors"].array?[0].rawString() {
                        if !message.isEmpty {
                            errorMessage = message
                        }
                    }
                }
                
                print("ERROR IN CONECTION: \(errorMessage)")
                displayMyAlertMessage(userMessage: errorMessage)
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }

    
    
    // ********************************************************
    // MARK: Change Password API Request
    // ********************************************************
    func changePasswordRequest(myView: UIView, newPassword:String,confirmPassword:String, completion: @escaping (ChangePasswordBaseClass) -> Void) {
            
            myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
            
            let Auth_header = [ "access-token": getAccessToken(),
                                "client": getClientToken(),
                                "uid": getUID() ]
            
            print("parameters: \(Auth_header)")
            
            let urlString = getPathUrl() + APIS.changePasswordPath
            var changePasswordResult : ChangePasswordBaseClass!
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                multipartFormData.append(newPassword.data(using: .utf8)!, withName: "password")
                multipartFormData.append(confirmPassword.data(using: .utf8)!, withName: "password_confirmation")
                
                
            }, usingThreshold: UInt64.init(), to: urlString, method: .patch, headers: Auth_header) { encodingResult in
                switch encodingResult {
                case .success(let upload, _,_ ):
                    upload.responseJSON { response in
                        if let value: AnyObject = response.result.value as AnyObject? {
                            if value["status"] as? Bool == true {
                                updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                                let result = value as! NSDictionary
                                
                                let apiReturn = JSON(result)
                                if let object = apiReturn.to(type: ChangePasswordBaseClass.self){
                                    changePasswordResult = object as? ChangePasswordBaseClass
                                }
                                print(changePasswordResult)
                                myView.stopIndicator(indicator: indicator, container: container)
                                completion(changePasswordResult)
                            }
                            else {
                                if let errors = value["errors"] as? NSArray {
                                    var mess = ""
                                    for err in errors {
                                        mess += (err as! String) + "\n"
                                    }
                                    displayMyAlertMessage(userMessage: mess)
                                }
                                myView.stopIndicator(indicator: indicator, container: container)
                            }
                        }
                        else {
       
                            displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
 
                            myView.stopIndicator(indicator: indicator, container: container)
                                
                        }
                    }
                    break
                case .failure(let encodingError):
                    
                    displayMyAlertMessage(userMessage: "Server Connection Failed")
                    myView.stopIndicator(indicator: indicator, container: container)
                    break
                    
                }
            }
        }

    
    
    // ********************************************************
    // MARK: Register API Request
    // ********************************************************
    func registerRequest(myView: UIView,firstName:String, lastname:String, email:String,phonenumber:String,countryCode:String, password:String, completion: @escaping (_ user:User?) ->()) -> Void {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)

        let urlString = getPathUrl() + APIS.registerPath
        
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append((firstName.data(using: .utf8))!, withName: "user[first_name]")
                multipartFormData.append(lastname.data(using: .utf8)!, withName: "user[last_name]")
                multipartFormData.append(email.data(using: .utf8)!, withName: "user[email]")
                
                multipartFormData.append(phonenumber.data(using: .utf8)!, withName: "user[phone_number]")
                
                multipartFormData.append(countryCode.data(using: .utf8)!, withName: "user[country_code]")
                
                multipartFormData.append(password.data(using: .utf8)!, withName: "user[password]")
                
                multipartFormData.append(getDeviceToken().data(using: .utf8)!, withName: "user[device_token]")
                multipartFormData.append("ios".data(using: .utf8)!, withName: "user[platform]")
                
        },
            to: urlString ,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _,_ ):
                    upload.responseJSON { response in
                        var userAux:User?
        
                        if let value: AnyObject = response.result.value as AnyObject? {
        
                            let apiReturn = JSON(value)
                            
                            myView.stopIndicator(indicator: indicator, container: container)
        
                            if apiReturn.null != nil{
                                processApiError(apiReturn: apiReturn)
                            }
                            else {

                                if let error = value["errors"] as? NSDictionary {
                                    print("ERROR IN CONECTION: \(error)")
                                    let mess = apiReturn["errors"]["full_messages"].array![0].rawString()!
                                    displayMyAlertMessage(userMessage: mess)
                                    myView.stopIndicator(indicator: indicator, container: container)
                                }
                                else {
                                    let userDefaults = UserDefaults.standard
                                    updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                                    userAux = User(email: email, apiKey: "1234")
                                    userDefaults.set(true, forKey: kIsUserLoggedIn)
                                }
                            }
                            completion(userAux)
                        }
                    }
                    break
                case .failure(let encodingError):
                        print("ERROR IN CONECTION: \(encodingError)")
                        displayMyAlertMessage(userMessage: "Server Connection Failed")
                        myView.stopIndicator(indicator: indicator, container: container)
                        break
                    
                }
                
        })
    }
    
    
    // ********************************************************
    // MARK: Update Profile API Request
    // ********************************************************
    func updateProfileRequest(myView: UIView,firstName:String, lastname:String,phonenumber:String,countryCode:String, profileImage: UIImage?,homeAddress:String,officeAddress:String, completion: @escaping (EditProfileData) -> Void) {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let urlString = getPathUrl() + APIS.getUpdateProfilePath
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
        print("urlString",urlString)
        print("Auth_headr",Auth_header)
        var updateProfileData : EditProfileData!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append((firstName.data(using: .utf8))!, withName: "first_name")
                multipartFormData.append(lastname.data(using: .utf8)!, withName: "last_name")
                //multipartFormData.append(email.data(using: .utf8)!, withName: "email")
                
                multipartFormData.append(phonenumber.data(using: .utf8)!, withName: "phone_number")
                
                multipartFormData.append(countryCode.data(using: .utf8)!, withName: "country_code")
                
                 multipartFormData.append(homeAddress.data(using: .utf8)!, withName: "home_address")
                
                multipartFormData.append(officeAddress.data(using: .utf8)!, withName: "office_address")
                
                if profileImage != nil {
                    
                    let imgData = profileImage!.base64(format: .JPEG(0.5))
                    let finalImagePath = "data:image/png;base64," + imgData!
                    multipartFormData.append(finalImagePath.data(using: .utf8)!, withName: "image")
                    
                }
        }, usingThreshold: UInt64.init(), to: urlString, method: .patch, headers: Auth_header) { encodingResult in
                switch encodingResult {
                case .success(let upload, _,_ ):
                    upload.responseJSON { response in
                        if let value: AnyObject = response.result.value as AnyObject? {
                            
                            if let status = value["status"] as? Bool  {
                                if status == false {
                                    if let errors = value["errors"] as? NSArray {
                                        var mess = ""
                                        for err in errors {
                                            mess += (err as! String) + "\n"
                                        }
                                        displayMyAlertMessage(userMessage: mess)
                                    }
                                    
                                    
                                    myView.stopIndicator(indicator: indicator, container: container)
                                    //completion(true)
                                }
                                else {
                                    myView.stopIndicator(indicator: indicator, container: container)
                                    updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                                    
                                    let result = value["data"] as! NSDictionary
                                    
                                    let apiReturn = JSON(result)
                                    if let object = apiReturn.to(type: EditProfileData.self){
                                        updateProfileData = object as! EditProfileData
                                    }
                                    // print(carType)
                                    myView.stopIndicator(indicator: indicator, container: container)
                                    completion(updateProfileData)
                                   // completion(false)
                                }
                            }
                            else {
                                displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
                                myView.stopIndicator(indicator: indicator, container: container)
                                //completion(true)
                            }
                        }
                        else {
                            //print("ERROR IN CONECTION: \(EncodingError)")
                            displayMyAlertMessage(userMessage: "Server Connection Failed")
                            myView.stopIndicator(indicator: indicator, container: container)
                            //completion(true)
                            
                        }
                    }
                    break
                case .failure(let encodingError):
                    print("ERROR IN CONECTION: \(encodingError)")
                    displayMyAlertMessage(userMessage: "Server Connection Failed")
                    myView.stopIndicator(indicator: indicator, container: container)
                    break
                    
                }
                
        }
    }
    
    
    // ********************************************************
    // MARK: Logout API Request
    // ********************************************************

    func logoutRequest(myView: UIView, completion: @escaping (_ error:Bool) ->()) -> Void {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
       
        let urlString = getPathUrl() + APIS.logoutPath
        
        let Auth_header = [ "access-token": getAccessToken(),
                            "client": getClientToken(),
                            "uid": getUID() ]
        
        print("URL: \(urlString)")

        Alamofire.request(urlString, method: .delete, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON{
            response in
                
            switch response.result {
                
            case .success:

                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    let apiReturn = JSON(value)
                    let apiReturnHeaders = JSON((response.response?.allHeaderFields)!)
                    myView.stopIndicator(indicator: indicator, container: container)
                    
                    if (apiReturn.null != nil) {
                        processApiError(apiReturn: apiReturn)
                        completion(true)
                    }
                    
//                    if apiReturn["errors"] != nil{
//                        processApiError(apiReturn: apiReturn)
//                        completion(true)
//                    }
                    else {
                        
                       completion(false)
                    }
                }
                break
                
            case .failure(let error):
                print("ERROR IN CONECTION: \(error)")
                displayMyAlertMessage(userMessage: "User was not found or was not logged in.")
                
                myView.stopIndicator(indicator: indicator, container: container)
 
                        
                
                completion(true)
                break
            }
        }
    }
    
    // ********************************************************
    // MARK: get_user_details API Request
    // ********************************************************
    func getUserDetailsRequest(myView: UIView,completion: @escaping (EditProfileData) -> Void) {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
        
        let urlString = getPathUrl() + APIS.getUserDetailsPath
        
        print("URL: \(urlString)")
        print("Parameter: \(Auth_header)")
        
        Alamofire.request(urlString, method: .get, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON { (response) in
            var userData : EditProfileData!
            var errorMessage = ""
            switch response.result {
                
            case .success:
                print("getUserDetailsRequest --- API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                    else {
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        let result = value["data"] as! NSDictionary
                        
                        let apiReturn = JSON(result)
                        if let object = apiReturn.to(type: EditProfileData.self){
                            userData = object as? EditProfileData
                        }
//                        // print(carType)
//                        myView.stopIndicator(indicator: indicator, container: container)
                        completion(userData)
                    }
                }
                break
                
            case .failure( _):
                
                if let data = response.data {
                    let responseJSON = JSON(data)
                    
                    if let message: String = responseJSON["errors"].array?[0].rawString() {
                        if !message.isEmpty {
                            errorMessage = message
                        }
                    }
                }
                
                print("ERROR IN CONECTION: \(errorMessage)")
                displayMyAlertMessage(userMessage: errorMessage)
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }

}
