//
//  MyVehiclesApi.swift
//  GO2
//
//  Created by NC2-46 on 20/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import UIKit

class MyVehiclesApi {
    
    // MARK: - Singelton
    static let sharedInstance:MyVehiclesApi = MyVehiclesApi()
    
    private init() {}
    
    //private let pathUrl = getConexionAPI_Production() + general.versionPath
    
    // MARK: - ApiMethods
    
    // ********************************************************
    // MARK: Get Vehicle Details API Request
    // ********************************************************
    func getVehicleDetailsRequest(myView: UIView, carID:String, completion: @escaping ([VehicleDetailsData]) -> Void)  {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header = [ "access-token": getAccessToken(),
                            "client": getClientToken(),
                            "uid": getUID() ]
        
        let urlString = getPathUrl() + APIS.getAllMyCars + "/\(carID).json"
        
        Alamofire.request(urlString, method: .get, headers: Auth_header).validate().responseJSON{
            response in
            var searchResult = [VehicleDetailsData]()
            switch response.result {
            case .success:
                print("getVehicleDetailsRequest - API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                    else {
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        //TKP
                        if let result = value["data"] as? NSArray{
                            let apiReturn = JSON(result)
                            //                            let apiReturn = JSON(value)
                            if let object = apiReturn.to(type: VehicleDetailsData.self){
                                searchResult = object as! [VehicleDetailsData]
                            }
                        }
                        print(searchResult)
                        myView.stopIndicator(indicator: indicator, container: container)
                        completion(searchResult)
                    }
                }
                break
                
            case .failure(let error):
                print("ERROR IN CONECTION: \(error)")
                displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
    
    func getVehicleListOfOwnerRequest(myView: UIView, carID:String, completion: @escaping ([OwnerVehicleListData]) -> Void)  {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header = [ "access-token": getAccessToken(),
                            "client": getClientToken(),
                            "uid": getUID() ]
        let urlString = getPathUrl() + APIS.getAllMyCars + APIS.carList + "?id=\(carID)"
        
        Alamofire.request(urlString, method: .get, headers: Auth_header).validate().responseJSON{
            response in
            var searchResult = [OwnerVehicleListData]()
            switch response.result {
            case .success:
                print("getVehicleListOfOwnerRequest - API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                    else {
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        //TKP
                        if let result = value["vehicles"] as? NSArray{
                            let apiReturn = JSON(result)
                            //                            let apiReturn = JSON(value)
                            if let object = apiReturn.to(type: OwnerVehicleListData.self){
                                searchResult = object as! [OwnerVehicleListData]
                            }
                        }
                        print(searchResult)
                        myView.stopIndicator(indicator: indicator, container: container)
                        completion(searchResult)
                    }
                }
                break
                
            case .failure(let error):
                print("ERROR IN CONECTION: \(error)")
                displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
    
    // ********************************************************
    // MARK: Create New Vehicle API Request
    // ********************************************************
    
    //    func addEditVehicleRequest(myview: UIView, carPlate:String, vehicleTypeId:String, carCityId:String, carBrand:String, carModel:String, carPrice:String, carInsuranceId:String, insurance_company:String, insurance_expiry_date:String, chassis_number:String, registration_number:String, number_of_seats:String, type_of_traction:String, carCity:String, carState:String, carCountry:String, vehicle_part_statuses_attributes[0][vehicle_part_id]:String, vehicle_part_statuses_attributes[0][status]:String, vehicle_part_statuses_attributes[1][vehicle_part_id]:String, vehicle_part_statuses_attributes[1][status]:String, images_attributes[0][image]: UIImage?, completion: @escaping (_ error: Bool) ->()) -> Void {
    
    
    func addEditVehicleRequest(myView: UIView, carID: String?, carPlate:String, vehicleTypeId:String, carCityId:String, carStateId:String, carCountryID:String, carBrand:String, carModel:String, carPrice:String, carInsuranceId:String, insurance_company:String, insurance_expiry_date:String, chassis_number:String, registration_number:String, number_of_seats:String, type_of_traction:String, carCity:String, carState:String, carCountry:String, carLat:String, carLng:String, carRideType:String, carAddress:String, vehicleImages: [VehicleDetailsImage], addVehicleImages: [UIImage], deletedImagesIds:[Int], deletedPartIds:[Int], partStatuses:[Int],vehicleParts:[VehicleDetailsPartStatuses], arrUpdatedParts:[VehicleDetailsPartStatuses], arrUpdatedPartStatuses:[Int], isPartsAvailable:Bool, completion: @escaping (_ error: Bool) ->()) -> Void {
        
        //    func addEditVehicleRequest(myView: UIView,carID: String?,carBrand:String, carPlate:String, carModel:String,carType:String,carRideType:String, carAddress:String,carLat:String,carLng:String,carCity:String,carState:String,carCountry:String,carCityId:String,carPrice:String,carImage: UIImage?, completion: @escaping (_ error: Bool) ->()) -> Void {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        var urlString = getPathUrl() + APIS.addNewCar
        
        if carID != nil {
            urlString = urlString.appending("/\(carID!).json")
        }
        
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
        print("urlString",urlString)
        print("Auth_headr",Auth_header)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(carPlate.data(using: .utf8)!, withName: "plate")
            multipartFormData.append(vehicleTypeId.data(using: .utf8)!, withName: "vehicle_type_id")
            
            multipartFormData.append((carBrand.data(using: .utf8))!, withName: "brand")
            multipartFormData.append(carModel.data(using: .utf8)!, withName: "model")
            multipartFormData.append(carCityId.data(using: .utf8)!, withName: "city_id")
            if carID == nil {
                multipartFormData.append(carPrice.data(using: .utf8)!, withName: "price")
            }
            if carID != nil{
                multipartFormData.append(carStateId.data(using: .utf8)!, withName: "state_id")
                multipartFormData.append(carCountryID.data(using: .utf8)!, withName: "country_id")
            }
            multipartFormData.append(carInsuranceId.data(using: .utf8)!, withName: "insurance_id")
            multipartFormData.append(insurance_company.data(using: .utf8)!, withName: "insurance_company")
            multipartFormData.append(insurance_expiry_date.data(using: .utf8)!, withName: "insurance_expiry_date")
            multipartFormData.append(chassis_number.data(using: .utf8)!, withName: "chassis_number")
            multipartFormData.append(registration_number.data(using: .utf8)!, withName: "registration_number")
            multipartFormData.append(number_of_seats.data(using: .utf8)!, withName: "number_of_seats")
            multipartFormData.append(type_of_traction.data(using: .utf8)!, withName: "type_of_traction")
            multipartFormData.append(carCity.data(using: .utf8)!, withName: "city")
            multipartFormData.append(carState.data(using: .utf8)!, withName: "state")
            multipartFormData.append(carCountry.data(using: .utf8)!, withName: "country")
            multipartFormData.append(carLat.data(using: .utf8)!, withName: "lat")
            multipartFormData.append(carLng.data(using: .utf8)!, withName: "lng")
            multipartFormData.append(carAddress.data(using: .utf8)!, withName: "address")
            
            if(carID != nil){
                var index = 0
                if deletedImagesIds.count > 0 {
                    for deletedImageId in deletedImagesIds {
                        
                        multipartFormData.append("\(deletedImageId)".data(using: .utf8)!, withName: "images_attributes[\(index)][id]")
                        multipartFormData.append("1".data(using: .utf8)!, withName: "images_attributes[\(index)][_destroy]")
                        index = index + 1
                    }
                }
                if vehicleImages.count > 0 {
                    for vehicleImage in vehicleImages {
                        if vehicleImage.id == 0{
                            if vehicleImage.url != ""{
                                let finalUrl = getConexionAPI_Production() + vehicleImage.url!
                                let url = URL(string: finalUrl)
                                let data = try? Data(contentsOf: url!)
                                let imgData = UIImage(data: data!)!.base64(format: .JPEG(0.5))
                                let finalImagePath = "data:image/png;base64," + imgData!
                                multipartFormData.append(finalImagePath.data(using: .utf8)!, withName: "images_attributes[\(index)][image]")
                            }else{
                                if vehicleImage.image != nil{
                                    let imgData = vehicleImage.image!.base64(format: .JPEG(0.5))
                                    let finalImagePath = "data:image/png;base64," + imgData!
                                    multipartFormData.append(finalImagePath.data(using: .utf8)!, withName: "images_attributes[\(index)][image]")
                                }
                            }
                            index = index + 1
                        }
                    }
                }
                
                var index1 = 0
                if deletedPartIds.count > 0 {
                    for deletedPartId in deletedPartIds {
                        multipartFormData.append("\(deletedPartId)".data(using: .utf8)!, withName: "vehicle_part_statuses_attributes[\(index1)][id]")
                        multipartFormData.append("1".data(using: .utf8)!, withName: "vehicle_part_statuses_attributes[\(index1)][_destroy]")
                        index1 = index1 + 1
                    }
                    if(partStatuses.count > 0){
                        
                        var indexOfVehiclePart = 0
                        for partStatus in partStatuses {
                            multipartFormData.append("\(partStatus)".data(using: .utf8)!, withName: "vehicle_part_statuses_attributes[\(index1)][status]")
                            multipartFormData.append("\(vehicleParts[indexOfVehiclePart].id ?? 0)".data(using: .utf8)!, withName: "vehicle_part_statuses_attributes[\(index1)][id]")
                            index1 = index1 + 1
                            indexOfVehiclePart = indexOfVehiclePart + 1
                        }
                    }
                }else{
                    var index1 = 0
                    if(partStatuses.count > 0){
                        var indexOfVehiclePart = 0
                        for partStatus in partStatuses {
                            multipartFormData.append("\(partStatus)".data(using: .utf8)!, withName: "vehicle_part_statuses_attributes[\(index1)][status]")
                            if isPartsAvailable{
                                multipartFormData.append("\(vehicleParts[indexOfVehiclePart].id ?? 0)".data(using: .utf8)!, withName: "vehicle_part_statuses_attributes[\(index1)][id]")
                            }else{
                                multipartFormData.append("\(vehicleParts[indexOfVehiclePart].id ?? 0)".data(using: .utf8)!, withName: "vehicle_part_statuses_attributes[\(index1)][vehicle_part_id]")
                            }
                            index1 = index1 + 1
                            indexOfVehiclePart = indexOfVehiclePart + 1
                        }
                    }
                }
            }else{
                if addVehicleImages.count > 0 {
                    var index = 0
                    for addVehicleImage in addVehicleImages {
                        let imgData = addVehicleImage.base64(format: .JPEG(0.5))
                        let finalImagePath = "data:image/png;base64," + imgData!
                        multipartFormData.append(finalImagePath.data(using: .utf8)!, withName: "images_attributes[\(index)][image]")
                        index = index + 1
                    }
                }
                
                if(partStatuses.count > 0){
                    var index = 0
                    for partStatus in partStatuses {
                        multipartFormData.append("\(partStatus)".data(using: .utf8)!, withName: "vehicle_part_statuses_attributes[\(index)][status]")
                        index = index + 1
                    }
                }
                
                if(vehicleParts.count > 0){
                    var index = 0
                    for vehiclePart in vehicleParts {
                        multipartFormData.append("\(vehiclePart.id ?? 0)".data(using: .utf8)!, withName: "vehicle_part_statuses_attributes[\(index)][vehicle_part_id]")
                        index = index + 1
                    }
                }
            }
            
        }, usingThreshold: UInt64.init(), to: urlString, method: carID == nil ? .post : .put, headers: Auth_header) { encodingResult in
            switch encodingResult {
            case .success(let upload, _,_ ):
                upload.responseJSON { response in
                    if let value: AnyObject = response.result.value as AnyObject? {
                        
                        //TKP
                        if let status = value["status"] as? Bool  {
                            if status == false {
                                if let errors = value["errors"] as? NSArray {
                                    var mess = ""
                                    for err in errors {
                                        mess += (err as! String) + "\n"
                                    }
                                    displayMyAlertMessage(userMessage: mess)
                                }
                                
                                
                                myView.stopIndicator(indicator: indicator, container: container)
                                completion(true)
                            }else if status == true{
                                if let data = value["data"] as? NSDictionary {
                                    var isAddLocation = false
                                    var dict: NSMutableDictionary = NSMutableDictionary()
                                    let dataOfLocation = CoreDBManager.sharedDatabase.getDataFromFile()
                                    dict = dataOfLocation.mutableCopy() as! NSMutableDictionary
                                    if let countryId = data.value(forKey: "country_id"){
                                        if (AppDel.arrCountrys.filter({$0.id == countryId as? Int}).first == nil){
                                            let countryObj:[String:Any] = [
                                                "id" : countryId,
                                                "name" : data.value(forKey: "country") ?? "",
                                                "short_name" : "",
                                                "phone_code" : 0
                                            ]
                                            if let arrCountriesFromJson = dataOfLocation.value(forKey: "countries") as? NSArray
                                            {
                                                let arrCountries = NSMutableArray(array: arrCountriesFromJson)
//                                                    arrStatesFromJson.adding(stateObj)//append(countryObj)
                                                arrCountries.add(countryObj)
                                                dict["countries"] = arrCountries as NSArray
                                                isAddLocation = true
                                            }
                                        }
                                    }
                                        if let stateId = data.value(forKey: "state_id"){
                                            if (AppDel.arrStates.filter({$0.id == stateId as? Int}).first == nil){
                                                let stateObj:[String:Any] = [
                                                    "id" : stateId,
                                                    "name" : data.value(forKey: "state") ?? "",
                                                    "country_id" : data.value(forKey: "country_id") ?? 0
                                                ]
                                                if let arrStatesFromJson = dataOfLocation.value(forKey: "states") as? NSArray
                                                {
                                                    let arrStates = NSMutableArray(array: arrStatesFromJson)
//                                                    arrStatesFromJson.adding(stateObj)//append(countryObj)
                                                   arrStates.add(stateObj)
                                                    dict["states"] = arrStates as NSArray
                                                    isAddLocation = true
                                                }                                            }
                                        }
                                        
                                    if let cityId = data.value(forKey: "city_id"){
                                        if (AppDel.arrCities.filter({$0.id == cityId as? Int}).first == nil){
                                            let cityObj:[String:Any] = [
                                                "id" : cityId,
                                                "name" : data.value(forKey: "city") ?? "",
                                                "stateId" : data.value(forKey: "state_id") ?? 0
                                            ]
                                            if let arrCitiesFromJson = dataOfLocation.value(forKey: "cities") as? NSArray
                                            {
                                                let arrCities = NSMutableArray(array: arrCitiesFromJson)
                                                arrCities.add(cityObj)//append(countryObj)
                                                dict["cities"] = arrCities as NSArray
                                                isAddLocation = true
                                            }                                          }
                                    }
                                    if isAddLocation{
                                        CoreDBManager.sharedDatabase.updateFile(locationData: NSDictionary(dictionary: dict))
                                        let data = CoreDBManager.sharedDatabase.getDataFromFile()
                                        AppDel.locationDict = data
                                        AppDel.arrCountrys = JSON(data.value(forKey: "countries") as? NSArray ?? NSArray()).to(type: CountriesModel.self) as! [CountriesModel]
                                        AppDel.arrStates = JSON(data.value(forKey: "states") as? NSArray ?? NSArray()).to(type: StatesModel.self) as! [StatesModel]
                                        AppDel.arrCities = JSON(data.value(forKey: "cities") as? NSArray ?? NSArray()).to(type: CitiesModel.self) as! [CitiesModel]
                                    }
                                }
                                myView.stopIndicator(indicator: indicator, container: container)
                                completion(false)
                            }
                        }
                        else {
                            displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
                            myView.stopIndicator(indicator: indicator, container: container)
                            completion(true)
                        }
                    }
                    else {
                        //print("ERROR IN CONECTION: \(EncodingError)")
                        displayMyAlertMessage(userMessage: "Server Connection Failed")
                        myView.stopIndicator(indicator: indicator, container: container)
                        completion(true)
                        
                    }
                }
                break
            case .failure(let encodingError):
                print("ERROR IN CONECTION: \(encodingError)")
                displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                completion(true)
                break
                
            }
        }
        
    }
    
    // ********************************************************
    // MARK: Delete Vehicle API Request
    // ********************************************************
    func deleteVehicleRequest(myView: UIView,carID: String?, completion: @escaping (_ error: Bool) ->()) -> Void {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let urlString = getPathUrl() + APIS.addNewCar + "/\(carID!).json"
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
        print("urlString",urlString)
        print("Auth_headr",Auth_header)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
        }, usingThreshold: UInt64.init(), to: urlString, method: .delete, headers: Auth_header) { encodingResult in
            switch encodingResult {
            case .success(let upload, _,_ ):
                upload.responseJSON { response in
                    if let value: AnyObject = response.result.value as AnyObject? {
                        
                        if let status = value["status"] as? Bool  {
                            if status == false {
                                if let errors = value["errors"] as? NSArray {
                                    var mess = ""
                                    for err in errors {
                                        mess += (err as! String) + "\n"
                                    }
                                    displayMyAlertMessage(userMessage: mess)
                                }
                                
                                
                                myView.stopIndicator(indicator: indicator, container: container)
                                completion(true)
                            }
                            else {
                                updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                                myView.stopIndicator(indicator: indicator, container: container)
                                completion(false)
                            }
                        }
                        else {
                            displayMyAlertMessage(userMessage: ("You need to again sign in or sign up before continuing."))
                            myView.stopIndicator(indicator: indicator, container: container)
                            completion(true)
                        }
                    }
                    else {
                        //print("ERROR IN CONECTION: \(EncodingError)")
                        displayMyAlertMessage(userMessage: "Server Connection Failed")
                        myView.stopIndicator(indicator: indicator, container: container)
                        completion(true)
                        
                    }
                }
                break
            case .failure(let encodingError):
                print("ERROR IN CONECTION: \(encodingError)")
                displayMyAlertMessage(userMessage: "Server Connection Failed")
                myView.stopIndicator(indicator: indicator, container: container)
                completion(true)
                break
                
            }
        }
        
    }
    
    // MARK: - ApiMethods
    
    // ********************************************************
    // MARK: get My Vehicle List API Request
    // ********************************************************
    func getMyVehiclesListRequest(myView: UIView, completion: @escaping ([VehicleDetailsData]) -> Void) {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
        
        let encodedURL = getPathUrl() + APIS.getAllMyCars
        
        print("URL: \(encodedURL)")
        print("Parameter: \(Auth_header)")
        
        Alamofire.request(encodedURL, method: .get, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON { (response) in
            var myVehicleListData = [VehicleDetailsData]()
            var errorMessage = ""
            switch response.result {
                
            case .success:
                print("getMyVehiclesListRequest --- API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                    else{
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        //TKP
                        if let result = value["data"] as? NSArray{
                            let apiReturn = JSON(result)
                            //                            let apiReturn = JSON(value)
                            if let object = apiReturn.to(type: VehicleDetailsData.self){
                                myVehicleListData = object as! [VehicleDetailsData]
                            }
                            // print(carType)
                            myView.stopIndicator(indicator: indicator, container: container)
                            completion(myVehicleListData)
                        }
                    }
                }
                break
                
            case .failure( _):
                
                if let data = response.data {
                    let responseJSON = JSON(data)
                    
                    if let message: String = responseJSON["errors"].array?[0].rawString() {
                        if !message.isEmpty {
                            errorMessage = message
                        }
                    }
                }
                
                print("ERROR IN CONECTION: \(errorMessage)")
                displayMyAlertMessage(userMessage: errorMessage)
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
}

