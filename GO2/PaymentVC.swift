//
//  PaymentVC.swift
//  GO2
//
//  Created by NC2-46 on 19/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit
import AuthorizeNetAccept

class PaymentVC: UIViewController {

    //MARK: Views
    @IBOutlet weak var globalView: UIView!
    
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtMonth: UITextField!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var txtCVV: UITextField!
    
    @IBOutlet var btnPayment: UIButtonX!
    var cityName : String!
    var purchaseAmount : String!
    var searchVehicleData : SearchVehicleData!
    var myVehicleData : VehicleDetailsData!
    var isFromNearbyVehicle : Bool = false
    var headerTitle: String = "Pay Now"
    let handler = AcceptSDKHandler(environment: AcceptSDKEnvironment.ENV_TEST)
    
    let request = AcceptSDKRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    
    @IBAction func btnPayNow(_ sender: UIButtonX) {
        guard validateData() else { return }
        let cardNumber = txtCardNumber.text!.replacingOccurrences(of: " ", with: "")
        
        genrateToken(cardNumber: cardNumber, expMonth: txtMonth.text!, expYear: txtYear.text!, cvvCode: txtCVV.text!)
    }
    
}


extension PaymentVC {
    func setupUI() {

        //Style
        navigationController?.customNavBar()
        //navigationItem.customTitle()
        self.title = "Pay Now"
        setBackBtn()
        
        globalView.setBackgroundWithTopBarToView()
        btnPayment.backgroundColor = Color.buttonBG
        txtCardNumber.delegate = self
        txtMonth.delegate = self
        txtYear.delegate = self
        txtCVV.delegate = self
        
        txtCardNumber.addLineToView(position: .LINE_POSITION_BOTTOM, color: .darkGray)
        txtMonth.addLineToView(position: .LINE_POSITION_BOTTOM, color: .darkGray)
        txtYear.addLineToView(position: .LINE_POSITION_BOTTOM, color: .darkGray)
        txtCVV.addLineToView(position: .LINE_POSITION_BOTTOM, color: .darkGray)
        txtCardNumber.becomeFirstResponder()
    }
}

extension PaymentVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField.tag {
        case 0:
            let maxLength = 19
            if (textField.text?.count == 4) || (textField.text?.count == 9) || (textField.text?.count ==  14){
                if string != "" {
                    textField.text? = textField.text! + " "
                }
            }
            else {
                if string == "" {
                    let currentString: NSString = textField.text! as NSString
                    let newString: NSString =
                        currentString.replacingCharacters(in: range, with: string) as NSString
                    
                    if newString.hasSuffix(" "){
                        textField.text?.removeLast()
                    }
                }
            }
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= maxLength// replace 30 for your max length value
            
        case 1,2:
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case 3:
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        
        default:
            return true
        }
        
    }
}


//Mark: - Extension For Validation
extension PaymentVC {
    //Mark: - Validate Function for checking fileds proper data.
    func validateData() -> Bool {
        self.view.endEditing(true)
        guard !(txtCardNumber.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter card number.")
            return false
        }
        guard !((txtCardNumber.text?.count)! < 19) else{
            displayMyAlertMessage(userMessage: "Please enter valid card number.")
            return false
        }
        guard !(txtMonth.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter expiry month.")
            return false
        }
        guard !((txtMonth.text?.count)! < 2) else{
            displayMyAlertMessage(userMessage: "Please enter valid expiry month.")
            return false
        }
        guard !(txtYear.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter expiry year.")
            return false
        }
        guard !((txtYear.text?.count)! < 2) else{
            displayMyAlertMessage(userMessage: "Please enter valid expiry year.")
            return false
        }
        guard !(txtCVV.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter cvv number.")
            return false
        }
        guard !((txtCVV.text?.count)! < 3) else{
            displayMyAlertMessage(userMessage: "Please Enter Valid CVV Number.")
            return false
        }
        
        return true
    }
}


extension PaymentVC {
    
    func genrateToken(cardNumber: String, expMonth: String,expYear: String,cvvCode: String){
       self.view.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        request.merchantAuthentication.name = PaymentDetails.API_LOGIN_ID
        request.merchantAuthentication.clientKey = PaymentDetails.CLIENT_KEY
//        let deviceID = UIDevice.current.identifierForVendor!.uuidString
//        request.merchantAuthentication.mobileDeviceId = deviceID
        request.securePaymentContainerRequest.webCheckOutDataType.token.cardNumber = cardNumber
        request.securePaymentContainerRequest.webCheckOutDataType.token.expirationMonth = expMonth
        request.securePaymentContainerRequest.webCheckOutDataType.token.expirationYear = expYear
        request.securePaymentContainerRequest.webCheckOutDataType.token.cardCode = cvvCode
        
        
        handler!.getTokenWithRequest(request, successHandler: { (inResponse:AcceptSDKTokenResponse) -> () in
            DispatchQueue.main.async(execute: {
                BookingCarApi.sharedInstance.acceptPaymentsRequest(myView: self.view, amount: self.purchaseAmount, data: inResponse.getOpaqueData().getDataValue(), completion: {
                        (transaction) in
                            
                        var carID = ""
                    
                    if self.isFromNearbyVehicle{
                        for offer in self.myVehicleData.offers! {
                                if offer.carId != nil {
                                    carID = "\(offer.carId!)"
                                    break
                                }
                            }
                    }else{
                        for offer in self.searchVehicleData.offers! {
                                if offer.carId != nil {
                                    carID = "\(offer.carId!)"
                                    break
                                }
                            }
                    }
                    
                    
                    
                        BookingCarApi.sharedInstance.reserveCarRequest(myView: self.view,
                                                                       startDate: self.isFromNearbyVehicle ? self.myVehicleData.start! : self.searchVehicleData.start!,
                                                                       endDate: self.isFromNearbyVehicle ? self.myVehicleData.end! : self.searchVehicleData.end!,
                            cityName: self.cityName,
                                                                       carType: self.isFromNearbyVehicle ? self.myVehicleData.vehicleType! : self.searchVehicleData.carType!,
                            carID: carID, completion: {
                            (bookingData) in
                                
                                self.displayAlertWithAction(userMessage: transaction.message!, userTitle: transaction.descriptionValue!, alertStyle: .alert, arrActionName: ["Done"], arrActionStyle: [.default], completion: { (btnIndex) in
                                    
                                    switch btnIndex {
                                    case 0:
                                        let centerViewController = self.loadViewController(StoryBoardName: "History", VCIdentifer: "HistoryVC") as! HistoryVC
                                            let centerNavController = UINavigationController(rootViewController: centerViewController)
                                        self.mm_drawerController.centerViewController = centerNavController
                                        break
                                    default :
                                        break
                                    }
                                })
                                                            
                            self.view.stopIndicator(indicator: indicator, container: container)
                        })
                    })
            })
        }){
            (inError:AcceptSDKErrorResponse) -> () in

            displayMyAlertMessage(userMessage: inError.getMessages().getMessages()[0].getText())
            self.view.stopIndicator(indicator: indicator, container: container)
        }
    }
    
    
    
}

