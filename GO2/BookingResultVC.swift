//
//  BookingResultVC.swift
//  GO2
//
//  Created by NC2-46 on 17/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class BookingResultVC: UIViewController,UISearchDisplayDelegate {
    
//    var locationManager = CLLocationManager()
    var arrSearchResultData = [SearchVehicleData]()
    var focusOnCorrdinate : CLLocationCoordinate2D!
    
    //MARK: Data User
    let userDefaults = UserDefaults.standard
    
    //MARK: Map
    @IBOutlet weak var mapView: GMSMapView!

    //MARK: Views
    @IBOutlet weak var globalView: UIView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
