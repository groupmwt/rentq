//
//  LoginController.swift
//  GO2
//
//  Created by Juan Cardozo on 5/22/17.
//  Copyright © 2017 GNM. All rights reserved.
//

import Foundation

class LoginController: UIViewController {
    
    //MARK: Textfields
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    //MARK: Buttons
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    
    //MARK: Views
    @IBOutlet weak var globalView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    //MARK: Login Action
    @IBAction func loginAction(_ sender: UIButton) {
        self.view.endEditing(true)
        guard validateData() else { return }
        
        //Get Textfield Data
        let userEmail = emailTextField.text!
        let userPassword = passwordTextField.text!
        UserApi.sharedInstance.loginRequest(myView: self.view, email: userEmail, password: userPassword) { (user) in
            
            if user != nil {
                self.redirectToDashboard()
            }
            else {
                displayMyAlertMessage(userMessage: "Email address or Password is Incorrect")
            }
        }
    }
    
    @IBAction func btnRegAction(_ sender: Any) {
        
        let vc = loadViewController(StoryBoardName: "UserRegister", VCIdentifer: "UserRegisterViewController") as! UserRegisterViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
  
    
    @IBAction func btnForgotAction(_ sender: Any) {
        
        let vc = loadViewController(StoryBoardName: "Main", VCIdentifer: "ForgotPasswardVC") as! ForgotPasswardVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

