//
//  MenuTableViewCell.swift
//  GO2
//
//  Created by GNM on 5/19/17.
//  Copyright © 2017 Juan Cardozo. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nameOption: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameOption.setTextWhite()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
