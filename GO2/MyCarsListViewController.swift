//
//  MyCarsListViewController.swift
//  GO2
//
//  Created by Juan Cardozo on 8/6/17.
//  Copyright © 2017 GNM. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit

class MyCarsListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: User Data
    let userDefaults = UserDefaults.standard
    
    // MARK: Data
    var itemSelected:MyCar?
    var dataFromAPI = [MyCar]()
    var filteredData = [MyCar]()
    var chosenCellIndex = 0
    
    // MARK: Static Labels
    @IBOutlet weak var labelHeaderTitle: UILabel!
    
    // MARK: Views
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var headerBar: UIView!
    var fromMenu : Bool = false
    /* rgu
    let searchController = UISearchController(searchResultsController: nil)
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        if fromMenu {
            hideMenu()
        }
        else {
            setBackBtn()
        }
        // MARK: Set Side Menu
        navigationController?.customNavBar()
        navigationItem.customTitle()
        
        // MARK: Data for API
        let UserID = userDefaults.value(forKey: "email")
        let UserClient = userDefaults.value(forKey: "client")
        let UserToken = userDefaults.value(forKey: "access-token")
        
        //Style
        navigationController?.customNavBar()
        navigationItem.customTitle()
        
        globalView.setBackgroundWithTopBarToView()
        headerBar.backgroundColor = Color.SubHeaderColor
        labelHeaderTitle.setTextHeader()

        
        dataTableView.delegate = self
        dataTableView.dataSource = self
            
        /* rgu
        searchController.searchResultsUpdater = self as! UISearchResultsUpdating
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.sizeToFit()
        searchController.searchBar.searchBarStyle = UISearchBarStyle.prominent

        definesPresentationContext = true
        dataTableView.tableHeaderView = searchController.searchBar
        
        loadData(UserID:UserID as! String, UserClient:UserClient as! String, UserToken:UserToken as! String)
         */
        
    }
    
    @IBAction func btnCheckAvailability(_ sender: Any) {
    }
    

    @IBAction func btnAddNewCar(_ sender: Any) {
        print("button click")
        let vc = loadViewController(StoryBoardName: "VehicleList", VCIdentifer: "AddNewCarVC") as! AddNewCarVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        /*
        if searchController.isActive && searchController.searchBar.text != ""{
            return filteredData.count
        }
        */
       // return dataFromAPI.count
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MyCarCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MyCarTableViewCell
        
        cell.configureCell(vehicleImage: #imageLiteral(resourceName: "pic"), vehicleBrand: "XYZ", vehiclePlate: "123456", vehicleModel: "ABC", tagvalue: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 150
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
        let centerViewController = loadViewController(StoryBoardName: "CarDetails", VCIdentifer: "CarDetailsVC") as! CarDetailsVC
        centerViewController.headerTitle = "Vehicle Details"
        centerViewController.isMyVehicleDetails = true
        self.navigationController?.pushViewController(centerViewController, animated: true)
        
    }
    

    
    func loadData(UserID:String, UserClient:String, UserToken:String){
        
        ReserveCarApi.sharedInstance.allMyCarsRequest(myView: self.view, uid: UserID, client: UserClient, token: UserToken, completion: {(mycars) -> Void in
            
            print("API My Cars: \(mycars)")
            
            let itemImg = UIImage(named: "car_icon")!
            let NumOfRows = mycars.count
            
            for i in 0...(NumOfRows-1){
                
                var newRow = mycars[i]
                
                let NewItem = MyCar(
                    identifier : newRow["id"].number as! Int,
                    plate: newRow["plate"].string as String!,
                    model: newRow["model"].string as String!,
                    brand: newRow["brand"].string as String!,
                    type: newRow["car_type"].string as String!,
                    imageCar: itemImg
                    )!
                
                self.dataFromAPI += [NewItem]
            }
            self.dataTableView.reloadData()
        })
        
    }
    
}




