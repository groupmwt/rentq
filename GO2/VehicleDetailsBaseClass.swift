//
//  VehicleDetailsBaseClass.swift
//
//  Created by NC2-46 on 23/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class VehicleDetailsBaseClass: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let data = "data"
  }

  // MARK: Properties
  public var status: Bool? = false
  public var data: VehicleDetailsData?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    status = json[SerializationKeys.status].boolValue
    data = VehicleDetailsData(json: json[SerializationKeys.data])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    dictionary[SerializationKeys.status] = status
    if let value = data { dictionary[SerializationKeys.data] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeBool(forKey: SerializationKeys.status)
    self.data = aDecoder.decodeObject(forKey: SerializationKeys.data) as? VehicleDetailsData
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(data, forKey: SerializationKeys.data)
  }

}
