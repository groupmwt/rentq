//
//  MyCarsListViewController.swift
//  GO2
//
//  Created by Juan Cardozo on 8/6/17.
//  Copyright © 2017 GNM. All rights reserved.
//

import Alamofire
import SwiftyJSON
import UIKit

class MyCarsListVC: UIViewController {
    
    // MARK: User Data
    let userDefaults = UserDefaults.standard
//    var arrMyVehicleList = [MyVehicleListData]()
    var arrMyVehicleList = [VehicleDetailsData]()
    var refreshControl = UIRefreshControl()
    var navigationTitle: String = ""
    // MARK: Views
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var btnAdd: UIButtonX!
    
    var fromMenu : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        btnAdd.backgroundColor = Color.buttonBG
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if fromMenu {
            hideMenu()
        }
        else {
            setBackBtn()
        }
        loadDataFromApi()
    }
    
    @IBAction func btnCheckAvailability(_ sender: UIButtonX) {
        let vc = loadViewController(StoryBoardName: "VehicleList", VCIdentifer: "CheckAvailabilityVC") as! CheckAvailabilityVC
        vc.carID = "\(arrMyVehicleList[sender.tag].id!)"
        vc.navigationTitle = arrMyVehicleList[sender.tag].brand ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    @IBAction func btnAddNewCar(_ sender: Any) {
        print("button click")
        let vc = loadViewController(StoryBoardName: "VehicleList", VCIdentifer: "AddNewCarVC") as! AddNewCarVC
        vc.navigationTitle = "Add Vehicle"
        vc.btnSaveTitle = "Save"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
}




