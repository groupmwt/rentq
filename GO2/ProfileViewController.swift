//
//  ProfileViewController.swift
//  GO2
//
//  Created by Juan Cardozo on 23/6/17.
//  Copyright © 2017 GNM. All rights reserved.
//

import UIKit
import CountryPickerView

class ProfileViewController: UIViewController {

    //MARK: Data User
    var homeAddressPlaceholder = "Home Address Here"
    var workAddressPlaceholder = "Work Address Here"
    var userData : EditProfileData!
    
    //MARK: Static Labels
    @IBOutlet weak var labelFirstName: UILabel!
    @IBOutlet weak var labelLastName: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelMobile: UILabel!
    @IBOutlet weak var labelPlaces: UILabel!
    @IBOutlet weak var txtHomeAddress: UITextView!
    @IBOutlet weak var txtWorkAddress: UITextView!
    
    //MARK: Images
    @IBOutlet weak var avatarImg: UIImageViewX!
    
    //MARK: TextFields
    @IBOutlet weak var textFieldFirstName: UITextFieldX!
    @IBOutlet weak var textFieldLastName: UITextFieldX!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldMobile: UITextField!
    
    //MARK: - Views
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var mobileCodePicker: CountryPickerView!
    @IBOutlet weak var phoneView: UIViewX!
    
    @IBOutlet weak var adressViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnPickCountryCode: UIButton!
    @IBOutlet weak var btnEditSave: UIButtonX!
    @IBOutlet weak var btnChangePassword: UIButtonX!
    @IBOutlet weak var imgPencilMark1: UIImageView!
    @IBOutlet weak var imgPencilMark2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       self.title = "My Profile"
         hideMenu()
        self.loadUserValues()
    }
      
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
        
    }
    
    @IBAction func btnEditSave(_ sender: UIButtonX) {
        
        let vc = loadViewController(StoryBoardName: "Profile", VCIdentifer: "ProfileEditVC") as! ProfileEditVC
        vc.userData = self.userData
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btnChangePassword(_ sender: UIButtonX) {
        
        let vc = loadViewController(StoryBoardName: "Profile", VCIdentifer: "ChangePasswordVC") as! ChangePasswordVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

