//
//  Go2Helpers.swift
//  GO2
//
//  Created by GNM on 5/22/17.
//  Copyright © 2017 Juan Cardozo. All rights reserved.
//

import UIKit

class Go2Helpers {
    
    static let sharedInstance:Go2Helpers = Go2Helpers()
    
    private init(){}
    
    static func isLessThanIPhone6() -> Bool{
        
        let model: String = UIDevice.current.modelName
        return (model == "iPod 5" || model == "iPhone 5" || model == "iPhone 5c" ||
                model == "iPhone 5s" || model == "iPhone SE")
                //|| model == "Simulator")
    }
    
    static func isPlus() -> Bool{
        
        let model: String = UIDevice.current.modelName
        return (model == "iPhone 6 Plus" || model == "iPhone 6s Plus" || model == "iPhone 7 Plus")
            //|| model == "Simulator")
    }
    
    static func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    static func isModal(mainView:UIViewController) -> Bool {
        if mainView.presentingViewController != nil {
            return true
        } else if mainView.navigationController?.presentingViewController?.presentedViewController == mainView.navigationController  {
            return true
        } else if mainView.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        return false
    }
}

