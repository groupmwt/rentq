//
//  ForgotPasswardVC.swift
//  GO2
//
//  Created by NC2-46 on 06/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit

class ForgotPasswardVC: UIViewController {

    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtEmail: UITextField!

    //MARK: - Views
    @IBOutlet weak var globalView: UIView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        guard validateData() else { return }
        //Get Textfield Data
        let userEmail = txtEmail.text!

        UserApi.sharedInstance.forgotPasswordRequest(myView: self.view, email: userEmail) { (message) in
   
            print("Return User: \(message)")
            
            self.displayAlertWithAction(userMessage: message ?? "", userTitle: "Forgot Password", alertStyle: .alert, arrActionName: ["Done"], arrActionStyle: [.default], completion: { (btnIndex) in
                
                switch btnIndex {
                case 0:
                    self.backTap()
                    break
                default :
                    break
                }
            })
        }
        
        //self.backTap()
    }
    
}


