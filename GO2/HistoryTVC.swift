//
//  HistoryTVC.swift
//  GO2
//
//  Created by NC2-46 on 07/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit

class HistoryTVC: UITableViewCell {

    @IBOutlet weak var mainView: UIViewX!
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblPlate: UILabel!
    @IBOutlet weak var lblModel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func configureCell(vehicleImageURL: String?,vehicleStartDate : String, vehicleEndDate:String,vehiclePlate:String,vehicleModel:String) {
        
        let finalUrl : String = getConexionAPI_Production() + (vehicleImageURL ?? "" )
        let url = URL.init(string: finalUrl)
        vehicleImage.sd_setShowActivityIndicatorView(true)
        vehicleImage.sd_setIndicatorStyle(.gray)
        vehicleImage.sd_setImage(with: url , placeholderImage: #imageLiteral(resourceName: "car_icon"))
        self.lblStartDate.text = "Start Date : " + vehicleStartDate
        self.lblEndDate.text = "End Date  : " + vehicleEndDate
        self.lblPlate.text = "Plate : " + vehiclePlate
        self.lblModel.text = "Model : " + vehicleModel
    }
}
