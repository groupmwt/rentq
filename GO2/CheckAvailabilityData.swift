//
//  CheckAvailabilityData.swift
//
//  Created by NC2-46 on 24/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CheckAvailabilityData: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let carId = "car_id"
    static let id = "id"
    static let offerDate = "offer_date"
    static let activeInSystem = "active_in_system"
    static let reservationId = "reservation_id"
    static let price = "price"
  }

  // MARK: Properties
  public var carId: Int?
  public var id: Int?
  public var offerDate: String?
  public var activeInSystem: Bool? = false
  public var reservationId: Int?
  public var price: Float?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    carId = json[SerializationKeys.carId].int
    id = json[SerializationKeys.id].int
    offerDate = json[SerializationKeys.offerDate].string
    activeInSystem = json[SerializationKeys.activeInSystem].boolValue
    reservationId = json[SerializationKeys.reservationId].int
    price = json[SerializationKeys.price].float
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = carId { dictionary[SerializationKeys.carId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = offerDate { dictionary[SerializationKeys.offerDate] = value }
    dictionary[SerializationKeys.activeInSystem] = activeInSystem
    if let value = reservationId { dictionary[SerializationKeys.reservationId] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.carId = aDecoder.decodeObject(forKey: SerializationKeys.carId) as? Int
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.offerDate = aDecoder.decodeObject(forKey: SerializationKeys.offerDate) as? String
    self.activeInSystem = aDecoder.decodeBool(forKey: SerializationKeys.activeInSystem)
    self.reservationId = aDecoder.decodeObject(forKey: SerializationKeys.reservationId) as? Int
    self.price = aDecoder.decodeObject(forKey: SerializationKeys.price) as? Float
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(carId, forKey: SerializationKeys.carId)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(offerDate, forKey: SerializationKeys.offerDate)
    aCoder.encode(activeInSystem, forKey: SerializationKeys.activeInSystem)
    aCoder.encode(reservationId, forKey: SerializationKeys.reservationId)
    aCoder.encode(price, forKey: SerializationKeys.price)
  }

}
