//
//  MyCarsDetailCalendarViewController.swift
//  GO2
//
//  Created by Juan Cardozo on 9/6/17.
//  Copyright © 2017 GNM. All rights reserved.
//

import UIKit
import FSCalendar

class CheckAvailabilityVC : UIViewController {
    
    // MARK: User Data
    let userDefaults = UserDefaults.standard
    var arrCheckAvailability = [CheckAvailabilityData]()
    var carID : String!
    var selectedDate : String!
    var price = "0"
    var offerID = ""
    var arrReserverDate = [String]()
    var arrAvailableDate = [String]()
    let formatter = DateFormatter()
    var isAnyApiCalled: Bool = false
    var navigationTitle = ""
    // Set date format
    var calendar = NSCalendar.init(calendarIdentifier: NSCalendar.Identifier.gregorian)
    var currentPage: Date?
    // first date in the range
    var firstDate: Date?
    // last date in the range
    var lastDate: Date?
    var datesRange: [Date]?
    lazy var today: Date = {
        return Date()
    }()
    
    //MARK: - Buttons
    @IBOutlet weak var btnAddAvailability: UIButton!
    
    @IBOutlet weak var btnDeleteAvailability: UIButtonX!
    
    
    //MARK: - Views
    @IBOutlet weak var carCalendar: FSCalendar!
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var btnNextCalender: UIButton!
    
    @IBOutlet weak var btnPrevCalender: UIButton!
    @IBOutlet weak var calenderHeader: UIView!
    @IBOutlet weak var currentDateTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
  
    }
    
    
    
    
    @IBAction func btnPrevClick(_ sender: Any) {

        self.moveCurrentPage(moveUp: false)
    }
    
    @IBAction func btnNextClick(_ sender: Any) {

       self.moveCurrentPage(moveUp: true)
    }

}

extension Date {
    
    func getMonthName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM YYYY"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    
    
}
