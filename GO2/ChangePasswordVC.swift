//
//  ChangePasswordVC.swift
//  GO2
//
//  Created by NC2-46 on 27/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
    //MARK: - Views
    @IBOutlet weak var globalView: UIView!
    
    //MARK: - textfields
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnChangePass: UIButtonX!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Change Password"
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: Login Action
    @IBAction func changePasswordAction(_ sender: UIButtonX) {
        self.view.endEditing(true)
        guard validateData() else { return }

        let userNewPassword = txtNewPassword.text!
        let userConfirmPassword = txtConfirmPassword.text!
    
        // MARK: API Request
        UserApi.sharedInstance.changePasswordRequest(myView: self.view, newPassword: userNewPassword, confirmPassword: userConfirmPassword) { (data) in
            
            if data.message != nil {
                self.displayAlertWithAction(userMessage: data.message!, userTitle: "Information", alertStyle: .alert, arrActionName: ["Done"], arrActionStyle: [.default], completion: { (btnIndex) in
                    
                    switch btnIndex {
                    case 0:
                        self.backTap()
                        break
                    default :
                        break
                    }
                })
                
            }
            
        }
        
    }
    
}
