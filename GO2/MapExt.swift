//
//  MapExt.swift
//  GO2
//
//  Created by NC2-46 on 18/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces
import CoreLocation


extension MapVC {
    func setupUI() {
        //Style
        navigationController?.customNavBar()
        globalView.setBackgroundWithTopBarToView()
        lblBookCar.setTextWhite()
        lblMyCar.setTextWhite()
        
        //sideMenus()
        hideMenu()
        self.title = "RENT-Q"
        self.navigationItem.rightBarButtonItem = nil
        let filterBtn : UIButton!
        filterBtn  = UIButton(frame : CGRect(x:0,y:0,width: 34, height :100))
        filterBtn.setImage(#imageLiteral(resourceName: "filter"), for: .normal)
        filterBtn.setImage(#imageLiteral(resourceName: "filter"), for: .selected)
        filterBtn.setImage(#imageLiteral(resourceName: "filter"), for: .highlighted)
        filterBtn.addTarget(self, action: #selector(actionFilter(_:)), for: .touchDown)
        let rightBarButton : UIBarButtonItem = UIBarButtonItem(customView : filterBtn)
        self.navigationItem.rightBarButtonItem = rightBarButton
      
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 10)
        
        self.searchBar.textAlignment = .left
        searchBar.text = ""
        searchBar.delegate = self
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        if isFilterResult {
            DispatchQueue.main.async {
                for i in 0..<self.arrFilterResultData.count {
                    
                    
                    if (self.arrFilterResultData[i].lat != nil) && (self.arrFilterResultData[i].lng != nil) {
                        let lat = self.arrFilterResultData[i].lat!
                        let long = self.arrFilterResultData[i].lng!
                        self.setMarkerLocation(lat: lat, long: long, tagValue: i,data: self.arrFilterResultData[i])
                    }
                    
                }
              
                if self.focusOnCorrdinate != nil {
                    
                    let myCamera = GMSCameraPosition.camera(withLatitude: self.focusOnCorrdinate.latitude, longitude: self.focusOnCorrdinate.longitude, zoom: 14)
                    self.mapView.camera = myCamera
                }
                
            }
            
        }
        
    }
    
    func setMarkerLocation(lat: Double , long : Double,tagValue : Int , data: Any? = nil,isCameraFocus:Bool = true){
        
        
        let Location = CLLocationCoordinate2DMake(lat,long)

        let marker = GMSMarker(position: Location)

        marker.map = mapView
        marker.accessibilityValue = "\(tagValue)"
        marker.appearAnimation = .pop
        marker.isTappable = true
        marker.userData = data
        let zoomLavel:Float = 14.0
        marker.icon = UIImage(named: "pin")?.resizeImageSize()
        
        
//        if isFilterResult == false {
            if isCameraFocus {
                let myCamera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: zoomLavel)
                mapView.camera = myCamera
            }
//        }
        

        print("location",Location)
    }
    
    @objc func actionFilter(_ sender : UIButton) {
        let centerViewController = loadViewController(StoryBoardName: "Filter", VCIdentifer: "FilterVC") as! FilterVC
        self.navigationController?.pushViewController(centerViewController, animated: true)
    }
}

extension MapVC : UISearchBarDelegate {
    
    @IBAction func txtStartEditing(_ sender: UITextField) {
        searchBarView.isHidden = true
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchBar.delegate = self
        searchController?.isActive = true
        DispatchQueue.main.async {
            self.searchController?.searchBar.becomeFirstResponder()
        }
        searchController?.searchResultsUpdater = resultsViewController
        searchController?.view.backgroundColor = UIColor.clear
        
        if UIDevice.current.hasNotch {
            subView = UIView(frame: CGRect(x: 0, y: 86, width: self.searchBar.bounds.width, height: 45.0))
            resultsViewController?.view.frame.origin.y = 131
//            resultsViewController?.view.frame = CGRect(x: 0, y: 131, width: self.view.bounds.width - 20, height: self.view.bounds.height - 70)
        } else {
            subView = UIView(frame: CGRect(x: 0, y: 65, width: self.searchBar.bounds.width, height: 45.0))
            resultsViewController?.view.frame.origin.y = 110
//            resultsViewController?.view.frame = CGRect(x: 0, y: 110, width: self.view.bounds.width - 20, height: self.view.bounds.height - 70)
        }
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        definesPresentationContext = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchController?.searchBar.resignFirstResponder()
        searchController?.isActive = false
        searchBarView.isHidden = false
        subView.isHidden = true
    }
    
}

// Handle the user's selection.
extension MapVC: GMSAutocompleteResultsViewControllerDelegate,UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        searchBar.text = place.name
        searchBarView.isHidden = false
        subView.isHidden = true
        searchBar.delegate = self
        reverseGeocodeCoordinate(coordinate: place.coordinate)
        
    }
    
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension MapVC : GMSMapViewDelegate {
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        guard let _ = mapView.myLocation?.coordinate.latitude,
            let _ = mapView.myLocation?.coordinate.longitude else { return false }

        reverseGeocodeCoordinate(coordinate: (mapView.myLocation?.coordinate)!,zoomLavel: 18)
        return true
    }
    
        
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D,zoomLavel:Float = 14.0) {
        
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                AppDel.country = address.country
                AppDel.state = address.administrativeArea
                AppDel.city = address.locality
                // 3
                if let city : String = address.locality {
                   
                    MapApi.sharedInstance.getNearbyVehiclesRequest(myView: self.view, cityName: city, latitude:"\(coordinate.latitude)", longitude:"\(coordinate.longitude)")
                    {(nearByVehicle) in
                        self.nearByVehicleData = nearByVehicle
                        self.mapView.clear()
                        for i in 0..<self.nearByVehicleData.count {
                            if (self.nearByVehicleData[i].lat != nil) && (self.nearByVehicleData[i].lng != nil) {
                                let lat = Double(self.nearByVehicleData[i].lat!) ?? 0.0
                                let long = Double(self.nearByVehicleData[i].lng!) ?? 0.0
                                self.setMarkerLocation(lat: lat, long: long, tagValue: i,data: self.nearByVehicleData[i],isCameraFocus: false)
                            }
                        }
                    }
                }
                
                // 4
                UIView.animate(withDuration: 0.25) {
                    self.view.layoutIfNeeded()
                }
                let location = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: zoomLavel)
                
                self.mapView.camera = location
            }
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if marker.userData != nil {
            
        }
        if marker.userData is SearchVehicleData {
            let data = marker.userData as! SearchVehicleData
            marker.title = data.model
            mapView.selectedMarker = marker
//            displayMyAlertMessage(userMessage: "Marker clicked : \(data.model ?? "")")
            let centerViewController = loadViewController(StoryBoardName: "CarDetails", VCIdentifer: "CarDetailsVC") as! CarDetailsVC
            centerViewController.searchVehicleData = data
            centerViewController.isDataComeFromNearbyVehicle = false
            centerViewController.vehicleId = data.id
            self.navigationController?.pushViewController(centerViewController, animated: true)
        }
        else if marker.userData is GetNearByVehicleData {
            let data = marker.userData as! GetNearByVehicleData
            marker.title = data.model
            mapView.selectedMarker = marker
//            displayMyAlertMessage(userMessage: "Marker clicked : \(data.model ?? "")")
            let centerViewController = loadViewController(StoryBoardName: "CarDetails", VCIdentifer: "CarDetailsVC") as! CarDetailsVC
            centerViewController.isDataComeFromNearbyVehicle = true
            centerViewController.vehicleId = data.id
            self.navigationController?.pushViewController(centerViewController, animated: true)
            
        }
        else {

        }
        return true
    }

    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
    }
    
    
   
}


// MARK: - CLLocationManagerDelegate
extension MapVC: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            if !isFilterResult {
                if isGetNearApiCalled == false {
                    reverseGeocodeCoordinate(coordinate: location.coordinate)
                    isGetNearApiCalled = true
                }
                else{
                    
                }
            }
            locationManager.stopUpdatingLocation()
        }
    }
}

extension UIDevice {
    var hasNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
}
