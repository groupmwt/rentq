//
//  CD_Ride_Type+CoreDataProperties.swift
//  GO2
//
//  Created by NC2-46 on 17/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//
//

import Foundation
import CoreData


extension CD_Ride_Type {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CD_Ride_Type> {
        return NSFetchRequest<CD_Ride_Type>(entityName: "CD_Ride_Type")
    }

    @NSManaged public var name: String?

}
