//
//  CustomAlertVC.swift
//  GO2
//
//  Created by NC2-46 on 04/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit

class CustomAlertVC: UIViewController {

    var titleText = ""
    var titleMessage = ""
    var carID = ""
    var offerID = ""
    var selectedDate = ""
    var price = ""
    var isPriceChangeApiCall : Bool = true
    var superView1 : CheckAvailabilityVC!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var btnCancle: UIButtonX!
    @IBOutlet weak var btnDone: UIButtonX!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPrice.placeholder = "Enter price here"
        txtPrice.text = price
        txtPrice.keyboardType = .decimalPad
        txtPrice.addLineToView(position: .LINE_POSITION_BOTTOM, color: Color.GradientMenuStart)
        btnCancle.backgroundColor = Color.buttonBG
        btnDone.backgroundColor = Color.buttonBG
        lblTitle.backgroundColor = Color.buttonBG
        lblTitle.text = titleText
        lblMessage.text = titleMessage
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        
    }

    @IBAction func btnDone(_ sender: UIButtonX) {
        if (txtPrice.text != nil) && (txtPrice.text != "") {
            if isPriceChangeApiCall {
                superView1.updateOffer(carID: self.carID, offerID: self.offerID, price: txtPrice.text!)
                removeAnimate()
            }
            else {
                superView1.addOffer(carID: self.carID, start_date: self.selectedDate, end_date: self.selectedDate, price: txtPrice.text!)
                removeAnimate()
            }
        }
        else {
           displayMyAlertMessage(userMessage: "Please enter valid price")
        }
        
        
    }
    
    @IBAction func btnCancel(_ sender: UIButtonX) {
        removeAnimate()
    }
    
}
