//
//  MyHistoryData.swift
//
//  Created by NC2-46 on 31/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class MyHistoryData: NSCoding,JSONable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let cityId = "city_id"
    static let name = "name"
    static let address = "address"
    static let countryId = "country_id"
    static let phoneNumber = "phone_number"
    static let model = "model"
    static let carType = "car_type"
    static let brand = "brand"
    static let stateId = "state_id"
    static let plate = "plate"
    static let id = "id"
    static let image = "image"
    static let countryCode = "country_code"
    static let startDate = "start_date"
    static let rideType = "ride_type"
    static let endDate = "end_date"
  }

  // MARK: Properties
  public var cityId: Int?
  public var name: String?
  public var address: String?
  public var countryId: Int?
  public var phoneNumber: String?
  public var model: String?
  public var carType: String?
  public var brand: String?
  public var stateId: Int?
  public var plate: String?
  public var id: Int?
  public var image: MyHistoryImage?
  public var countryCode: String?
  public var startDate: String?
  public var rideType: String?
  public var endDate: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    cityId = json[SerializationKeys.cityId].int
    name = json[SerializationKeys.name].string
    address = json[SerializationKeys.address].string
    countryId = json[SerializationKeys.countryId].int
    phoneNumber = json[SerializationKeys.phoneNumber].string
    model = json[SerializationKeys.model].string
    carType = json[SerializationKeys.carType].string
    brand = json[SerializationKeys.brand].string
    stateId = json[SerializationKeys.stateId].int
    plate = json[SerializationKeys.plate].string
    id = json[SerializationKeys.id].int
    image = MyHistoryImage(json: json[SerializationKeys.image])
    countryCode = json[SerializationKeys.countryCode].string
    startDate = json[SerializationKeys.startDate].string
    rideType = json[SerializationKeys.rideType].string
    endDate = json[SerializationKeys.endDate].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = cityId { dictionary[SerializationKeys.cityId] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = countryId { dictionary[SerializationKeys.countryId] = value }
    if let value = phoneNumber { dictionary[SerializationKeys.phoneNumber] = value }
    if let value = model { dictionary[SerializationKeys.model] = value }
    if let value = carType { dictionary[SerializationKeys.carType] = value }
    if let value = brand { dictionary[SerializationKeys.brand] = value }
    if let value = stateId { dictionary[SerializationKeys.stateId] = value }
    if let value = plate { dictionary[SerializationKeys.plate] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = image { dictionary[SerializationKeys.image] = value.dictionaryRepresentation() }
    if let value = countryCode { dictionary[SerializationKeys.countryCode] = value }
    if let value = startDate { dictionary[SerializationKeys.startDate] = value }
    if let value = rideType { dictionary[SerializationKeys.rideType] = value }
    if let value = endDate { dictionary[SerializationKeys.endDate] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.cityId = aDecoder.decodeObject(forKey: SerializationKeys.cityId) as? Int
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.countryId = aDecoder.decodeObject(forKey: SerializationKeys.countryId) as? Int
    self.phoneNumber = aDecoder.decodeObject(forKey: SerializationKeys.phoneNumber) as? String
    self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
    self.carType = aDecoder.decodeObject(forKey: SerializationKeys.carType) as? String
    self.brand = aDecoder.decodeObject(forKey: SerializationKeys.brand) as? String
    self.stateId = aDecoder.decodeObject(forKey: SerializationKeys.stateId) as? Int
    self.plate = aDecoder.decodeObject(forKey: SerializationKeys.plate) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? MyHistoryImage
    self.countryCode = aDecoder.decodeObject(forKey: SerializationKeys.countryCode) as? String
    self.startDate = aDecoder.decodeObject(forKey: SerializationKeys.startDate) as? String
    self.rideType = aDecoder.decodeObject(forKey: SerializationKeys.rideType) as? String
    self.endDate = aDecoder.decodeObject(forKey: SerializationKeys.endDate) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(cityId, forKey: SerializationKeys.cityId)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(countryId, forKey: SerializationKeys.countryId)
    aCoder.encode(phoneNumber, forKey: SerializationKeys.phoneNumber)
    aCoder.encode(model, forKey: SerializationKeys.model)
    aCoder.encode(carType, forKey: SerializationKeys.carType)
    aCoder.encode(brand, forKey: SerializationKeys.brand)
    aCoder.encode(stateId, forKey: SerializationKeys.stateId)
    aCoder.encode(plate, forKey: SerializationKeys.plate)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(countryCode, forKey: SerializationKeys.countryCode)
    aCoder.encode(startDate, forKey: SerializationKeys.startDate)
    aCoder.encode(rideType, forKey: SerializationKeys.rideType)
    aCoder.encode(endDate, forKey: SerializationKeys.endDate)
  }

}
