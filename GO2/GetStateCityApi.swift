//
//  GetStateCityApi.swift
//  GO2
//
//  Created by C115 on 18/02/21.
//  Copyright © 2021 Juan Cardozo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

//TKP
class GetStateCityApi{
    static let sharedInstance:GetStateCityApi = GetStateCityApi()
    private init() {}
    
    func getCarTypesRequest(myView: UIView, completion: @escaping ([States]) -> Void)  {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header = [ "access-token": getAccessToken(),
                            "client": getClientToken(),
                            "uid": getUID() ]
                
        let urlString = getPathUrl() + APIS.getStates
        
        Alamofire.request(urlString, method: .get, headers: Auth_header).validate().responseJSON{
            response in
            var searchResult = [States]()
            
            switch response.result {
                case .success:
                    print("STATE CITY -- API Connect Successful")
                    
                    if let value: AnyObject = response.result.value as AnyObject? {
                        
                            updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                            
                            let apiReturn = JSON(value)
                            if let object = apiReturn.to(type: States.self){
                                searchResult = object as! [States]
                            }
                            print(searchResult)
                            myView.stopIndicator(indicator: indicator, container: container)
                            completion(searchResult)
                    }
                break
                
                case .failure(let error):
                    print("ERROR IN CONECTION: \(error)")
                    displayMyAlertMessage(userMessage: error.localizedDescription)
                    myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
}

