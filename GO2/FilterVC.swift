//
//  FilterVC.swift
//  GO2
//
//  Created by NC2-46 on 16/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit
import CoreLocation

class FilterVC: UIViewController {

    //MARK: Labels
    @IBOutlet weak var labelBarTitle: UILabel!
    @IBOutlet weak var labelRentalDate: UILabel!
    @IBOutlet weak var labelVehicleType: UILabel!
    @IBOutlet weak var labelRideType: UILabel!
    @IBOutlet weak var labelRentalCountry: UILabel!
    @IBOutlet weak var labelRentalState: UILabel!
    @IBOutlet weak var labelRentalCity: UILabel!
    @IBOutlet weak var labelReturnAddress: UILabel!
    @IBOutlet weak var labelReturnDate: UILabel!
    
    //MARK: Textfields
    @IBOutlet weak var rentalDateTF: UITextField!
    
    @IBOutlet weak var rentalVehicleCategoryTF: UITextField!
    @IBOutlet weak var rentalRideTypeTF: UITextField!
    
    @IBOutlet weak var rentalCountryTF: UITextField!
    @IBOutlet weak var rentalStateTF: UITextField!
    @IBOutlet weak var rentalCityTF: UITextField!
    @IBOutlet weak var rentalAddressTF: UITextView!
    @IBOutlet weak var returnDateTF: UITextField!
    
    //MARK: Buttons
    @IBOutlet weak var searchBtn: UIButton!
    
    //MARK: Views
    @IBOutlet weak var globalView: UIView!
    @IBOutlet weak var headerBar: UIView!
    
    @IBOutlet weak var cityStateStack: UIStackView!
    var stateView = UIView()
    var cityView = UIView()
    
    // MARK: Data Arrays
    var LocationDetails : LocationData!
    var arrCountrys = [CountriesModel]() //[CD_Country]()
    var arrStates = [StatesModel]()//[CD_State]()
    var arrCities = [CitiesModel]()//[CD_City]()
    var arrTypeCars = [CarTypeData]()
    var arrRideType = [CD_Ride_Type]()
    var arrVehicleCategory = [CD_Vehicle_Category]()
    var selectedCorrdinate : CLLocationCoordinate2D!
    var selectedRideType = String()
    //MARK: Data Pickers
    let toolBar = UIToolbar()
    
    let rentalDatePickerIdentifier = "rentalDatePicker"
    let rentalDatePicker = UIDatePicker()
    
    let returnDatePickerIdentifier = "returnDatePicker"
    let returnDatePicker = UIDatePicker()
    
    let vehicleTypePickerViewIdentifier = "vehicleTypeView"
    var vehicleTypePickerView = UIPickerView()
    
    let rideTypePickerViewIdentifier = "rideTypeView"
    var rideTypePickerView = UIPickerView()
    
    let countryPickerViewIdentifier = "countryPickerView"
    var countryPickerView = UIPickerView()
    
    let statePickerViewIdentifier = "statePickerView"
    var statePickerView = UIPickerView()
    
    let cityPickerViewIdentifier = "cityPickerView"
    var cityPickerView = UIPickerView()
    
    // Create date formatter
    let dateFormatter: DateFormatter = DateFormatter()
    let timeFormatter: DateFormatter = DateFormatter()
    
    //MARK: Formatter
    let formatter = DateFormatter()

    var navigationTitle: String = "Find Vehicle"
    var isRentalDateSelected: Bool = false
    var isRentalVehicleTypeSelected: Bool = false
    var isRentalRideTypeSelected: Bool = false
    var isRentalCountrySelected: Bool = false
    var isRentalStateSelected: Bool = false
    var isRentalCitySelected: Bool = false
    var isRentalAddressSelected: Bool = false
    var isReturnDateSelected: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        TinyToast.shared.dismissAll()
    }
    
    @IBAction func searchCars(_ sender: UIButton) {
        self.view.endEditing(true)
        guard validateData() else { return }
        
        let startDate = rentalDateTF.text!
        let endDate = returnDateTF.text!
        let carType = rentalVehicleCategoryTF.text!
        let city = rentalCityTF.text!
        let rideType = rentalRideTypeTF.text!
        
//        BookingCarApi.sharedInstance.searchCarsRequest(myView: self.view, city: city, start_date: startDate, end_date: endDate, car_type: carType, ride_type: rideType) { (searchResult) in
            
        BookingCarApi.sharedInstance.searchCarsRequest(myView: self.view, city: city, state: rentalStateTF.text ?? "", country: rentalCountryTF.text ?? "", lat: selectedCorrdinate.latitude, lng: selectedCorrdinate.longitude, start_date: startDate, end_date: endDate, car_type: carType, ride_type: selectedRideType) { (searchResult) in
            
            let centerVC = self.loadViewController(StoryBoardName: "Map", VCIdentifer: "MapVC") as! MapVC
            centerVC.arrFilterResultData = searchResult
            centerVC.focusOnCorrdinate = self.selectedCorrdinate
            centerVC.isFilterResult = true
            self.navigationController?.pushViewController(centerVC, animated: true)
        }
        
    }
}
