//
//  UserRegisterExtension.swift
//  GO2
//
//  Created by NC2-46 on 10/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import CountryPickerView

extension UserRegisterViewController {
    
    func setupUI(){
        //Style
        globalView.setBackgroundToView()
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.customNavBar()
        
        setBackBtn()
        
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        txtPhone.delegate = self
        txtPassword.delegate = self
        txtConfirmPassword.delegate = self
        
        txtFirstName.autocapitalizationType = .words
        txtLastName.autocapitalizationType = .words
        txtEmail.keyboardType = .emailAddress
        txtPhone.textType = .phone
        txtPhone.keyboardType = .numberPad
        txtPassword.textType = .password
        txtConfirmPassword.textType = .password
        
        self.mobileCodePicker.delegate = self
        self.mobileCodePicker.dataSource = self
        self.mobileCodePicker.showPhoneCodeInView = true
        self.mobileCodePicker.showCountryCodeInView = true
    
        self.mobileCodePicker.setCountryByCode("DO")
        self.mobileCodePicker.countryDetailsLabel.textColor = Color.TextYellow
        txtPhone.setPlaceHolderTextColor(Color.textfiledPlaceholderColor)
        btnSignup.backgroundColor = Color.buttonBG
    }
}

extension UserRegisterViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


//Mark: - Extension For Validation
extension UserRegisterViewController {
    //Mark: - Validate Function for checking fileds proper data.
    func validateData() -> Bool {
        guard !(txtFirstName.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter first name")
            return false
        }
        guard !(txtLastName.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter last name")
            return false
        }
        guard !(txtEmail.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter email address")
            return false
        }
        guard (txtEmail.hasValidEmail) else{
            displayMyAlertMessage(userMessage: "Please enter valid email address")
            return false
        }
        
        guard !(txtPhone.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter phone number")
            return false
        }
        guard (txtPhone.isValidMobileNumber) else{
            displayMyAlertMessage(userMessage: "Phone number must be 6-15 digits")
            return false
        }
        guard !(txtPassword.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter password")
            return false
        }
        
        guard (txtPassword.isValidPassword) else{
            displayMyAlertMessage(userMessage: "Password length should be at least 6 characters")
            return false
        }
        
        guard !(txtConfirmPassword.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter Confirm password")
            return false
        }
        
        guard (txtPassword.text == txtConfirmPassword.text) else{
            displayMyAlertMessage(userMessage: "Password and Confirm password does not match")
            return false
        }
        return true
    }
}


extension UserRegisterViewController: CountryPickerViewDelegate,CountryPickerViewDataSource {
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        //userCountryCode = country.code
        mobileCodePicker.countryDetailsLabel.textColor = Color.TextYellow
        
    }
    
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        self.title = ""
        return "Pick country code"
    }
    
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
    
}
