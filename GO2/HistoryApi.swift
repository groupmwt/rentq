//
//  HistoryApi.swift
//  GO2
//
//  Created by NC2-46 on 21/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class HistoryApi {
    
    // MARK: - Singelton
    static let sharedInstance:HistoryApi = HistoryApi()
    
    private init() {}
    
    //private let pathUrl = getConexionAPI_Production() + general.versionPath
    
    
    // MARK: - ApiMethods
    
    // ********************************************************
    // MARK: My Reserved Vehicles API Request
    // ********************************************************
    func getMyReservedVehiclesRequest(myView: UIView, completion: @escaping ([MyHistoryData]) -> Void) {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
        let encodedURL = getPathUrl() + APIS.myReservations
        
        print("URL: \(encodedURL)")
        print("Parameter: \(Auth_header)")
        
        Alamofire.request(encodedURL, method: .get, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON { (response) in
            var myReseredData = [MyHistoryData]()
            var errorMessage = ""
            switch response.result {
            
            case .success:
                print("getMyReservedVehiclesRequest - API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                    else {
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        
                        if let result = value["data"] as? NSArray{
                            
                            let apiReturn = JSON(result)
                            if let object = apiReturn.to(type: MyHistoryData.self){
                                myReseredData = object as! [MyHistoryData]
                            }
                            // print(carType)
                            myView.stopIndicator(indicator: indicator, container: container)
                            completion(myReseredData)
                        }
                    }
                }
                break
                
            case .failure( _):
                
                if let data = response.data {
                    let responseJSON = JSON(data)
                    
                    if let message: String = responseJSON["errors"].array?[0].rawString() {
                        if !message.isEmpty {
                            errorMessage = message
                        }
                    }
                }
                
                print("ERROR IN CONECTION: \(errorMessage)")
                displayMyAlertMessage(userMessage: errorMessage)
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
    
    // ********************************************************
    // MARK: My Reserved Vehicles API Request
    // ********************************************************
    func getMyRentalVehiclesRequest(myView: UIView, completion: @escaping ([MyHistoryData]) -> Void) {
        
        myView.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        
        let Auth_header : [String : String] = [
            "access-token": getAccessToken() ,
            "client": getClientToken() ,
            "uid": getUID() ]
        
        let encodedURL = getPathUrl() + APIS.myRents
        
        print("URL: \(encodedURL)")
        print("Parameter: \(Auth_header)")
        
        Alamofire.request(encodedURL, method: .get, encoding: JSONEncoding.default, headers: Auth_header).validate().responseJSON { (response) in
            var myReseredData = [MyHistoryData]()
            var errorMessage = ""
            switch response.result {
            
            case .success:
                print("getMyRentalVehiclesRequest - API Connect Successful")
                
                if let value: AnyObject = response.result.value as AnyObject? {
                    
                    if value["status"] as? Bool == false {
                        displayMyAlertMessage(userMessage: (value["error"] as? String)!)
                        myView.stopIndicator(indicator: indicator, container: container)
                    }
                    else {
                        updateHeaderData(response: JSON((response.response?.allHeaderFields)!))
                        
                        if let result = value["data"] as? NSArray{
                            
                            let apiReturn = JSON(result)
                            if let object = apiReturn.to(type: MyHistoryData.self){
                                myReseredData = object as! [MyHistoryData]
                            }
                            // print(carType)
                            myView.stopIndicator(indicator: indicator, container: container)
                            completion(myReseredData)
                        }
                    }
                }
                break
                
            case .failure( _):
                
                if let data = response.data {
                    let responseJSON = JSON(data)
                    
                    if let message: String = responseJSON["errors"].array?[0].rawString() {
                        if !message.isEmpty {
                            errorMessage = message
                        }
                    }
                }
                
                print("ERROR IN CONECTION: \(errorMessage)")
                displayMyAlertMessage(userMessage: errorMessage)
                myView.stopIndicator(indicator: indicator, container: container)
                break
            }
        }
    }
    
}
