//
//  BookingResultExt.swift
//  GO2
//
//  Created by NC2-46 on 17/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import GoogleMaps
import CoreLocation


extension BookingResultVC {
    func setupUI() {
        //Style
        navigationController?.customNavBar()
        globalView.setBackgroundWithTopBarToView()

        setBackBtn()
        self.title = "Near By Vehicles"
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        //mapView.settings.myLocationButton = true
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 20)
        
        
        DispatchQueue.main.async {
            for i in 0..<self.arrSearchResultData.count {
                
                
                if (self.arrSearchResultData[i].lat != nil) && (self.arrSearchResultData[i].lng != nil) {
                    let lat = Double(self.arrSearchResultData[i].lat!) ?? 0.0
                    let long = Double(self.arrSearchResultData[i].lng!) ?? 0.0
                    self.setMarkerLocation(lat: lat, long: long, tagValue: i,data: self.arrSearchResultData[i])
                }
                
            }
            
            if self.focusOnCorrdinate != nil {
                let myCamera = GMSCameraPosition.camera(withLatitude: self.focusOnCorrdinate.latitude, longitude: self.focusOnCorrdinate.longitude, zoom: 14)
                self.mapView.camera = myCamera
            }
            
        }
        
        }
    
    func setMarkerLocation(lat: Double , long : Double,tagValue : Int , data: Any? = nil){
        
        
        let Location = CLLocationCoordinate2DMake(lat,long)
        
        let marker = GMSMarker(position: Location)
        
        marker.map = self.mapView
        marker.accessibilityValue = "\(tagValue)"
        marker.appearAnimation = .pop
        marker.isTappable = true
        marker.userData = data
        marker.icon = UIImage(named: "pin")?.resizeImageSize()
        
        print("location",Location)
    }
        
}
    


extension BookingResultVC : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if marker.userData != nil {
            
        }
        if marker.userData is SearchVehicleData {
            let data = marker.userData as! SearchVehicleData
            marker.title = data.model
            mapView.selectedMarker = marker
            let centerViewController = loadViewController(StoryBoardName: "CarDetails", VCIdentifer: "CarDetailsVC") as! CarDetailsVC
            
            centerViewController.searchVehicleData = data
            self.navigationController?.pushViewController(centerViewController, animated: true)
        }
        else {
            
        }
        return true
    }
    
//    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
////        if marker.userData is SearchVehicleData {
////            let data = marker.userData as! SearchVehicleData
////
////            let centerViewController = loadViewController(StoryBoardName: "CarDetails", VCIdentifer: "CarDetailsVC") as! CarDetailsVC
////
////            centerViewController.searchVehicleData = data
////            self.navigationController?.pushViewController(centerViewController, animated: true)
////        }
//    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
    }
}

