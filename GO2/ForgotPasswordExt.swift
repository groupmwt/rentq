//
//  ForgotPasswordExt.swift
//  GO2
//
//  Created by NC2-46 on 10/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
extension ForgotPasswardVC {
    func setupUI(){
        
        //Style
        globalView.setBackgroundToView()
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.customNavBar()
        self.title = "Forgot Password"

        setBackBtn()
        txtEmail.delegate = self
        //txtEmail.becomeFirstResponder()
        txtEmail.textType = .emailAddress
        btnSubmit.backgroundColor = Color.buttonBG
    }
}

extension ForgotPasswardVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//Mark: - Extension For Validation
extension ForgotPasswardVC{
    //Mark: - Validate Function for checking fileds proper data.
    func validateData() -> Bool {
        guard !(txtEmail.isEmpty) else{
            displayMyAlertMessage(userMessage: "Please enter email address")
            return false
        }
        guard (txtEmail.hasValidEmail) else{
            displayMyAlertMessage(userMessage: "Please enter valid email address")
            return false
        }
               
        return true
    }
}
