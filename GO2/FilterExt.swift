//
//  FilterExt.swift
//  GO2
//
//  Created by NC2-46 on 16/09/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import GooglePlaces
import CoreLocation
import IQKeyboardManagerSwift

extension FilterVC: SelectedAddressDelegete{
    func getSelectedAddress(pickAddress: GMSPlace) {
        rentalAddressTF.textColor = Color.textfiledTextColor
        rentalAddressTF.text = pickAddress.formattedAddress
        selectedCorrdinate = pickAddress.coordinate
        isRentalAddressSelected = true
    }
    
    
    func setupUI() {
        TinyToast.shared.dismissAll()
        setBackBtn()
        self.title = navigationTitle
        //Style
        navigationController?.customNavBar()

        globalView.setBackgroundWithTopBarToView()
        
        headerBar.backgroundColor = Color.SubHeaderColor
        searchBtn.backgroundColor = Color.buttonBG
        labelBarTitle.setTextHeader()
        labelRentalDate.setTextFormGrey()
        labelVehicleType.setTextFormGrey()
        labelRideType.setTextFormGrey()
        labelRentalCountry.setTextFormGrey()
        labelRentalState.setTextFormGrey()
        labelRentalCity.setTextFormGrey()
        labelReturnAddress.setTextFormGrey()
        labelReturnDate.setTextFormGrey()
        
        //Set Toolbar Configuration
        rentalDateTF.accessibilityIdentifier = "rentalDateTF"
        rentalVehicleCategoryTF.accessibilityIdentifier = "rentalVehicleTypeTF"
        rentalRideTypeTF.accessibilityIdentifier = "rentalRideTypeTF"
        rentalCountryTF.accessibilityIdentifier = "rentalCountryTF"
        rentalStateTF.accessibilityIdentifier = "rentalStateTF"
        rentalCityTF.accessibilityIdentifier = "rentalCityTF"
        rentalAddressTF.accessibilityIdentifier = "rentalAddressTF"
        returnDateTF.accessibilityIdentifier = "returnDateTF"
        
        rentalDatePicker.frame = CGRect(x: 0, y: self.view.frame.height - (self.view.frame.height - 70), width: self.view.frame.width, height: 70)
        returnDatePicker.frame = CGRect(x: 0, y: self.view.frame.height - (self.view.frame.height - 70), width: self.view.frame.width, height: 70)
        
        //Set Date/Time Formatters
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        timeFormatter.dateFormat = "h:mm a"
        timeFormatter.amSymbol = "AM"
        timeFormatter.pmSymbol = "PM"
        
        rentalDateTF.delegate = self
        returnDateTF.delegate = self
        rentalVehicleCategoryTF.delegate = self
        rentalRideTypeTF.delegate = self
        rentalCountryTF.delegate = self
        rentalStateTF.delegate = self
        rentalCityTF.delegate = self
        rentalAddressTF.delegate = self
        
        //Set Pickers Configuration
        vehicleTypePickerView.delegate = self
        vehicleTypePickerView.dataSource = self
        vehicleTypePickerView.accessibilityLabel = vehicleTypePickerViewIdentifier
        rentalVehicleCategoryTF.inputView = vehicleTypePickerView
        
        rideTypePickerView.delegate = self
        rideTypePickerView.dataSource = self
        rideTypePickerView.accessibilityLabel = rideTypePickerViewIdentifier
        rentalRideTypeTF.inputView = rideTypePickerView
        
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        countryPickerView.accessibilityLabel = countryPickerViewIdentifier
        rentalCountryTF.inputView = countryPickerView
        
        statePickerView.delegate = self
        statePickerView.dataSource = self
        statePickerView.accessibilityLabel = statePickerViewIdentifier
        rentalStateTF.inputView = statePickerView
        
        cityPickerView.delegate = self
        cityPickerView.dataSource = self
        cityPickerView.accessibilityLabel = cityPickerViewIdentifier
        rentalCityTF.inputView = cityPickerView
        
        //Set DatePickers
        rentalDatePicker.datePickerMode = .date
        returnDatePicker.datePickerMode = .date
        
        let date = Date()
        rentalDatePicker.minimumDate = date
        returnDatePicker.minimumDate = date
        
        rentalDatePicker.addTarget(self, action: #selector(dateRentalChanged), for: UIControlEvents.valueChanged)
        returnDatePicker.addTarget(self, action: #selector(dateReturnChanged), for: UIControlEvents.valueChanged)
        
        rentalDateTF.inputView = rentalDatePicker
        returnDateTF.inputView = returnDatePicker
        
        stateView = cityStateStack.arrangedSubviews[0]
        cityView = cityStateStack.arrangedSubviews[1]
        
//        stateView.isHidden = true
//        cityView.isHidden = true
        
//        self.arrTypeCars = CoreDBManager.sharedDatabase.getVehicleTypes()
//        self.arrRideType = CoreDBManager.sharedDatabase.getRideTypes()
        self.arrCountrys = CoreDBManager.sharedDatabase.getAllCountry()
        self.arrVehicleCategory = CoreDBManager.sharedDatabase.getVehicleCategories()
        getCarType(catId: arrVehicleCategory[0].id)
        if arrVehicleCategory.count > 0 {
            self.rentalVehicleCategoryTF.text = arrVehicleCategory[0].name!
            self.isRentalVehicleTypeSelected = true
        }
        if arrRideType.count > 0 {
            self.rentalRideTypeTF.text = arrRideType[0].name!
            self.isRentalRideTypeSelected = true
        }
        
        self.arrCountrys = AppDel.arrCountrys
        self.arrStates = AppDel.arrStates
        self.arrCities = AppDel.arrCities
        
        if AppDel.country != "" && AppDel.country != nil{
            rentalCountryTF.text = AppDel.country
            if AppDel.country != "" && AppDel.country != nil{
                if let currentCountry:CountriesModel = arrCountrys.filter({$0.name == AppDel.country}).first{
                    if let index = self.arrCountrys.firstIndex(where:{$0.name == currentCountry.name}){
                        countryPickerView.selectRow(index, inComponent: 0, animated: true)
                        isRentalCountrySelected = true
                        self.arrStates = CoreDBManager.sharedDatabase.getStatesFromId(countryId: arrCountrys[index].id ?? 0)
                    }
                }
            }
        }
        if AppDel.state != "" && AppDel.state != nil{
            rentalStateTF.text = AppDel.state
            if AppDel.state != "" && AppDel.state != nil{
                if let currentState:StatesModel = arrStates.filter({$0.name == AppDel.state}).first{
                    if let index = arrStates.firstIndex(where:{$0.name == currentState.name}){
                        statePickerView.selectRow(index, inComponent: 0, animated: true)
                        isRentalStateSelected = true
                        self.arrCities = CoreDBManager.sharedDatabase.getCitiesFromId(stateId: arrStates[index].id ?? 0)
                    }
                }
            }
        }
        
        if AppDel.city != "" && AppDel.city != nil{
            rentalCityTF.text = AppDel.city
            if AppDel.city != "" && AppDel.city != nil{
                if let currentCity:CitiesModel = arrCities.filter({$0.name == AppDel.city}).first{
                    if let index = arrCities.firstIndex(where:{$0.name == currentCity.name}){
                        isRentalCitySelected = true
                        cityPickerView.selectRow(index, inComponent: 0, animated: true)
                    }
                }
            }
        }
    }
        
    @objc func dateRentalChanged(){
        rentalDateTF.text = dateFormatter.string(from: rentalDatePicker.date)
        returnDatePicker.minimumDate = rentalDatePicker.date
    }
    
    //MARK :- GetAllCarTypes
    func getCarType(catId:Int16){
        BookingCarApi.sharedInstance.allCarTypesRequest(categoryId: catId) { (carTypes) in
            if carTypes.count > 0{
                self.arrTypeCars = carTypes
                self.rentalRideTypeTF.text = carTypes[0].name
                self.selectedRideType = "\(carTypes[0].id ?? 0)"
            }
        }
    }
    
    @objc func dateReturnChanged(){
        returnDateTF.text = dateFormatter.string(from: returnDatePicker.date)
    }

}


extension FilterVC : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.accessibilityLabel == countryPickerViewIdentifier {
            return arrCountrys.count
        }
        else if pickerView.accessibilityLabel == statePickerViewIdentifier {
            return arrStates.count
        }
        else if pickerView.accessibilityLabel == cityPickerViewIdentifier {
            return arrCities.count
        }
        else if pickerView.accessibilityLabel == vehicleTypePickerViewIdentifier{
            return arrVehicleCategory.count
        }
        else if pickerView.accessibilityLabel == rideTypePickerViewIdentifier{
            return arrTypeCars.count
        }
        else {
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.accessibilityLabel == countryPickerViewIdentifier {
//            stateView.isHidden = true
//            cityView.isHidden = true
            return arrCountrys[row].name
        }
        else if pickerView.accessibilityLabel == cityPickerViewIdentifier {
            return arrCities[row].name
        }
        else if pickerView.accessibilityLabel == statePickerViewIdentifier {
            return arrStates[row].name
        }
        else if pickerView.accessibilityLabel == vehicleTypePickerViewIdentifier {
            return arrVehicleCategory[row].name
        }
        else if pickerView.accessibilityLabel == rideTypePickerViewIdentifier {
            return arrTypeCars[row].name!.replacingOccurrences(of: "_", with: "")
        }
        else {
            return ""
        }
        //return ""
    }
}

extension FilterVC: UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        let vc = loadViewController(StoryBoardName: "Location", VCIdentifer: "AddressPickerVC") as! AddressPickerVC
        vc.delegateSelectedAddress = self
        
        self.navigationController?.pushViewController(vc, animated: true)
        return false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}


extension FilterVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let id = textField.accessibilityIdentifier
        switch id {
        case rentalCountryTF.accessibilityIdentifier!?,rentalStateTF.accessibilityIdentifier!?,rentalCityTF.accessibilityIdentifier!?:
            IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
            break
        default:
            IQKeyboardManager.shared.previousNextDisplayMode = .default
            break
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let id = textField.accessibilityIdentifier
        
        switch id {
        case returnDateTF.accessibilityIdentifier!?:
            if !isRentalDateSelected {
                displayMyAlertMessage(userMessage: "Please select Rental date")
                return false
            }
            return true
        default:
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let id = textField.accessibilityIdentifier
        
        switch id {
        case rentalDateTF.accessibilityIdentifier!? :
            rentalDateTF.text = dateFormatter.string(from: rentalDatePicker.date)
            returnDatePicker.minimumDate = rentalDatePicker.date
            self.view.endEditing(true)
            isRentalDateSelected = true
            returnDateTF.text = ""
            isReturnDateSelected = false
            break
        case returnDateTF.accessibilityIdentifier!?:
            returnDateTF.text = dateFormatter.string(from: returnDatePicker.date)
            self.view.endEditing(true)
            isReturnDateSelected = true
            break
        case rentalVehicleCategoryTF.accessibilityIdentifier!?:
            if arrVehicleCategory.count > 0 {
                rentalVehicleCategoryTF.text = arrVehicleCategory[vehicleTypePickerView.selectedRow(inComponent: 0)].name
                self.view.endEditing(true)
                isRentalVehicleTypeSelected = true
                getCarType(catId: arrVehicleCategory[vehicleTypePickerView.selectedRow(inComponent: 0)].id)
            }
            break
        case rentalRideTypeTF.accessibilityIdentifier!?:
            if arrTypeCars.count > 0 {
                rentalRideTypeTF.text = arrTypeCars[rideTypePickerView.selectedRow(inComponent: 0)].name!
                self.view.endEditing(true)
                isRentalRideTypeSelected = true
                selectedRideType = "\(arrTypeCars[rideTypePickerView.selectedRow(inComponent: 0)].id ?? 0)"
            }
            break
        case rentalCountryTF.accessibilityIdentifier!?:
            if arrCountrys.count > 0 {
                let id : Int = arrCountrys[countryPickerView.selectedRow(inComponent: 0)].id ?? 0
                let name : String = arrCountrys[countryPickerView.selectedRow(inComponent: 0)].name!
                rentalCountryTF.text = name
                self.view.endEditing(true)
                arrStates.removeAll()
                arrCities.removeAll()
                rentalStateTF.text = ""
                rentalCityTF.text = ""
                
//                self.arrStates = CoreDBManager.sharedDatabase.getAllStateRelatedByCountry(countryID: id)
                self.arrStates = CoreDBManager.sharedDatabase.getStatesFromId(countryId: id)
                statePickerView.reloadAllComponents()
                isRentalCountrySelected = true
//                stateView.isHidden = false
            }
            
            break
        case rentalStateTF.accessibilityIdentifier!?:
            if arrStates.count > 0 {
                let id : Int = arrStates[statePickerView.selectedRow(inComponent: 0)].id ?? 0
                
                let name : String = arrStates[statePickerView.selectedRow(inComponent: 0)].name!
                rentalStateTF.text = name
                self.view.endEditing(true)
                arrCities.removeAll()
                rentalCityTF.text = ""
                
//                self.arrCities = CoreDBManager.sharedDatabase.getAllCityRelatedByState(stateID: id)
                self.arrCities = CoreDBManager.sharedDatabase.getCitiesFromId(stateId: id)
                statePickerView.reloadAllComponents()
                isRentalStateSelected = true
//                cityView.isHidden = false
            }
            break
        case rentalCityTF.accessibilityIdentifier!?:
            if arrCities.count > 0 {
                rentalCityTF.text = arrCities[cityPickerView.selectedRow(inComponent: 0)].name
                self.view.endEditing(true)
                isRentalCitySelected = true
            }
            break
        default:
            self.view.endEditing(true)
            break
        }
    }
}

//Mark: - Extension For Validation
extension FilterVC {
    //Mark: - Validate Function for checking fileds proper data.
    func validateData() -> Bool {
        self.view.endEditing(true)
        guard (isRentalDateSelected) else{
            displayMyAlertMessage(userMessage: "Please Select a Rental Date.")
            return false
        }
//        guard (isRentalVehicleTypeSelected) else{
//            displayMyAlertMessage(userMessage: "Please Select a Rental Vehicle Type.")
//            return false
//        }
//        guard (isRentalRideTypeSelected) else{
//            displayMyAlertMessage(userMessage: "Please Select a Rental Ride Type.")
//            return false
//        }
//        guard (isRentalCountrySelected) else{
//            displayMyAlertMessage(userMessage: "Please Select a Country.")
//            return false
//        }
        
//        guard (isRentalStateSelected) && (!stateView.isHidden) else{
        guard (!stateView.isHidden) else{
            displayMyAlertMessage(userMessage: "Please Select a State.")
            return false
        }
        
//        guard (isRentalCitySelected) && (!cityView.isHidden) else{
        guard (!cityView.isHidden) else{
            displayMyAlertMessage(userMessage: "Please Select a City.")
            return false
        }
        
        guard (isRentalAddressSelected) else{
            displayMyAlertMessage(userMessage: "Please Select/Pick an Address.")
            return false
        }
        
        guard (isReturnDateSelected) else{
            displayMyAlertMessage(userMessage: "Please Select a Return Date.")
            return false
        }
        
        return true
    }
}

