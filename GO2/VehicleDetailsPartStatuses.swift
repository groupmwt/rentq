//
//  VehicleDetailsPartStatuses.swift
//  GO2
//
//  Created by C115 on 10/11/21.
//  Copyright © 2021 Juan Cardozo. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class VehicleDetailsPartStatuses: NSCoding,JSONable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let name = "name"
        static let status = "status"
        static let vehicleId = "vehicle_id"
        static let vehiclePartId = "vehicle_part_id"
        static let createdAt = "created_at"
        static let updatedAt = "updated_at"
        static let vehiclePart = "vehicle_part"
        static let vehicleType = "vehicle_type"
        static let vehicleTypeId = "vehicle_type_id"
    }
    
    // MARK: Properties
    public var id: Int?
    public var name: String?
    public var status: Int?
    public var vehicleId: Int?
    public var vehiclePartId: Int?
    public var createdAt: String?
    public var updatedAt: String?
    public var vehiclePart: String?
    public var vehicleType: String?
    public var vehicleTypeId: Int?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        id = json[SerializationKeys.id].int
        name = json[SerializationKeys.name].string
        status = json[SerializationKeys.status].int
        vehicleId = json[SerializationKeys.vehicleId].int
        vehiclePartId = json[SerializationKeys.vehiclePartId].int
        createdAt = json[SerializationKeys.createdAt].string
        updatedAt = json[SerializationKeys.updatedAt].string
        vehiclePart = json[SerializationKeys.vehiclePart].string
        vehicleType = json[SerializationKeys.vehicleType].string
        vehicleTypeId = json[SerializationKeys.vehicleTypeId].int
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = vehicleId { dictionary[SerializationKeys.vehicleId] = value }
        if let value = vehiclePartId { dictionary[SerializationKeys.vehiclePartId] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = vehiclePart { dictionary[SerializationKeys.vehiclePart] = value }
        if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
        if let value = vehicleTypeId { dictionary[SerializationKeys.vehicleTypeId] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? Int
        self.vehicleId = aDecoder.decodeObject(forKey: SerializationKeys.vehicleId) as? Int
        self.vehiclePartId = aDecoder.decodeObject(forKey: SerializationKeys.vehiclePartId) as? Int
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.vehiclePart = aDecoder.decodeObject(forKey: SerializationKeys.vehiclePart) as? String
        self.vehicleType = aDecoder.decodeObject(forKey: SerializationKeys.vehicleType) as? String
        self.vehicleTypeId = aDecoder.decodeObject(forKey: SerializationKeys.vehicleTypeId) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(status, forKey: SerializationKeys.status)
        aCoder.encode(vehicleId, forKey: SerializationKeys.vehicleId)
        aCoder.encode(vehiclePartId, forKey: SerializationKeys.vehiclePartId)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(vehiclePart, forKey: SerializationKeys.vehiclePart)
        aCoder.encode(vehicleType, forKey: SerializationKeys.vehicleType)
        aCoder.encode(vehicleTypeId, forKey: SerializationKeys.vehicleTypeId)
    }
    
}
