//
//  CheckAvailabilityExt.swift
//  GO2
//
//  Created by NC2-46 on 24/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
import UIKit
import FSCalendar


extension CheckAvailabilityVC {
    
    func setupUI() {
        
        //Style
        navigationController?.customNavBar()
        //navigationItem.customTitle()
        self.title = navigationTitle
        setBackBtn()
        globalView.setBackgroundWithTopBarToView()
        calenderHeader.backgroundColor = Color.SubHeaderColor
        carCalendar.allowsSelection = true
        carCalendar.allowsMultipleSelection = false
        
        carCalendar.formatter.dateFormat = "yyyy-MM-dd"
        carCalendar.today = nil;
        carCalendar.clipsToBounds = false
        
        carCalendar.delegate = self
        carCalendar.dataSource = self
        formatter.dateFormat = "yyyy-MM-dd"
        carCalendar.placeholderType = .none
        // Set date format
        let calendar = NSCalendar.init(calendarIdentifier: NSCalendar.Identifier.gregorian)
        
        let currentMonth = (calendar?.component(NSCalendar.Unit.month, from: NSDate() as Date))!
        let currentYear = (calendar?.component(NSCalendar.Unit.year, from: NSDate() as Date))!
        
        checkAvailability(year : "\(currentYear)",month: "\(currentMonth)")
        
        
    }
    
    func moveCurrentPage(moveUp: Bool) {
        
        let calendar = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = moveUp ? 1 : -1
        
        self.currentPage = calendar.date(byAdding: dateComponents, to: carCalendar.currentPage)
        
        self.carCalendar.setCurrentPage(self.currentPage!, animated: true)
        currentDateTitle.text = carCalendar.currentPage.getMonthName()
        
    }
    
    func checkAvailability(year : String,month: String) {
        if self.isAnyApiCalled == false {
            self.moveCurrentPage(moveUp: false)
        }
    CheckAvailabilityApi.sharedInstance.get_offers_calendarRequest(myView: self.view, carID: carID, year: year, month: month) { (data) in
            self.arrAvailableDate.removeAll()
            self.arrReserverDate.removeAll()
            if  data != nil {
                self.arrCheckAvailability = data!
                for availability in data! {
                    
                    if availability.activeInSystem! {
                        self.arrAvailableDate.append(availability.offerDate!)
                    }
                    else {
                        self.arrReserverDate.append(availability.offerDate!)
                    }
                }
                
                self.carCalendar.reloadData()
                if self.isAnyApiCalled == false{
                    self.carCalendar.currentPage = Date()
                }
                
                self.view.stopIndicator(indicator: indicator, container: container)
            }
            else {
                if self.isAnyApiCalled == false{
                    self.carCalendar.currentPage = Date()
                }
            }
    
        }
    }
    
}

extension CheckAvailabilityVC: FSCalendarDataSource, FSCalendarDelegate,FSCalendarDelegateAppearance {
    
    
    func minimumDate(for calendar: FSCalendar) -> Date {

        if self.isAnyApiCalled == false {
            let date = self.calendar?.date(byAdding: .month, value: -1, to: Date())
        
            return date!
        }
        else {
             return Date()
        }
    }
    
    // FSCalendarDelegate
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        self.offerID = ""
        selectedDate = ""
        selectedDate = formatter.string(from: date)

        for availability in self.arrCheckAvailability {
            if availability.offerDate != nil {
                if selectedDate! == availability.offerDate! {
                    self.price = "\(availability.price!)"
                    self.offerID = "\(availability.id!)"
                    break
                }
            }
        }
        if self.offerID != "" {
            
            self.displayAlertWithAction(userMessage: "Select Appropriate action", userTitle: selectedDate, alertStyle: .actionSheet, arrActionName: ["Change Price","Delete Availability","Cancel"], arrActionStyle: [.default,.destructive,.cancel], completion: { (btnAction) in
                switch btnAction {
                case 0:
                     let popOverVC = UIStoryboard(name: "VehicleList", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                     
                     popOverVC.titleText = "Change Rent Price"
                       
                    popOverVC.titleMessage = "How much price you want to set for this date : \(self.selectedDate!) ?"
                     
                    popOverVC.isPriceChangeApiCall = true
                     popOverVC.carID = self.carID
                     popOverVC.selectedDate = self.selectedDate
                     popOverVC.offerID = self.offerID
                     popOverVC.price = self.price
                     popOverVC.superView1 = self
                     self.addChildViewController(popOverVC)
                     
                     popOverVC.view.frame = self.view.frame
                     self.view.addSubview(popOverVC.view)
                     popOverVC.didMove(toParentViewController: self)
                     
                    break
                case 1:
                    
                    self.displayAlertWithAction(userMessage: "Are you sure you want to delete availability for this vehicle?", userTitle: "Delete Vehicle Availability", alertStyle: .alert, arrActionName: ["Delete","Cancel"], arrActionStyle: [.destructive,.cancel], completion: { (btnAction) in
                        switch btnAction {
                        case 0:
                            CheckAvailabilityApi.sharedInstance.removeOffersRequest(myView: self.view, carID: self.carID, startDate: self.selectedDate, endDate: self.selectedDate, completion: { (hasError) in
                                if !hasError {
                                    let date = self.carCalendar.currentPage
                                    let mycalendar = NSCalendar.current
                                    let components = mycalendar.dateComponents([.day, .month, .year], from: date)
                                    let year =  components.year
                                    let month = components.month
                                    
                                    self.checkAvailability(year : "\(year!)",month: "\(month!)")
                                    displayMyAlertMessage(userMessage: "Availability deleted sucessfully.")
                                }
                            })
                            break
                        default :
                            break
                        }
                        })
                        break
                default:
                    break
                }
            })
            
           
        }
        else {
            
            self.displayAlertWithAction(userMessage: "Select Appropriate action", userTitle: selectedDate, alertStyle: .actionSheet, arrActionName: ["Add Single Availability","Add Multiple Availability","Cancel"], arrActionStyle: [.default,.default,.cancel], completion: { (btnAction) in
                switch btnAction {
                case 0:
                    
                    let popOverVC = UIStoryboard(name: "VehicleList", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertVC
                    
                    popOverVC.titleText = "Add Availability"
                    
                    popOverVC.titleMessage = "For date \(self.selectedDate!), How much rent price you want to set ?"
                    popOverVC.isPriceChangeApiCall = false
                    popOverVC.carID = self.carID
                    popOverVC.selectedDate = self.selectedDate!
                    //popOverVC.price = "0"
                    popOverVC.superView1 = self
                    self.addChildViewController(popOverVC)
                    
                    popOverVC.view.frame = self.view.frame
                    self.view.addSubview(popOverVC.view)
                    popOverVC.didMove(toParentViewController: self)
                    
                    
                    break
                case 1:
                    let popOverVC = UIStoryboard(name: "VehicleList", bundle: nil).instantiateViewController(withIdentifier: "MultipleSelectionVC") as! MultipleSelectionVC
                    
                    popOverVC.carID = self.carID
                    popOverVC.minimumDate = date
                    popOverVC.superView1 = self
                    self.addChildViewController(popOverVC)
                    
                    popOverVC.view.frame = self.view.frame
                    self.view.addSubview(popOverVC.view)
                    popOverVC.didMove(toParentViewController: self)
                    break
                default :
                    break
                }
                })
        }
        
    }
    
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
       
    }
   
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        
        let myString = formatter.string(from: date)
        if arrReserverDate.contains(myString) {
            displayMyAlertMessage(userMessage: "On this date Vehicle is already reserved, You can not perform any action")
            return false
        }
        return true
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.isAnyApiCalled = true
        let date = calendar.currentPage
        let mycalendar = NSCalendar.current
        let components = mycalendar.dateComponents([.day, .month, .year], from: date)
        let year =  components.year
        let month = components.month
        currentDateTitle.text = calendar.currentPage.getMonthName()
        checkAvailability(year : "\(year!)",month: "\(month!)")
    }

    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        let myString = formatter.string(from: date)
        let backgroundImage = UIImageView()
        backgroundImage.frame = CGRect(x: 0, y: 0, width: cell.titleLabel.bounds.width, height: cell.titleLabel.bounds.height)
        backgroundImage.tag = 999
        cell.clipsToBounds = true
        
        for ce in cell.subviews {
            if ce.tag == 999 {
                ce.removeFromSuperview()
            }
        }
        if arrReserverDate.contains(myString) {
            backgroundImage.image = #imageLiteral(resourceName: "reserved-active")
            backgroundImage.contentMode = .scaleAspectFit
            cell.insertSubview(backgroundImage, at: 0)

        }
        if arrAvailableDate.contains(myString) {
            backgroundImage.image = #imageLiteral(resourceName: "active")
            backgroundImage.contentMode = .scaleAspectFit
            cell.insertSubview(backgroundImage, at: 0)
           
        }

    }
    

    func updateOffer(carID:String, offerID:String,price:String){
        CheckAvailabilityApi.sharedInstance.updateOffersRequest(myView: self.view, carID: carID, offerID: offerID, price: price, completion: { (data) in
            
            for i in 0..<self.arrCheckAvailability.count {
                if data.id! == self.arrCheckAvailability[i].id!{
                    self.arrCheckAvailability[i] = data
                    displayMyAlertMessage(userMessage: "Price is successfully changed for \(self.arrCheckAvailability[i].offerDate ?? "")")
                    break
                }
            }

            self.view.stopIndicator(indicator: indicator, container: container)
        })
    }
    
    func addOffer(carID:String, start_date:String,end_date:String,price:String){
        CheckAvailabilityApi.sharedInstance.addOffersRequest(myView: self.view, carID: carID, startDate: start_date, endDate: end_date, price: price, completion: { (hasError) in
            if !hasError {
                let date = self.carCalendar.currentPage
                let mycalendar = NSCalendar.current
                let components = mycalendar.dateComponents([.day, .month, .year], from: date)
                let year =  components.year
                let month = components.month
                
                self.checkAvailability(year : "\(year!)",month: "\(month!)")
                displayMyAlertMessage(userMessage: "Availability added sucessfully.")
            }
            self.view.stopIndicator(indicator: indicator, container: container)
        })
    }
    
}
