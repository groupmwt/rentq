//
//  VehicleDetailsData.swift
//
//  Created by NC2-46 on 23/08/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class VehicleDetailsData: NSCoding,JSONable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let plate = "plate"
        static let model = "model"
        static let brand = "brand"
        static let userId = "user_id"
        static let vehicleTypeId = "vehicle_type_id"
        static let address = "address"
        static let lat = "lat"
        static let lng = "lng"
        static let cityId = "city_id"
        static let softDeleted = "soft_deleted"
        static let createdAt = "created_at"
        static let updatedAt = "updated_at"
        static let insuranceId = "insurance_id"
        static let insuranceCompany = "insurance_company"
        static let insuranceExpiryDate = "insurance_expiry_date"
        static let chassisNumber = "chassis_number"
        static let registrationNumber = "registration_number"
        static let numberOfSeats = "number_of_seats"
        static let typeOfTraction = "type_of_traction"
        static let images = "images"
        static let vehicleType = "vehicle_type"
        static let vehicleCategoryId = "vehicle_category_id"
        static let vehicleCategoryType = "vehicle_category_type"
        static let stateId = "state_id"
        static let countryId = "country_id"
        static let city = "city"
        static let state = "state"
        static let country = "country"
        static let vehiclePartStatuses = "vehicle_part_statuses"
        static let offers = "offers"
        static let end = "end"
        static let start = "start"
    }
    
    // MARK: Properties
    public var id: Int?
    public var plate: String?
    public var model: String?
    public var brand: String?
    public let userId: Int?
    public let vehicleTypeId: Int?
    public var address: String?
    public var lat: String?
    public var lng: String?
    public var cityId: Int?
    public var softDeleted: Bool? = false
    public let createdAt: String?
    public let updatedAt: String?
    public let insuranceId: Int?
    public let insuranceCompany: String?
    public let insuranceExpiryDate: String?
    public let chassisNumber: String?
    public let registrationNumber: String?
    public let numberOfSeats: Int?
    public let typeOfTraction: String?
    public var images: [VehicleDetailsImage]?
    public let vehicleType: String?
    public let vehicleCategoryId: Int?
    public let vehicleCategoryType: String?
    public var stateId: Int?
    public var countryId: Int?
    public var city: String?
    public var state: String?
    public var country: String?
    public var vehiclePartStatuses: [VehicleDetailsPartStatuses]?
    public var offers: [SearchVehicleOffers]?
    public var end: String?
    public var start: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        id = json[SerializationKeys.id].int
        plate = json[SerializationKeys.plate].string
        model = json[SerializationKeys.model].string
        brand = json[SerializationKeys.brand].string
        userId = json[SerializationKeys.userId].int
        vehicleTypeId = json[SerializationKeys.vehicleTypeId].int
        address = json[SerializationKeys.address].string
        lat = json[SerializationKeys.lat].string
        lng = json[SerializationKeys.lng].string
        cityId = json[SerializationKeys.cityId].int
        softDeleted = json[SerializationKeys.softDeleted].boolValue
        createdAt = json[SerializationKeys.createdAt].string
        updatedAt = json[SerializationKeys.updatedAt].string
        insuranceId = json[SerializationKeys.insuranceId].int
        insuranceCompany = json[SerializationKeys.insuranceCompany].string
        insuranceExpiryDate = json[SerializationKeys.insuranceExpiryDate].string
        chassisNumber = json[SerializationKeys.chassisNumber].string
        registrationNumber = json[SerializationKeys.registrationNumber].string
        numberOfSeats = json[SerializationKeys.numberOfSeats].int
        typeOfTraction = json[SerializationKeys.typeOfTraction].string
        if let items = json[SerializationKeys.images].array { images = items.map { VehicleDetailsImage(json: $0) } }
        vehicleType = json[SerializationKeys.vehicleType].string
        vehicleCategoryId = json[SerializationKeys.vehicleCategoryId].int
        vehicleCategoryType = json[SerializationKeys.vehicleCategoryType].string
        stateId = json[SerializationKeys.stateId].int
        countryId = json[SerializationKeys.countryId].int
        city = json[SerializationKeys.city].string
        state = json[SerializationKeys.state].string
        country = json[SerializationKeys.country].string
        end = json[SerializationKeys.end].string
          start = json[SerializationKeys.start].string
        if let items = json[SerializationKeys.offers].array { offers = items.map { SearchVehicleOffers(json: $0) } }

        if let items = json[SerializationKeys.vehiclePartStatuses].array { vehiclePartStatuses = items.map { VehicleDetailsPartStatuses(json: $0) } }
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = plate { dictionary[SerializationKeys.plate] = value }
        if let value = model { dictionary[SerializationKeys.model] = value }
        if let value = brand { dictionary[SerializationKeys.brand] = value }
        if let value = userId { dictionary[SerializationKeys.userId] = value }
        if let value = vehicleTypeId { dictionary[SerializationKeys.vehicleTypeId] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = lat { dictionary[SerializationKeys.lat] = value }
        if let value = lng { dictionary[SerializationKeys.lng] = value }
        if let value = cityId { dictionary[SerializationKeys.cityId] = value }
        dictionary[SerializationKeys.softDeleted] = softDeleted
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = insuranceId { dictionary[SerializationKeys.insuranceId] = value }
        if let value = insuranceCompany { dictionary[SerializationKeys.insuranceCompany] = value }
        if let value = insuranceExpiryDate { dictionary[SerializationKeys.insuranceExpiryDate] = value }
        if let value = chassisNumber { dictionary[SerializationKeys.chassisNumber] = value }
        if let value = registrationNumber { dictionary[SerializationKeys.registrationNumber] = value }
        if let value = numberOfSeats { dictionary[SerializationKeys.numberOfSeats] = value }
        if let value = typeOfTraction { dictionary[SerializationKeys.typeOfTraction] = value }
        if let value = images { dictionary[SerializationKeys.images] = value.map { $0.dictionaryRepresentation() } }
        if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
        if let value = vehicleCategoryId { dictionary[SerializationKeys.vehicleCategoryId] = value }
        if let value = vehicleCategoryType { dictionary[SerializationKeys.vehicleCategoryType] = value }
        if let value = stateId { dictionary[SerializationKeys.stateId] = value }
        if let value = countryId { dictionary[SerializationKeys.countryId] = value }
        if let value = city { dictionary[SerializationKeys.city] = value }
        if let value = state { dictionary[SerializationKeys.state] = value }
        if let value = country { dictionary[SerializationKeys.country] = value }
        if let value = end { dictionary[SerializationKeys.end] = value }
          if let value = start { dictionary[SerializationKeys.start] = value }
        if let value = offers { dictionary[SerializationKeys.offers] = value.map { $0.dictionaryRepresentation() } }
        if let value = vehiclePartStatuses { dictionary[SerializationKeys.vehiclePartStatuses] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.plate = aDecoder.decodeObject(forKey: SerializationKeys.plate) as? String
        self.model = aDecoder.decodeObject(forKey: SerializationKeys.model) as? String
        self.brand = aDecoder.decodeObject(forKey: SerializationKeys.brand) as? String
        self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? Int
        self.vehicleTypeId = aDecoder.decodeObject(forKey: SerializationKeys.vehicleTypeId) as? Int
        self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
        self.lat = aDecoder.decodeObject(forKey: SerializationKeys.lat) as? String
        self.lng = aDecoder.decodeObject(forKey: SerializationKeys.lng) as? String
        self.cityId = aDecoder.decodeObject(forKey: SerializationKeys.cityId) as? Int
        self.softDeleted = aDecoder.decodeBool(forKey: SerializationKeys.softDeleted)
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.insuranceId = aDecoder.decodeObject(forKey: SerializationKeys.insuranceId) as? Int
        self.insuranceCompany = aDecoder.decodeObject(forKey: SerializationKeys.insuranceCompany) as? String
        self.insuranceExpiryDate = aDecoder.decodeObject(forKey: SerializationKeys.insuranceExpiryDate) as? String
        self.chassisNumber = aDecoder.decodeObject(forKey: SerializationKeys.chassisNumber) as? String
        self.registrationNumber = aDecoder.decodeObject(forKey: SerializationKeys.registrationNumber) as? String
        self.numberOfSeats = aDecoder.decodeObject(forKey: SerializationKeys.numberOfSeats) as? Int
        self.typeOfTraction = aDecoder.decodeObject(forKey: SerializationKeys.typeOfTraction) as? String
        self.images = aDecoder.decodeObject(forKey: SerializationKeys.images) as? [VehicleDetailsImage]
        self.vehicleType = aDecoder.decodeObject(forKey: SerializationKeys.vehicleType) as? String
        self.vehicleCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.vehicleCategoryId) as? Int
        self.vehicleCategoryType = aDecoder.decodeObject(forKey: SerializationKeys.vehicleCategoryType) as? String
        self.stateId = aDecoder.decodeObject(forKey: SerializationKeys.stateId) as? Int
        self.countryId = aDecoder.decodeObject(forKey: SerializationKeys.countryId) as? Int
        self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as? String
        self.state = aDecoder.decodeObject(forKey: SerializationKeys.state) as? String
        self.country = aDecoder.decodeObject(forKey: SerializationKeys.country) as? String
        self.offers = aDecoder.decodeObject(forKey: SerializationKeys.offers) as? [SearchVehicleOffers]
        self.end = aDecoder.decodeObject(forKey: SerializationKeys.end) as? String
          self.start = aDecoder.decodeObject(forKey: SerializationKeys.start) as? String
        self.vehiclePartStatuses = aDecoder.decodeObject(forKey: SerializationKeys.vehiclePartStatuses) as? [VehicleDetailsPartStatuses]
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(plate, forKey: SerializationKeys.plate)
        aCoder.encode(model, forKey: SerializationKeys.model)
        aCoder.encode(brand, forKey: SerializationKeys.brand)
        aCoder.encode(userId, forKey: SerializationKeys.userId)
        aCoder.encode(vehicleTypeId, forKey: SerializationKeys.vehicleTypeId)
        aCoder.encode(address, forKey: SerializationKeys.address)
        aCoder.encode(lat, forKey: SerializationKeys.lat)
        aCoder.encode(lng, forKey: SerializationKeys.lng)
        aCoder.encode(cityId, forKey: SerializationKeys.cityId)
        aCoder.encode(softDeleted, forKey: SerializationKeys.softDeleted)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(insuranceId, forKey: SerializationKeys.insuranceId)
        aCoder.encode(insuranceCompany, forKey: SerializationKeys.insuranceCompany)
        aCoder.encode(insuranceExpiryDate, forKey: SerializationKeys.insuranceExpiryDate)
        aCoder.encode(chassisNumber, forKey: SerializationKeys.chassisNumber)
        aCoder.encode(registrationNumber, forKey: SerializationKeys.registrationNumber)
        aCoder.encode(numberOfSeats, forKey: SerializationKeys.numberOfSeats)
        aCoder.encode(typeOfTraction, forKey: SerializationKeys.typeOfTraction)
        aCoder.encode(images, forKey: SerializationKeys.images)
        aCoder.encode(vehicleType, forKey: SerializationKeys.vehicleType)
        aCoder.encode(vehicleCategoryId, forKey: SerializationKeys.vehicleCategoryId)
        aCoder.encode(vehicleCategoryType, forKey: SerializationKeys.vehicleCategoryType)
        aCoder.encode(stateId, forKey: SerializationKeys.stateId)
        aCoder.encode(countryId, forKey: SerializationKeys.countryId)
        aCoder.encode(offers, forKey: SerializationKeys.offers)
        aCoder.encode(countryId, forKey: SerializationKeys.countryId)
        aCoder.encode(end, forKey: SerializationKeys.end)
          aCoder.encode(start, forKey: SerializationKeys.start)
        aCoder.encode(city, forKey: SerializationKeys.city)
        aCoder.encode(state, forKey: SerializationKeys.state)
        aCoder.encode(country, forKey: SerializationKeys.country)
    }
    
}
