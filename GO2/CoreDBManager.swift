//`
//  CoreDBManager.swift
//  RGUAssignment14
//
//  Created by NC2-46 on 07/02/18.
//  Copyright © 2018 NC2-46. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class CoreDBManager: NSObject {
    static let sharedDatabase = CoreDBManager()
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.scott.CoreDataTest1" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var applicationSupportDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.scott.CoreDataTest1" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first
        return urls!
    }()
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "GO2")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            let url = self.applicationSupportDirectory.appendingPathComponent("GO2.sqlite")
            NSLog("Database Path: \(url)")
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    //MARK: - Get locations from local json file
    func readJSONFromFile() -> Any?
    {
        var json: Any?
        if let path = Bundle.main.path(forResource: "Locations", ofType: "json") {
            do {
                let fileUrl = URL(fileURLWithPath: path)
                // Getting data from JSON file using the file URL
                let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                json = try? JSONSerialization.jsonObject(with: data)
            } catch {
                // Handle error here
            }
        }
        
        return json as? NSDictionary
        //        return json
    }
    
    func getDataFromFile() -> NSDictionary{
        var locationData = NSDictionary()
        do {
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
            let fileName = "Locations.json"
            if let documentPath = paths.first {
                let filePath = NSMutableString(string: documentPath).appendingPathComponent(fileName)
                let URL = NSURL.fileURL(withPath: filePath)
                
                let fileManager = FileManager.default
                if fileManager.fileExists(atPath: filePath) {
                    print("FILE AVAILABLE")
                    do{
                        let data = try Data(contentsOf: URL)
                        locationData = try! JSONSerialization.jsonObject(with: data) as! NSDictionary
                    }catch{
                        print("error is .........",error.localizedDescription)
                    }
                    
                } else {
                    print("FILE NOT AVAILABLE")
                }
            }
        }
        return locationData
    }
    
    func updateFile(locationData:NSDictionary){
        var error : NSError?
        let jsonData = try! JSONSerialization.data(withJSONObject: locationData, options: .prettyPrinted)
//        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
 
        let fileManager = FileManager.default
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let filePath = documentDirectoryPath.appendingPathComponent("Locations.json")
        if fileManager.fileExists(atPath: filePath){
            do {
//                let data = try PropertyListSerialization.data(fromPropertyList: locationData, format: PropertyListSerialization.PropertyListFormat.binary, options: 0)
                try fileManager.removeItem(atPath: filePath)
                do {
                    let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true)
                    let fileName = "Locations.json"
                    if let documentPath = paths.first {
                        let filePath = NSMutableString(string: documentPath).appendingPathComponent(fileName)
                        let URL = NSURL.fileURL(withPath: filePath)
                        
                        let fileManager = FileManager.default
                        if fileManager.fileExists(atPath: filePath) {
                            print("FILE AVAILABLE")
                        } else {
                            print("FILE NOT AVAILABLE")
                            if #available(iOS 11.0, *) {
                                do{
                                    try jsonData.write(to: URL)
                                    print("file saved")
                                }catch{
                                    print("error is .........",error.localizedDescription)
                                }
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                    }
                }
            }
            catch {
                print("Error in reading json",error.localizedDescription)
            }
        }
    }
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK: - Save Vehicle Category.
    func saveVehicleCategory() {
        
        BookingCarApi.sharedInstance.allCarCategoriesRequest() { (carCategory) in
            self.deleteAllRecords(entityName: ENTITY_VEHICLE_Category)
            
            for type in carCategory {
                
                var carObj : CD_Vehicle_Category! = CD_Vehicle_Category()
                let objContext = self.persistentContainer.viewContext
                carObj = NSEntityDescription.insertNewObject(forEntityName:ENTITY_VEHICLE_Category,into: objContext) as? CD_Vehicle_Category
                carObj.id = type.id ?? 0
                carObj.name = type.name!
                self.saveContext()
            }
        }
        
    }
    
    //MARK: - Save Vehicle Type.
    func saveVehicleType(categoryId:Int16) {
        
        BookingCarApi.sharedInstance.allCarTypesRequest(categoryId: categoryId) { (carType) in
            self.deleteAllRecords(entityName: ENTITY_VEHICLE_TYPE)
            
            for type in carType {
                
                var carObj : CD_Vehicle_Type! = CD_Vehicle_Type()
                let objContext = self.persistentContainer.viewContext
                carObj = NSEntityDescription.insertNewObject(forEntityName:ENTITY_VEHICLE_TYPE,into: objContext) as? CD_Vehicle_Type
                
                carObj.name = type.name!
                self.saveContext()
            }
            
        }
        
    }
    
    //MARK: - Save Ride Type.
    func saveRideType() {
        //        let arrRideType = ["air","ground","sea_going"]
        let arrRideType = ["air","aircraft","airplane","bay boats","bicycle","blimps","bowriders","cabin cruisers", "cuddy cabins","deck boats","dinghies","gliders","ground","helicopter","hot air balloons","motocycle","seagoing"]
        self.deleteAllRecords(entityName: ENTITY_RIDE_TYPE)
        for rideType in arrRideType {
            var rideObj : CD_Ride_Type! = CD_Ride_Type()
            let objContext = self.persistentContainer.viewContext
            rideObj = NSEntityDescription.insertNewObject(forEntityName:ENTITY_RIDE_TYPE,into: objContext) as? CD_Ride_Type
            
            rideObj.name = rideType
            self.saveContext()
        }
        
        //            self.saveLocation()
        
    }
    
    //MARK: - Save Location Details.
    func saveLocation(myView : UIView = UIView()) {
        print("START LOADER *************  ==> saveLocation")
        let vw = AppDel.window?.rootViewController?.presentedViewController?.view ?? UIView()
        vw.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
        BookingCarApi.sharedInstance.getLocationRequest() { (location) in
            let vw = AppDel.window?.rootViewController?.presentedViewController?.view ?? UIView()
            vw.displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
            self.deleteAllRecords(entityName: ENTITY_COUNTRY)
            print("Dataa delete ==> countries")
            if location.countries?.count != 0 {
                
                for location_country in location.countries! {
                    var obj : CD_Country! = CD_Country()
                    let objContext = self.persistentContainer.viewContext
                    obj = NSEntityDescription.insertNewObject(forEntityName:ENTITY_COUNTRY,into: objContext) as? CD_Country
                    obj.id = Int16(location_country.id!)
                    obj.name = location_country.name
                    self.saveContext()
                    print("Dataa STORE ==> countries")
                    
                }
                
                //                self.deleteAllRecords(entityName: ENTITY_STATE)
                print("Dataa delete ==> STATE")
                
                //                if location.states != nil {
                //
                //                    for location_state in location.states! {
                //                        var obj : CD_State! = CD_State()
                //                        let objContext = self.persistentContainer.viewContext
                //                        obj = NSEntityDescription.insertNewObject(forEntityName:ENTITY_STATE,into: objContext) as? CD_State
                //                        obj.id = Int16(location_state.id!)
                //                        obj.name = location_state.name
                //                        obj.country_id = Int16(location_state.countryId ?? 0)
                //                        self.saveContext()
                //                        print("Dataa STORE ==> STATE")
                //
                //                    }
                //
                //                    self.deleteAllRecords(entityName: ENTITY_CITY)
                //                    print("Dataa delete ==> CITY")
                //
                //                    if location.cities != nil {
                //                        for location_city in location.cities! {
                //                            var obj : CD_City! = CD_City()
                //                            let objContext = self.persistentContainer.viewContext
                //                            obj = NSEntityDescription.insertNewObject(forEntityName:ENTITY_CITY,into: objContext) as? CD_City
                //                            obj.id = Int16(location_city.id!)
                //                            obj.name = location_city.name
                //                            obj.state_id = Int16(location_city.stateId!)
                //                            self.saveContext()
                //                            print("Dataa STORE ==> CCITY")
                //                        }
                //                    }
                //                    print("STOP LOADER *************  ==> CCITY")
                //                    let vw = AppDel.window?.rootViewController?.presentedViewController?.view ?? UIView()
                //                    vw.stopIndicator(indicator: indicator, container: container)//displayIndicator(indicator: indicator, container: container, loadingView: loadingView)
                //                }
            }
        }
    }
    
    // fetch All Vehicle Category Details.
    func getVehicleCategories() -> [CD_Vehicle_Category] {
        
        let objContext = self.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest<NSFetchRequestResult>(entityName: ENTITY_VEHICLE_Category)
        
        do{
            let results = try  objContext.fetch(fetchRequest) as! [CD_Vehicle_Category]
            return results
        }
        catch
        {
            print("error executing fetch request: \(error)")
            
        }
        return [CD_Vehicle_Category]()
    }
    
    // fetch All Vehicle Type Details.
    func getVehicleTypes() -> [CD_Vehicle_Type] {
        
        let objContext = self.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest<NSFetchRequestResult>(entityName: ENTITY_VEHICLE_TYPE)
        
        do{
            let results = try  objContext.fetch(fetchRequest) as! [CD_Vehicle_Type]
            return results
        }
        catch
        {
            print("error executing fetch request: \(error)")
            
        }
        return [CD_Vehicle_Type]()
    }
    
    // fetch All Ride Type Details.
    func getRideTypes() -> [CD_Ride_Type] {
        
        let objContext = self.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest<NSFetchRequestResult>(entityName: ENTITY_RIDE_TYPE)
        
        do{
            let results = try  objContext.fetch(fetchRequest) as! [CD_Ride_Type]
            return results
        }
        catch
        {
            print("error executing fetch request: \(error)")
            
        }
        return [CD_Ride_Type]()
    }
    
    func getCurrentLocation() -> (country:String,state:String,city:String){
        return ("","","")
    }
    
    // fetch All CD_Country Details.
    func getAllCountry() -> [CountriesModel] {
//        if let jsonData = UserDefaults.standard.value(forKey: "locationCountries"){
//            var arrCountries = [CountriesModel]()
//            let apiReturn = JSON(jsonData)
//            arrCountries = apiReturn.to(type: CountriesModel.self) as! [CountriesModel]
//            arrCountries = arrCountries.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
//            return arrCountries
//        }else{
//             let jsonData = readJSONFromFile()
            let jsonData = AppDel.locationDict
//            let jsonData = CoreDBManager.sharedDatabase.getDataFromFile()
            let arrCountriesFromJson = (jsonData as! NSDictionary).value(forKey: "countries") as? NSArray ?? NSArray()
            var arrCountries = [CountriesModel]()
            let apiReturn = JSON(arrCountriesFromJson)
            arrCountries = apiReturn.to(type: CountriesModel.self) as! [CountriesModel]
            arrCountries = arrCountries.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
            return arrCountries
//        }
        
        //        for i in arrCountriesFromJson {
        //            let ob = i as! NSDictionary
        //            var obj : CD_Country! = CD_Country()
        //            let objContext = self.persistentContainer.viewContext
        //            obj = NSEntityDescription.insertNewObject(forEntityName:ENTITY_COUNTRY,into: objContext) as? CD_Country
        //            obj.id = Int16(ob["id"] as! String) ?? 0
        //            obj.name = ob["name"] as! String
        //            arrCountries.append(obj)
        //            print("Dataa STORE ==> countries")
        //        }
    }
    
    func getAllState() -> [StatesModel] {
//        if let jsonData = UserDefaults.standard.value(forKey: "locationStates"){
//            var arrStates = [StatesModel]()
//            let apiReturn = JSON(jsonData)
//            arrStates = apiReturn.to(type: StatesModel.self) as! [StatesModel]
//            arrStates = arrStates.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
//            return arrStates
//        }else{
//            let jsonData = readJSONFromFile()
        let jsonData = AppDel.locationDict
//            let jsonData = CoreDBManager.sharedDatabase.getDataFromFile()
            let arrStatesFromJson = (jsonData as! NSDictionary).value(forKey: "states") as? NSArray ?? NSArray()
            var arrStates = [StatesModel]()
            let apiReturn = JSON(arrStatesFromJson)
            arrStates = apiReturn.to(type: StatesModel.self) as! [StatesModel]
            arrStates = arrStates.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
            return arrStates
//        }
//        let jsonData = readJSONFromFile()
        
    }
    
    func getAllCities() -> [CitiesModel] {
//        if let jsonData = UserDefaults.standard.value(forKey: "locationCities"){
//            var arrCities = [CitiesModel]()
//            let apiReturn = JSON(jsonData)
//            arrCities = apiReturn.to(type: CitiesModel.self) as! [CitiesModel]
//            arrCities = arrCities.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
//            return arrCities
//        }else{
//             let jsonData = readJSONFromFile()
        let jsonData = AppDel.locationDict
//            let jsonData = CoreDBManager.sharedDatabase.getDataFromFile()
            let arrCitiesFromJson = (jsonData as! NSDictionary).value(forKey: "cities") as? NSArray ?? NSArray()
            var arrCities = [CitiesModel]()
            let apiReturn = JSON(arrCitiesFromJson)
            arrCities = apiReturn.to(type: CitiesModel.self) as! [CitiesModel]
            arrCities = arrCities.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
            return arrCities
//        }
    }
    
    //    func getAllCountry() -> [CD_Country] {
    //
    //        let objContext = self.persistentContainer.viewContext
    //
    //        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest<NSFetchRequestResult>(entityName: ENTITY_COUNTRY)
    //        do{
    //            let results = try  objContext.fetch(fetchRequest) as! [CD_Country]
    //
    //            return results
    //        }
    //        catch
    //        {
    //            print("error executing fetch request: \(error)")
    //
    //        }
    //        return [CD_Country]()
    //    }
    
    // fetch All CD_State Details.
    func getAllStateRelatedByCountry(countryID: Int) -> [StatesModel] {
//        if let jsonData = UserDefaults.standard.value(forKey: "locationStates"){
//            let apiReturn = JSON(jsonData)
//            let arrCountries = apiReturn.to(type: StatesModel.self) as! [StatesModel]
//            var arrStates = arrCountries.filter { (obj) -> Bool in
//                (obj.countryId ?? 0) == countryID
//            }
//            arrStates = arrStates.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
//            return arrStates
//        }else{
//             let jsonData = readJSONFromFile()
            let jsonData = AppDel.locationDict
//            let jsonData = CoreDBManager.sharedDatabase.getDataFromFile()
            let arrCountriesFromJson = (jsonData as! NSDictionary).value(forKey: "states") as? NSArray ?? NSArray()
            let apiReturn = JSON(arrCountriesFromJson)
            let arrCountries = apiReturn.to(type: StatesModel.self) as! [StatesModel]
            var arrStates = arrCountries.filter { (obj) -> Bool in
                (obj.countryId ?? 0) == countryID
            }
            arrStates = arrStates.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
            return arrStates
//        }
    }
    
    func getAllCityRelatedByState(stateID : Int) -> [CitiesModel] {
//        if let jsonData = UserDefaults.standard.value(forKey: "locationCities"){
//            let apiReturn = JSON(jsonData)
//            let arrCountries = apiReturn.to(type: CitiesModel.self) as! [CitiesModel]
//            var arrStates = arrCountries.filter { (obj) -> Bool in
//                (obj.stateId ?? 0) == stateID
//            }
//            arrStates = arrStates.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
//            return arrStates
//        }else{
//             let jsonData = readJSONFromFile()
            let jsonData = AppDel.locationDict
//            let jsonData = CoreDBManager.sharedDatabase.getDataFromFile()
            let arrCountriesFromJson = (jsonData as! NSDictionary).value(forKey: "cities") as? NSArray ?? NSArray()
            let apiReturn = JSON(arrCountriesFromJson)
            let arrCountries = apiReturn.to(type: CitiesModel.self) as! [CitiesModel]
            var arrStates = arrCountries.filter { (obj) -> Bool in
                (obj.stateId ?? 0) == stateID
            }
            arrStates = arrStates.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
            return arrStates
//        }
    }
    //    func getAllStateRelatedByCountry(countryID: Int) -> [CD_State] {
    //
    //        let objContext = self.persistentContainer.viewContext
    //
    //        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: ENTITY_STATE)
    //
    //        fetchRequest.predicate = NSPredicate(format: "country_id = %d", countryID)
    //        do {
    //            let results = try  objContext.fetch(fetchRequest) as! [CD_State]
    //
    //            if(results.count > 0)
    //            {
    //                return results
    //            }
    //        }
    //        catch {
    //            print("error executing fetch request: \(error)")
    //        }
    //
    //        return [CD_State]()
    //    }
    
    // fetch All CD_City Details.
    //    func getAllCityRelatedByState(stateID : Int) -> [CD_City] {
    //
    //        let objContext = self.persistentContainer.viewContext
    //
    //        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: ENTITY_CITY)
    //
    //        fetchRequest.predicate = NSPredicate(format: "state_id = %d", stateID)
    //        do {
    //            let results = try  objContext.fetch(fetchRequest) as! [CD_City]
    //
    //            if(results.count > 0)
    //            {
    //                return results
    //            }
    //        }
    //        catch {
    //            print("error executing fetch request: \(error)")
    //        }
    //
    //        return [CD_City]()
    //    }
    
//    func getCountryName(id : Int) -> String {
////        if let jsonData = UserDefaults.standard.value(forKey: "locationCountries"){
////            var arrCountries = [CountriesModel]()
////            let apiReturn = JSON(jsonData)
////            arrCountries = apiReturn.to(type: CountriesModel.self) as! [CountriesModel]
////            let objCountry = arrCountries.filter({ (obj) -> Bool in
////                (obj.id ?? 0) == id
////            })
////            if objCountry.count > 0 {
////                return objCountry[0].name ?? ""
////            }
////            return ""
////        }else{
////             let jsonData = readJSONFromFile()
//            let jsonData = AppDel.locationDict
////            let jsonData = CoreDBManager.sharedDatabase.getDataFromFile()
//            let arrCountriesFromJson = (jsonData as! NSDictionary).value(forKey: "countries") as? NSArray ?? NSArray()
//            var arrCountries = [CountriesModel]()
//            let apiReturn = JSON(arrCountriesFromJson)
//            arrCountries = apiReturn.to(type: CountriesModel.self) as! [CountriesModel]
//            let objCountry = arrCountries.filter({ (obj) -> Bool in
//                (obj.id ?? 0) == id
//            })
//            if objCountry.count > 0 {
//                return objCountry[0].name ?? ""
//            }
//            return ""
////        }
//        
//        
//        
//        
//        //        let objContext = self.persistentContainer.viewContext
//        //
//        //        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: ENTITY_COUNTRY)
//        //
//        //        fetchRequest.predicate = NSPredicate(format: "id = %d", id)
//        //        do {
//        //            let results = try  objContext.fetch(fetchRequest) as! [CD_Country]
//        //
//        //            if(results.count > 0)
//        //            {
//        //                return (results[0].name ?? "")
//        //            }
//        //        }
//        //        catch {
//        //            print("error executing fetch request: \(error)")
//        //        }
//        //
//        //        return ""
//    }
//    
//    func getStateName(id : Int) -> String {
////        if let jsonData = UserDefaults.standard.value(forKey: "locationStates"){
////            let apiReturn = JSON(jsonData)
////            let arrCountries = apiReturn.to(type: StatesModel.self) as! [StatesModel]
////            let arrStates = arrCountries.filter { (obj) -> Bool in
////                (obj.id ?? 0) == id
////            }
////            if arrStates.count > 0 {
////                return arrStates[0].name ?? ""
////            }
////            return ""
////        }else{
////             let jsonData = readJSONFromFile()
//        let jsonData = AppDel.locationDict
////            let jsonData = CoreDBManager.sharedDatabase.getDataFromFile()
//            let arrCountriesFromJson = (jsonData as! NSDictionary).value(forKey: "states") as? NSArray ?? NSArray()
//            let apiReturn = JSON(arrCountriesFromJson)
//            let arrCountries = apiReturn.to(type: StatesModel.self) as! [StatesModel]
//            let arrStates = arrCountries.filter { (obj) -> Bool in
//                (obj.id ?? 0) == id
//            }
//            if arrStates.count > 0 {
//                return arrStates[0].name ?? ""
//            }
//            return ""
////        }
//        //        let objContext = self.persistentContainer.viewContext
//        //
//        //        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: ENTITY_STATE)
//        //
//        //        fetchRequest.predicate = NSPredicate(format: "id = %d", id)
//        //        do {
//        //            let results = try  objContext.fetch(fetchRequest) as! [CD_State]
//        //
//        //            if(results.count > 0)
//        //            {
//        //                return (results[0].name ?? "")
//        //            }
//        //        }
//        //        catch {
//        //            print("error executing fetch request: \(error)")
//        //        }
//        //
//        //        return ""
//    }
//    
//    func getCityName(id : Int) -> String {
////        if let jsonData = UserDefaults.standard.value(forKey: "locationCities"){
////            let apiReturn = JSON(jsonData)
////            let arrCountries = apiReturn.to(type: CitiesModel.self) as! [CitiesModel]
////
////            let objCountry = arrCountries.filter({ (obj) -> Bool in
////                (obj.id ?? 0) == id
////            })
////            if objCountry.count > 0 {
////                return objCountry[0].name ?? ""
////            }
////            return ""
////        }else{
////             let jsonData = readJSONFromFile()
//        let jsonData = AppDel.locationDict
////            let jsonData = CoreDBManager.sharedDatabase.getDataFromFile()
//            let arrCountriesFromJson = (jsonData as! NSDictionary).value(forKey: "cities") as? NSArray ?? NSArray()
//            let apiReturn = JSON(arrCountriesFromJson)
//            let arrCountries = apiReturn.to(type: CitiesModel.self) as! [CitiesModel]
//
//            let objCountry = arrCountries.filter({ (obj) -> Bool in
//                (obj.id ?? 0) == id
//            })
//            if objCountry.count > 0 {
//                return objCountry[0].name ?? ""
//            }
//            return ""
////        }
//        //        let objContext = self.persistentContainer.viewContext
//        //
//        //        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: ENTITY_CITY)
//        //
//        //        fetchRequest.predicate = NSPredicate(format: "id = %d", id)
//        //        do {
//        //            let results = try  objContext.fetch(fetchRequest) as! [CD_City]
//        //
//        //            if(results.count > 0)
//        //            {
//        //                return (results[0].name ?? "")
//        //            }
//        //        }
//        //        catch {
//        //            print("error executing fetch request: \(error)")
//        //        }
//        //
//        //        return ""
//    }
    
    func getStatesFromId(countryId:Int) -> [StatesModel]{
        let statesArr = AppDel.arrStates.filter({$0.countryId == countryId})
//        let statesArr = AppDel.arrStates.filter { (obj) -> Bool in
//            (obj.countryId ?? 0) == countryId
//        }
        return statesArr.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
    }
    
    func getCitiesFromId(stateId:Int) -> [CitiesModel]{
        let citiesArr = AppDel.arrCities.filter({$0.stateId == stateId})
//        let citiesArr = AppDel.arrCities.filter { (obj) -> Bool in
//            (obj.stateId ?? 0) == stateId
//        }
        return citiesArr.sorted(by: { $0.name?.lowercased() ?? "" < $1.name?.lowercased() ?? "" })
    }
    
    func getCountryName(countryId:Int) -> String{
        let objCountry = AppDel.arrCountrys.filter({$0.id == countryId})
//        let objCountry = AppDel.arrCountrys.filter({ (obj) -> Bool in
//            (obj.id ?? 0) == countryId
//        })
        if objCountry.count > 0 {
            return objCountry[0].name ?? ""
        }
        return ""
    }
    
    func getStateName(stateId:Int) -> String{
        let objState = AppDel.arrStates.filter({$0.id == stateId})
//        let objState = AppDel.arrStates.filter({ (obj) -> Bool in
//            (obj.id ?? 0) == stateId
//        })
        if objState.count > 0 {
            return objState[0].name ?? ""
        }
        return ""
    }
    
    func getCityName(cityId:Int) -> String{
        let objCity = AppDel.arrCities.filter({$0.id == cityId})
//        let objCity = AppDel.arrCities.filter({ (obj) -> Bool in
//            (obj.id ?? 0) == cityId
//        })
        if objCity.count > 0 {
            return objCity[0].name ?? ""
        }
        return ""
    }
    
    //delete All Records
    func deleteAllRecords(entityName : String) {
        let context = self.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            self.saveContext()
        } catch {
            print ("There was an error")
        }
    }
}
