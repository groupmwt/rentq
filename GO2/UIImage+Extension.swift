//
//  UIImage+Extension.swift
//  GO2
//
//  Created by NC2-46 on 10/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import Foundation
extension UIImage {
    
    func resizeImageSize(newSize: CGSize = CGSize(width: 50, height: 50)) -> UIImage?
    {
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        //image.draw(in: CGRectMake(0, 0, newSize.width, newSize.height))
        self.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height))  )
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
   
    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
    
    func convertImageToBase64() -> String {
        let imageData = UIImageJPEGRepresentation(self, 0.5)
        return imageData!.base64EncodedString(options: .lineLength64Characters)
    }
    
    // MARK: - UIImage (Base64 Encoding)
    
    public enum ImageFormat {
        case PNG
        case JPEG(CGFloat)
    }
    
    public func base64(format: ImageFormat) -> String? {
        var imageData: Data?
        switch format {
        case .PNG: imageData = UIImagePNGRepresentation(self)
        case .JPEG(let compression): imageData = UIImageJPEGRepresentation(self, compression)
        }
        return imageData?.base64EncodedString()
    }
    
    func compressByKB(maxByte: Float) -> Data
    { // max 300kb == 300000
        let maxMB = (maxByte * 1024)
        var compressQuality: CGFloat = 1
        var imageData = UIImageJPEGRepresentation(self, 1)
        var imageByte = Float((UIImageJPEGRepresentation(self, 1)?.count)!)
        while imageByte > maxMB
        {
            imageByte = Float((UIImageJPEGRepresentation(self, compressQuality)?.count)!)
            compressQuality -= 0.1
        }
        imageData = UIImageJPEGRepresentation(self, compressQuality)
        return imageData!
    }
}
