//
//  VehicleParts.swift
//  GO2
//
//  Created by C115 on 12/11/21.
//  Copyright © 2021 Juan Cardozo. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class VehicleParts: NSCoding,JSONable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let name = "name"
        static let vehicleTypeId = "vehicle_type_id"
        static let createdAt = "created_at"
        static let updatedAt = "updated_at"
        static let vehiclePartName = "vehicle_part_name"
        static let vehicleType = "vehicle_type"
        static let vehicleCategoryId = "vehicle_category_id"
        static let vehicleCategoryType = "vehicle_category_type"
    }
    
    // MARK: Properties
    public var id: Int?
    public var name: String?
    public var vehicleTypeId: Int?
    public var createdAt: String?
    public var updatedAt: String?
    public var vehiclePartName: String?
    public var vehicleType: String?
    public var vehicleCategoryId: Int?
    public var vehicleCategoryType: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        id = json[SerializationKeys.id].int
        name = json[SerializationKeys.name].string
        vehicleTypeId = json[SerializationKeys.vehicleTypeId].int
        createdAt = json[SerializationKeys.createdAt].string
        updatedAt = json[SerializationKeys.updatedAt].string
        vehiclePartName = json[SerializationKeys.vehiclePartName].string
        vehicleType = json[SerializationKeys.vehicleType].string
        vehicleCategoryId = json[SerializationKeys.vehicleCategoryId].int
        vehicleCategoryType = json[SerializationKeys.vehicleCategoryType].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = vehicleTypeId { dictionary[SerializationKeys.vehicleTypeId] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = vehiclePartName { dictionary[SerializationKeys.vehiclePartName] = value }
        if let value = vehicleType { dictionary[SerializationKeys.vehicleType] = value }
        if let value = vehicleCategoryId { dictionary[SerializationKeys.vehicleCategoryId] = value }
        if let value = vehicleCategoryType { dictionary[SerializationKeys.vehicleCategoryType] = value }
        
        
       
        
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.vehicleTypeId = aDecoder.decodeObject(forKey: SerializationKeys.vehicleTypeId) as? Int
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.vehiclePartName = aDecoder.decodeObject(forKey: SerializationKeys.vehiclePartName) as? String
        self.vehicleType = aDecoder.decodeObject(forKey: SerializationKeys.vehicleType) as? String
        self.vehicleCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.vehicleCategoryId) as? Int
        self.vehicleCategoryType = aDecoder.decodeObject(forKey: SerializationKeys.vehicleCategoryType) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(vehicleTypeId, forKey: SerializationKeys.vehicleTypeId)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(vehiclePartName, forKey: SerializationKeys.vehiclePartName)
        aCoder.encode(vehicleType, forKey: SerializationKeys.vehicleType)
        aCoder.encode(vehicleCategoryId, forKey: SerializationKeys.vehicleCategoryId)
        aCoder.encode(vehicleCategoryType, forKey: SerializationKeys.vehicleCategoryType)
    }
    
}
