//
//  LocationPickerVC.swift
//  GO2
//
//  Created by NC2-46 on 18/08/18.
//  Copyright © 2018 Juan Cardozo. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces

//MARK:- Protocol
protocol SelectedPlaceDelegete
{
    func getSelectedPlace(pickAddress : GMSAddress,placeName: String)
}


class LocationPickerVC: UIViewController{
    var placesClient: GMSPlacesClient!
    var delegateSelectedPlace: SelectedPlaceDelegete!
    var locationManager = CLLocationManager()
    var isFirstTimeLocationGet : Bool = false
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var placeName = ""
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var lblPlace: UILabel!
    
    @IBOutlet weak var imgMarkerPin: UIImageView!
    var pickAddress : GMSAddress!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackBtn()
        navigationController?.customNavBar()
        navigationItem.customTitle()
        lblPlace.isHidden = true
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.isIndoorEnabled = true
        mapView.isTrafficEnabled = true
        mapView.isBuildingsEnabled = true
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 20)
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        if CLLocationManager.authorizationStatus() == .notDetermined
        {
            locationManager.requestAlwaysAuthorization()
        }
        placesClient = GMSPlacesClient.shared()
        
        addToPopover()
    }
    
    func addToPopover(){
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
//        let filter = GMSAutocompleteFilter()
//        filter.type = .establishment
//        filter.country = "UK"
//
//        resultsViewController?.autocompleteFilter = filter
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
            
        if #available(iOS 13.0, *) {
            searchController?.searchBar.searchTextField.textColor = UIColor.white
        } else {
            // Fallback on earlier versions
        }
        
        // Add the search bar to the right of the nav bar,
        // use a popover to display the results.
        // Set an explicit size as we don't want to use the entire nav bar.
        searchController?.searchBar.frame = lblPlace.frame //(CGRect(x: 0, y: 0, width: 250.0, height: 44.0))
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: (searchController?.searchBar)!)
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Keep the navigation bar visible.
        searchController?.hidesNavigationBarDuringPresentation = false
        let country = AppDel.country ?? ""
        let state = AppDel.state ?? ""
        let city = AppDel.city ?? ""
        self.searchController?.searchBar.text = city + "," + state + "," + country
        DispatchQueue.main.async {
            self.searchController?.searchBar.becomeFirstResponder()
        }
        

//        searchController?. = .popover
    }
    
    @objc override func backTap(animation: Bool = true) {
        
        if pickAddress != nil {
             self.delegateSelectedPlace.getSelectedPlace(pickAddress: pickAddress, placeName: placeName)
        }
       
        popViewController(animation: true)
    }
}


extension LocationPickerVC : GMSMapViewDelegate {

    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        guard let lat = mapView.myLocation?.coordinate.latitude,
            let lng = mapView.myLocation?.coordinate.longitude else { return false }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 15.0)
        mapView.animate(to: camera)
        return true
    }

    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//        reverseGeocodeCoordinate(coordinate: position.target)
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
        
        
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                self.lblPlace.text = ""
                
                if let error = error {
                    print("Unable to Reverse Geocode Location (\(error))")
                } else {
                    if let localAddress : [String] = address.lines {
                        self.pickAddress = address
                        self.placeName = ""
                        for add in localAddress {
                            self.placeName = self.placeName + add + "\n"
//                            self.searchController?.searchBar.text = placeName
                        }
                        if self.pickAddress != nil {
                            self.delegateSelectedPlace.getSelectedPlace(pickAddress: self.pickAddress, placeName: self.placeName)
                        }
                       
                        self.popViewController(animation: true)
                    }

//                    if let placemarks = response, let placemark = placemarks.firstResult() {
////                        self.city = placemark.locality!
//                        //self.country = placemark.country!
//
//                        let country = placemark.country ?? ""
//                        let state = placemark.administrativeArea ?? ""
//                        let city = placemark.locality ?? ""
//                        AppDel.country = country
//                        AppDel.state = state
//                        AppDel.city = city
//                    }
                }
                
                
                // 4
                UIView.animate(withDuration: 0.25) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
     
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
      
        
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
      
    }
}

// MARK: - CLLocationManagerDelegate
extension LocationPickerVC: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if isFirstTimeLocationGet == false {
            if let location = locations.first {
                let camera  = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15.0)
                mapView.animate(to: camera)
                isFirstTimeLocationGet = true
            }
        }
        
    }
}

// Handle the user's selection.
    extension LocationPickerVC: GMSAutocompleteResultsViewControllerDelegate {
        func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                               didAutocompleteWith place: GMSPlace) {
//            searchController?.isActive = false
            // Do something with the selected place.
            print("Place name: \(place.name ?? "")")
            print("Place address: \(String(describing: place.formattedAddress))")
//            print("Place attributions: \(place.attributions ?? "")")
//            lblPlace.text = "\(place.formattedAddress ?? "")"
            var locationCoordinates = CLLocationCoordinate2D()
            locationCoordinates.latitude = place.coordinate.latitude
            locationCoordinates.longitude = place.coordinate.longitude
            DispatchQueue.main.async{
                self.reverseGeocodeCoordinate(coordinate: locationCoordinates)
            }
//            searchController?.searchBar.text = lblPlace.text
//            if pickAddress != nil {
//                 self.delegateSelectedPlace.getSelectedPlace(pickAddress: pickAddress, placeName: placeName)
//            }
//
//            popViewController(animation: true)
        }

        func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                               didFailAutocompleteWithError error: Error){
            // TODO: handle the error.
            print("Error: ", error.localizedDescription)
        }

        // Turn the network activity indicator on and off again.
        func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }

        func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        
        func updateSearchResultsForSearchController(searchController: UISearchController) {
            
        }
    }
