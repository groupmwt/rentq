//
//  MenuTableViewController.swift
//  GO2
//
//  Created by GNM on 5/19/17.
//  Copyright © 2017 Juan Cardozo. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class MenuViewController: UITableViewController {

    var manuOptions:Array = [String]()
    var menuIcons:Array = [UIImage]()
    
    @IBOutlet weak var header: UIView!
    @IBOutlet var tableMenu: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        manuOptions = ["Map", "Book a vehicle", "History", "My Vehicles","Profile","Tems and Conditions","Privacy Policy" ,"Log Out"]
        
        menuIcons = [#imageLiteral(resourceName: "i_map"),#imageLiteral(resourceName: "i_rent_my_vehicle"),
                     #imageLiteral(resourceName: "i_reserve_vehicle"), #imageLiteral(resourceName: "i_my_vehicles"),
                      #imageLiteral(resourceName: "i_profile"),
                     #imageLiteral(resourceName: "i_terms"),#imageLiteral(resourceName: "i_policy"),
                     #imageLiteral(resourceName: "i_log_out")]
        
        self.view.backgroundColor? = Color.screenBG
        self.navigationController?.isNavigationBarHidden = true
        tableMenu.bounces = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return manuOptions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        
        cell.icon.image = menuIcons[indexPath.row]
        cell.nameOption.text = manuOptions[indexPath.row]
        return cell
        
    }
    

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let selectionColor = UIView() as UIView
        selectionColor.layer.borderWidth = 1
        selectionColor.layer.borderColor = UIColor(red:2/255, green:30/255, blue:39/255, alpha:1.0).cgColor
        selectionColor.backgroundColor = UIColor(red:2/255, green:30/255, blue:39/255, alpha:1.0)
//        selectionColor.layer.borderColor = UIColor(red:0.00, green:0.00, blue:0.87, alpha:1.0).cgColor
//        selectionColor.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.87, alpha:1.0)
        cell.selectedBackgroundView = selectionColor
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.mm_drawerController!.toggle(.left, animated: true, completion: nil)
        
        switch indexPath.row {
        case 0:
            let centerViewController = loadViewController(StoryBoardName: "Map", VCIdentifer: "MapVC") as! MapVC
            let centerNavController = UINavigationController(rootViewController: centerViewController)
            self.mm_drawerController.centerViewController = centerNavController
            break
            
        case 1:
            
            let centerViewController = loadViewController(StoryBoardName: "Map", VCIdentifer: "MapVC") as! MapVC
            let centerNavController = UINavigationController(rootViewController: centerViewController)
            UserDefaults.standard.set(true, forKey: kSetting)
            
            self.mm_drawerController.centerViewController = centerNavController
            
//            let centerViewController = loadViewController(StoryBoardName: "BookVehicles", VCIdentifer: "BookVehiclesHomeVC") as! BookVehiclesHomeVC
//            centerViewController.fromMenu = true
//            centerViewController.navigationTitle = "Reserve Vehicle"
//            centerViewController.headerTitle = "Reserve Vehicles"
//            let centerNavController = UINavigationController(rootViewController: centerViewController)
//            self.mm_drawerController.centerViewController = centerNavController
            break
        case 2:
            let centerViewController = loadViewController(StoryBoardName: "History", VCIdentifer: "HistoryVC") as! HistoryVC
            let centerNavController = UINavigationController(rootViewController: centerViewController)
            self.mm_drawerController.centerViewController = centerNavController
            
            break
        case 3:
            let centerViewController = loadViewController(StoryBoardName: "VehicleList", VCIdentifer: "MyCarsListVC") as! MyCarsListVC
            centerViewController.fromMenu = true
            centerViewController.navigationTitle = "My Vehicles"
            let centerNavController = UINavigationController(rootViewController: centerViewController)
            self.mm_drawerController.centerViewController = centerNavController
            break
            
        case 4:
            let centerViewController = loadViewController(StoryBoardName: "Profile", VCIdentifer: "ProfileViewController") as! ProfileViewController
            let centerNavController = UINavigationController(rootViewController: centerViewController)
            self.mm_drawerController.centerViewController = centerNavController
            break

        case 5:
            let centerViewController = loadViewController(StoryBoardName: "PrivacyPolicy", VCIdentifer: "TermConditionVC") as! TermConditionVC
            let centerNavController = UINavigationController(rootViewController: centerViewController)
            self.mm_drawerController.centerViewController = centerNavController
            break
            
        case 6:
            let centerViewController = loadViewController(StoryBoardName: "PrivacyPolicy", VCIdentifer: "PrivacyPolicyVC") as! PrivacyPolicyVC
            centerViewController.headerTitle = "Privacy Policy"
            let centerNavController = UINavigationController(rootViewController: centerViewController)
            self.mm_drawerController.centerViewController = centerNavController
            break
            
        case 7:
            displayAlertWithAction(userMessage: "Are you sure you want to logout from this app?", userTitle: "Logout", alertStyle: .alert, arrActionName: ["Cancel","Ok"], arrActionStyle: [.cancel,.default], completion: { (btnIndex) in
                switch btnIndex {
                case 1:
                    removeUserLogin(viewController: self)
                    break
                default :
                    break
                }
            })
            
            break

        default:
            break
        }
        
    }
}
