//
//  User.swift
//  GO2
//
//  Created by Juan Cardozo on 5/25/17.
//  Copyright © 2017 GNM. All rights reserved.
//

import UIKit

class User {
    
    // MARK: Properties
    var email:String
    var apiKey:String?
    
   
    
    // MARK: Initialization
    init?(email:String, apiKey:String) {
        self.email = email
        self.apiKey = apiKey
    }
    
}
