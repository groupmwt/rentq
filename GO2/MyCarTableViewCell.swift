//
//  MyCarTableViewCell.swift
//  GO2
//
//  Created by Juan Carlos Cardozo on 26/6/17.
//  Copyright © 2017 Juan Cardozo. All rights reserved.
//

import UIKit
import SDWebImage
class MyCarTableViewCell: UITableViewCell {
    
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var btnCheckAvailability: UIButtonX!
    @IBOutlet weak var vehicleBrand: UILabel!
    @IBOutlet weak var vehiclePlate: UILabel!
    @IBOutlet weak var vehicleModel: UILabel!
    @IBOutlet weak var vehicleAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.vehicleImage.contentMode = .scaleAspectFill
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(vehicleImageURL: String?,vehicleBrand : String, vehiclePlate:String,vehicleModel:String,vehicleAddress:String, tagvalue:Int) {

        let finalUrl : String = getConexionAPI_Production() + (vehicleImageURL ?? "" )
        let url = URL.init(string: finalUrl)
        vehicleImage.sd_setShowActivityIndicatorView(true)
        vehicleImage.sd_setIndicatorStyle(.gray)
        vehicleImage.sd_setImage(with: url , placeholderImage: nil)
        self.vehicleBrand.text = "Brand : " + vehicleBrand
        self.vehiclePlate.text = "Plate  : " + vehiclePlate
        self.vehicleModel.text = "Model : " + vehicleModel
        self.vehicleAddress.text = "Address : " + vehicleAddress
        btnCheckAvailability.tag = tagvalue
        btnCheckAvailability.backgroundColor = Color.buttonBG
    }
}

